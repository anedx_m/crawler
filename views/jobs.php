
<div class="job-actions">
	<?php
	if ( $crawler_status == 'Stop' ) {
		$class_action			 = 'btn-success';
		$class_action_text		 = 'Start';
		$class_action_text_state = 'Stopped';
	} else {
		$class_action			 = 'btn-danger';
		$class_action_text		 = 'Stop';
		$class_action_text_state = 'Running';
	}
	?>
	<div class="row">
		<div class="col-sm-1">
			<span style="font-size: 15px; font-weight:bold;"><?php echo $class_action_text_state ?></span> <input type="button" class="btn btn-default crawler_status <?php echo $class_action ?>" value="<?php echo $class_action_text ?>">
		</div>

		<div class="col-sm-7">
			<div class="btn-hs-add-job btn btn-default"><div class="text-hs-add-job fa-chevron-down-after">ADD JOB</div></div>
			<div class="add-new-job" style="display: none;">
				<form action="?act=AddJob" method="POST">
					<input type="hidden" name="MAX_FILE_SIZE" value="30000" />
					<div class="row">
						<div class="col-sm-3">
							<div class="row padding-2">
								<div class="col-sm-9 text-right">
									<label for="depth">Max Depth: </label>
								</div>
								<div class="col-sm-3">
									<input id="depth" class="input-30" name="job[depth]"type="input" value="<?php echo $this->getValue( $settings, 'default_depth' ) ?>">
								</div>
							</div>
							<div class="row padding-2">
								<div class="col-sm-9 text-right">
									<label for="bots">Crawlers: </label>
								</div>
								<div class="col-sm-3">
									<input id="bots" class="input-30" name="job[crawlers]" type="input" value="<?php echo $this->getValue( $settings, 'default_crawlers' ) ?>">
								</div>
							</div>
							<div class="row padding-2">
								<div class="col-sm-9 text-right">
									<label for="bots">Threads Per Crawler: </label>
								</div>
								<div class="col-sm-3">
									<input id="bots" class="input-30" name="job[threads_per_crawler]" type="input" value="<?php echo $this->getValue( $settings, 'default_threads_per_crawler' ) ?>">
								</div>
							</div>
							<div class="row padding-2">
								<div class="col-sm-9 text-right">
									<label for="max_run_domains">Domains Per Crawler: </label>
								</div>
								<div class="col-sm-3">
									<input id="max_urls_per_domain" class="input-70" name="job[max_run_domains]"  value="<?php echo $this->getValue( $settings, 'default_domains_per_crawler' ) ?>">
								</div>
							</div>
							<div class="row padding-2">
								<div class="col-sm-9 text-right">
									<label for="only_domain">Only within the domain: </label>
								</div>
								<div class="col-sm-3">
									<input id="only_domain" class="input-30" name="job[only_domain]" type="checkbox" checked value="1">
								</div>
							</div>
							<div class="row padding-2">
								<div class="col-sm-9 text-right">
									<label for="max_urls_per_domain">Max urls per domain: </label>
								</div>
								<div class="col-sm-3">
									<input id="max_urls_per_domain" class="input-70" name="job[max_urls_per_domain]"  value="1000">
								</div>
							</div>
							<div class="row padding-2">
								<div class="col-sm-9 text-right">
									<label for="max_time_in_mins">Crawl Domain Max Time(in mins): </label>
								</div>
								<div class="col-sm-3">
									<input id="max_time_in_mins" class="input-70" name="job[max_time_in_mins]"  value="10">
								</div>
							</div>
						</div>
						<div class="col-sm-9">
							<div class="row padding-2">
								<div class="col-sm-3 text-right">
									<label for="name_job" >Job Title: </label>
								</div>
								<div class="col-sm-9">
									<input id="name_job" name="job[name_job]" class="width-100pr" type="text" />
								</div>
							</div>
							<div class="row padding-2">
								<div class="col-sm-3 text-right">
									<label for="fileJob">Job Domains/Urls: </label>
								</div>
								<div class="col-sm-9">
									<textarea class="list-urls" name="job[list_urls]"></textarea>
								</div>
							</div>
							<div class="row padding-2">
								<div class="col-sm-offset-6 col-sm-6">
									<input type="submit" class="btn btn-primary" value="Add Job">
								</div>
							</div>
						</div>
					</div>

				</form>
			</div>

		</div>
		<div class="col-sm-4">
			<?php if ( !empty( $current_job ) ): ?>
				<div style="float:	">

					<?php if ( !empty( $current_job[ 'title' ] ) ): ?>
						<h4>Current Job:</h4>
						<table class="table table-hover table-bordered table-striped">
							<tr>
								<th>Current Job</th>
								<th>Total parsed urls</th>
								<th>Domains found</th>
							</tr>
							<tr>
								<td class="current_job"><?php echo $current_job[ 'title' ] ?></td>
								<td class="passed_urls"><?php echo number_format( $current_job[ 'passed_urls' ] ) ?></td>
								<td class="count_domains"><?php echo number_format( $current_job[ 'count_domains' ] ) ?></td>
							</tr>
						</table>
					<?php endif; ?>
					<div class="btn btn-default slide-processes"><div class="fa-chevron-down-after">View Status</div></div>
					<div class="d-processes">
						<table class="table table-hover table-bordered table-striped processes">
							<tr>
								<td>
									Running Processes:
								</td>
								<td class="running_processes">
									<?php echo $processes[ 'running_processes' ] ?>
								</td>
							</tr>
							<tr>
								<td>
									Crawlers:
								</td>
								<td class="crawler">
									<?php echo $processes[ 'crawler' ] ?>
								</td>
							</tr>
							<tr>
								<td>
									Test Offline:
								</td>
								<td class="test_offline">
									<?php echo $processes[ 'test_offline' ] ?>
								</td>
							</tr>
							<tr>
								<td>
									Refresh Moz DA:
								</td>
								<td class="refresh_moz_da">
									<?php echo $processes[ 'refresh_moz_da' ] ?>
								</td>
							</tr>
							<tr>
								<td>
									Refresh DNS check:
								</td>
								<td  class="refresh_dns_check">
									<?php echo $processes[ 'refresh_dns_check' ] ?>
								</td>
							</tr>
							<tr>
								<td>
									Refresh Whois Avail:
								</td>
								<td  class="refresh_whois_avail">
									<?php echo $processes[ 'refresh_whois_avail' ] ?>
								</td>
							</tr>
							<tr>
								<td>
									Refresh Whois Date:
								</td>
								<td class="refresh_whois_date">
									<?php echo $processes[ 'refresh_whois_date' ] ?>
								</td>
							</tr>
							<tr>
								<td>
									Refresh Magestic:
								</td>
								<td class="refresh_magestic">
									<?php echo $processes[ 'refresh_magestic' ] ?>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="btn btn-default slide-crawlers margin-top-7"><div class="fa-chevron-down-after">View Crawlers</div></div>
				<div class="d-crawlers">
					<table class="table table-hover table-bordered table-striped crawlers">
						<thead>
						<tr>
							<th>Domain</th>
							<th>Urls</th>
							<th>Passed Urls</th>
						</tr>
						</thead>
						<tbody id="crawlers_state">
						<?php
						foreach ( $crawlers as $crawler ) {
							?>
							<tr>
								<td><?php echo $crawler[ 'domain' ] ?></td>
								<td><?php echo $crawler[ 'urls' ] ?></td>
								<td><?php echo $crawler[ 'passed_urls' ] ?></td>
							</tr>
							<?php
						}
						?>
						</tbody>
					</table>
				</div>
			</div>
		<?php endif; ?>
	</div>
</div>
</div>
<div class="search padding-10">
	<input id="search-input" class="input-sm" placeholder="search">
	<input id="search-btn" type="button" class="btn btn-default" value="search">
</div>
<div class="jobs-table"></div>
<script>
	var ex_jobs = new Table( '?act=ListJobs', '.jobs-table' );

	$( document ).ready( function() {
		ex_jobs.list( { order_by: 'status' } )
		$( '#search-btn' ).click( function() {
			var params = { search: $( '#search-input' ).val( ) }
			ex_jobs.list( params, 1 );
		} )
		ex_jobs.afterList.add( function() {

			$( '.jobs-table .delete' ).click( function() {
				var job_id = $( this ).attr( 'id' );
				var r = confirm( 'Are you sure?' )
				if ( r ) {
					$.post( '?act=DeleteJob', { 'job_id': job_id }, function() {
						ex_jobs.list()
					} )

				}
			} )

			$( '.jobs-table .stop' ).click( function() {
				var job_id = $( this ).attr( 'id' );
				var r = confirm( 'Are you sure?' )
				if ( r ) {
					$.post( '?act=StopJob', { 'job_id': job_id }, function() {
						ex_jobs.list()
					} )

				}
			} )

			$( '.crawler_status' ).click( function() {
				$.post( '?act=SetCrawlerStatus', '', function( data ) {
//					alert(data)
					document.location.reload()
				} )
			} )
		} )
		$( '.btn-hs-add-job' ).click( function() {
			$( ".add-new-job" ).slideToggle( 'slow' );
		} )

<?php if ( !empty( $current_job ) ): ?>
			if ( typeof t !== 'undefined' )
				clearInterval( t );
			t = setInterval( refresh_current_job_processes, 3000 )
<?php endif; ?>

		$( '.d-processes' ).toggle()
		$( '.slide-processes' ).click( function() {
			$( '.d-processes' ).slideToggle( 'slow' );
		} )

		$( '.d-crawlers' ).toggle()
		$( '.slide-crawlers' ).click( function() {
			$( '.d-crawlers' ).slideToggle( 'slow' );
		} )

		function refresh_current_job_processes() {
			$.post( '?act=GetCurrentJobAndProcesses', '', function( data ) {
				$( '.current_job' ).html( data.current_job )
				$( '.urls_in_queue' ).html( data.urls_in_queue )
				$( '.passed_urls' ).html( data.passed_urls )
				$( '.count_domains' ).html( data.count_domains )
				$( '.crawler' ).html( data.crawler )
				$( '.running_processes' ).html( data.running_processes )
				$( '.test_offline' ).html( data.test_offline )
				$( '.refresh_moz_da' ).html( data.refresh_moz_da )
				$( '.refresh_dns_check' ).html( data.refresh_dns_check)
				$( '.refresh_whois_avail' ).html( data.refresh_whois_avail )
				$( '.refresh_whois_date' ).html( data.refresh_whois_date )
				$( '.refresh_magestic' ).html( data.refresh_magestic )
				
				var html ="";
				$.each(data.crawlers, function(i, item) {
					html += "<tr><td>"+item.domain+"</td><td>"+item.urls+"</td><td>"+item.passed_urls+"</td></tr>";
				})
				$( '#crawlers_state' ).html( html );
				
			}, 'JSON' )
		}
		
		
	} )



</script>