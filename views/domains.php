<form class="domains-params">
	<div class="row ">
		<div class="col-lg-2">
			<div class="padding-10">
				<input id="search-input" class="input-sm search-input" placeholder="search" name="search">
			</div>
		</div>
		<div class="col-lg-3">
			<div class="padding-10">
				<input id="date_from" class="input-sm date_from" placeholder="Date from" name="date_from">
				<input id="date_to" class="input-sm date_to" placeholder="Date to" name="date_to">
			</div>
		</div>
		<div class="col-lg-7">
			<div class="export-domains padding-10" style="float: right;">
				<label for="max_rows">Max rows:&nbsp;</label><input type="text" id="max_rows" class="input-50">
				<input id="export-domains" type="button" class="btn btn-primary" value="Export">
			</div>
		</div>
	</div>
</form>
<div class="domains-table"></div>

<script>
	var ex_domains = new Table( '?act=ListDomains', '.domains-table' );

	$( document ).ready( function() {
		$( '#date_from,#date_to' ).datepicker( { dateFormat: 'yy-mm-dd' } );
		ex_domains.list( { order_by: 'date_found', direction: 'DESC' } )
		$( '#search-domain' ).click( function() {
			var params = getParams();
			ex_domains.list( params, 1 );
		} )

		$( '#export-domains' ).click( function() {
			var params = getParams();
			params.max_row = $( '#max_rows' ).val( );
			var order_by = $( '.domains-table' ).find( '.order_by' ).val()
			var direction = $( '.domains-table' ).find( '.direction' ).val()
			document.location = '?act=ExportDomains&' + $.param( { 'params': params } ) + '&' + $.param( { 'order_by': order_by, 'direction': direction } );
		} )
	} )

	function search() {
		var params = getParams();
		ex_domains.list( params, 1 );
	}
	function getParams() {
		var inps = $( '.domains-params input' );
		var params = { };
		$.each( inps, function( i, el ) {
			var name = $( el ).attr( 'name' )
			var type = $( el ).attr( 'type' )
			if ( name ) {
				if ( type == 'checkbox' ) {
					params[name] = $( el ).is( ':checked' )
				} else
					params[name] = $( el ).val();
			}
		} )
		console.log( params )
		return params;
	}
	$( '.domains-params input:not([type=checkbox],.date_from,.date_to)' ).afterKeyDown( function( ev ) {
		search()
	}, 1000, true );
	$( '.domains-params input' ).afterChange( function( ev ) {
		search()
	}, 1000, true );
</script>