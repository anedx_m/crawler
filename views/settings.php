
<form class="settings-form form-horizontal">
	<div class="settings row">
		<div class="setting col-sm-2">
			<h4 class="header">General</h4>
			<label for="password">Password:</label><input name="settings[password]" placeholder="Password" id="password" type="text" class="" value="<?php echo $this->getValue( $settings, 'password' ) ?>">
			<label for="connection_timeout">Connection timeout(in seconds):</label><input name="settings[connection_timeout]" placeholder="Connection timeout" id="connection_timeout" type="text" class="" value="<?php echo $this->getValue( $settings, 'connection_timeout' ) ?>">
			<label for="max_page_size">Max Page Size ( in Kbytes):</label><input name="settings[max_page_size]" placeholder="Max Page Size" id="max_page_size" type="text" class="" value="<?php echo $this->getValue( $settings, 'max_page_size' ) ?>">
			<label for="max_exec_time">Max execution time(in seconds):</label><input name="settings[max_exec_time]" placeholder="Max execution time" id="max_exec_time" type="text" class="" value="<?php echo $this->getValue( $settings, 'max_exec_time' ) ?>">

		</div>
		<div class="setting col-sm-3">
			<h4 class="header">Crawler</h4>
			<label for="default_threads">Default Crawlers:</label><input name="settings[default_crawlers]" placeholder="Default Crawlers" id="default_crawlers" type="text" class="" value="<?php echo $this->getValue( $settings, 'default_crawlers' ) ?>">
			<label for="default_threads_per_crawler">Default threads per crawler:</label><input name="settings[default_threads_per_crawler]" placeholder="Default threads per crawler" id="default_threads_per_crawler" type="text" class="" value="<?php echo $this->getValue( $settings, 'default_threads_per_crawler' ) ?>">
			<label for="default_domains_per_crawler">Default domains per crawler:</label><input name="settings[default_domains_per_crawler]" placeholder="Default domains per crawler" id="default_domains_per_crawler" type="text" class="" value="<?php echo $this->getValue( $settings, 'default_domains_per_crawler' ) ?>">
			<label for="default_depth">Default depth:</label><input  name="settings[default_depth]" placeholder="Default depth" id="default_depth" type="text" class="" value="<?php echo $this->getValue( $settings, 'default_depth' ) ?>">
		</div>
		<div class="setting col-sm-3">
			<h4 class="header">Offline check</h4>
			<label for="default_depth">Сonnection timeout:</label><input  name="settings[offlinecheck_connection_timeout]" placeholder="Offline check connection timeout" id="offlinecheck_connection_timeout" type="text" class="" value="<?php echo $this->getValue( $settings, 'offlinecheck_connection_timeout' ) ?>">
			<label for="offlinecheck_crawlers">Crawlers:</label><input  name="settings[offlinecheck_crawlers]" placeholder="Offline crawlers" id="offlinecheck_crawlers" type="text" class="" value="<?php echo $this->getValue( $settings, 'offlinecheck_crawlers' ) ?>">
			<label for="offlinecheck_threads">Threads:</label><input  name="settings[offlinecheck_threads]" placeholder="Offline threads" id="offlinecheck_threads" type="text" class="" value="<?php echo $this->getValue( $settings, 'offlinecheck_threads' ) ?>">
			<label for="offlinecheck_skip_tlds">Skip tlds (separate by space):</label><textarea  name="settings[offlinecheck_skip_tlds]" placeholder="gov.uk us edu" id="offlinecheck_skip_tlds" type="text" class="skip_tlds" value=""><?php echo $this->getValue( $settings, 'offlinecheck_skip_tlds' ) ?></textarea>
			<label for="offlinecheck_skip_names">Skip domains having words (separate by space):</label><input name="settings[offlinecheck_skip_names]" placeholder="viagra sex" id="offlinecheck_skip_names" type="text" class="" value="<?php echo $this->getValue( $settings, 'offlinecheck_skip_names' ) ?>">
			<label for="offlinecheck_days_ago">Refresh every (days):</label><input name="settings[offlinecheck_days_ago]" placeholder="Refresh every (days)" id="offlinecheck_days_ago" type="text" class="" value="<?php echo $this->getValue( $settings, 'offlinecheck_days_ago' ) ?>">
		</div>
	</div>
	<div class="settings row">
		<div class="setting col-sm-2">
			<h4 class="header">Domainr Whois Available</h4>
			<!--<label for="whois_api_key">Dynadot Api keys (one per line):</label><textarea name="settings[dynadot_keys]" placeholder="Dynadot Api keys (one per line)" id="dynadot_keys" type="text" class="" value=""><?php // echo $this->getValue( $settings, 'dynadot_keys' ) ?></textarea>-->
			<label for="min_moz_da_domainr">Min moz	DA to check availability:</label><input name="settings[min_moz_da_domainr]" placeholder="min moz da" id="refresh_whois" type="text" class="" value="<?php echo $this->getValue( $settings, 'min_moz_da_domainr' ) ?>">
			<label for="dig_crawlers">Dig Crawlers:</label><input name="settings[dig_crawlers]" placeholder="crawlers" id="dig_crawlers" type="text" class="" value="<?php echo $this->getValue( $settings, 'dig_crawlers' ) ?>">
			<label for="refresh_dns">Run dig every (days):</label><input name="settings[refresh_dns]" placeholder="Refresh every (days)" id="refresh_dns" type="text" class="" value="<?php echo $this->getValue( $settings, 'refresh_dns' ) ?>">
			<label for="mashape_key">Mashape key</label><input name="settings[mashape_key]" placeholder="mashape key" id="mashape_key" type="text" class="" value="<?php echo $this->getValue( $settings, 'mashape_key' ) ?>">
			<label for="domainr_crawlers">Domainr Crawlers:</label><input name="settings[domainr_crawlers]" placeholder="crawlers" id="domainr_crawlers" type="text" class="" value="<?php echo $this->getValue( $settings, 'domainr_crawlers' ) ?>">
			<label for="domains_per_request">Domains per request:</label><input name="settings[domains_per_request]" placeholder="domains per request" id="domains_per_request" type="text" class="" value="<?php echo $this->getValue( $settings, 'domains_per_request' ) ?>">
			<label for="refresh_whois">Run domainr every (days):</label><input name="settings[refresh_whois]" placeholder="Refresh every (days)" id="refresh_whois" type="text" class="" value="<?php echo $this->getValue( $settings, 'refresh_whois' ) ?>">
		</div>
		<div class="setting col-sm-3">
			<h4 class="header">DomainTools Whois Expiry Date</h4>
			<label for="domaintools_crawlers">Crawlers:</label><input name="settings[domaintools_crawlers]" placeholder="7" id="domaintools_crawlers" type="text" class="" value="<?php echo $this->getValue( $settings, 'domaintools_crawlers' ) ?>">	
			<label for="domaintools_threads">Threads:</label><input name="settings[domaintools_threads]" placeholder="7" id="domaintools_threads" type="text" class="" value="<?php echo $this->getValue( $settings, 'domaintools_threads' ) ?>">	
			<label for="min_da_check_expiry_date">Min DA to check whois expiry date:</label><input name="settings[min_da_check_expiry_date]" placeholder="20" id="min_da_check_expiry_date" type="text" class="" value="<?php echo $this->getValue( $settings, 'min_da_check_expiry_date' ) ?>">
			<label for="check_days_before_expiry">Number of days before expiry date to update date:</label><input name="settings[check_days_before_expiry]" placeholder="7" id="check_days_before_expiry" type="text" class="" value="<?php echo $this->getValue( $settings, 'check_days_before_expiry' ) ?>">
		</div>
		<div class="setting col-sm-3">
			<h4 class="header">MOZ api</h4>
			<label for="moz_api_key">Api keys (AccessID:SecretKey per line):</label><textarea  name="settings[moz_api_key]" placeholder="MOZ api key" id="moz_api_key" type="text" class=""><?php echo $this->getValue( $settings, 'moz_api_key' ) ?></textarea>
			<label for="refresh_moz">Refresh every (days):</label><input name="settings[refresh_moz]" placeholder="Refresh every (days)" id="refresh_moz" type="text" class="" value="<?php echo $this->getValue( $settings, 'refresh_moz' ) ?>">
		</div>
		<div class="setting col-sm-3">
			<h4 class="header">Majestic</h4>
			<label for="min_moz_da">Min DA to use Magestic:</label><input name="settings[min_moz_da]" placeholder="Min DA to use Magestic" id="min_moz_da" type="text" class="" value="<?php echo $this->getValue( $settings, 'min_moz_da' ) ?>">
			<label for="ms_username">Username:</label><input name="settings[ms_username]" placeholder="Majestic username" id="ms_username" type="text" class="" value="<?php echo $this->getValue( $settings, 'ms_username' ) ?>">
			<label for="ms_password">Password:</label><input name="settings[ms_password]" placeholder="Majestic password" id="ms_password" type="text" class="" value="<?php echo $this->getValue( $settings, 'ms_password' ) ?>">
			<label for="ms_wait_time">Wait before request (seconds):</label><input name="settings[ms_wait_time]" placeholder="Majestic wait time" id="ms_wait_time" type="text" class="" value="<?php echo $this->getValue( $settings, 'ms_wait_time' ) ?>">
			<label for="refresh_majestic">Refresh every (days):</label><input name="settings[refresh_majestic]" placeholder="Refresh every (days)" id="refresh_majestic" type="text" class="" value="<?php echo $this->getValue( $settings, 'refresh_majestic' ) ?>">
		</div>
	</div>
</form>
<div class="clearfix"></div>
<div class="row">
	<div class="col-sm-2 ">
		<input type="button" class="btn btn-primary" id="settings-save" value="Save">
	</div>
</div>


<script>
	$( '#settings-save' ).click( function() {
		$.post( '?act=SaveSettings', $( '.settings-form' ).serialize(), function() {
			alert( "Settings updated" )
		} )
	} )
</script>
