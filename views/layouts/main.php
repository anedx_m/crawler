<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Settings</title>
        <script type="text/javascript" src="assets/js/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="assets/jquery-ui/jquery-ui.min.js"></script>
		<link rel="stylesheet" type="text/css" href="assets/jquery-ui/jquery-ui.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/main.css">
		<link rel="stylesheet" type="text/css" href="assets/font-awesome/css/font-awesome.min.css">
		<script type="text/javascript" src="assets/js/Table.js"></script>
		<script type="text/javascript" src="assets/js/callback.js"></script>
		<script type="text/javascript" src="assets/js/afterEvents.js"></script>
        <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="assets/js/bootbox.min.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
    </head>    
    <body>
		<?php 
			$this->renderPartial($view, $param);
		?>
    </body>
</html>



