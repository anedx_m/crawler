<ul class="nav nav-tabs main-tabs">
	<li class="active"><a href="#Jobs">Jobs</a></li>
	<li><a href="#Proxies">Proxies</a></li>
	<li><a href="#Useragents">Useragents</a></li>
	<li><a href="#ExludeDomains">Exclude Domains</a></li>
	<li><a href="#Domains">Domains All</a></li>
	<li><a href="#DomainsOffline">Domains Offline</a></li>
	<li><a href="#Settings">Settings</a></li>
	
</ul>
<div class="tab-content main-tab-content">
    <div class="tab-pane active" id="Jobs"></div>
    <div class="tab-pane" id="Proxies"></div>
	<div class="tab-pane" id="Useragents"></div>
	<div class="tab-pane" id="ExludeDomains"></div>
	<div class="tab-pane" id="Domains"></div>
	<div class="tab-pane" id="DomainsOffline"></div>
	<div class="tab-pane" id="Settings"></div>
</div>
<script>
	$( document ).ready( function() {
		$( '.main-tabs a' ).click( function(e) {
			e.preventDefault();
			var action = $( this ).attr( 'href' ).substr( 1 );
			var that = this
			$('.main-tab-content div').empty();
			$( this ).tab( 'show' )
			$.post( '?act=' + action, '', function( data ) {
				$('.main-tab-content div.active').html(data)
			},'HTML')
		} )
		$( '.main-tabs .active a' ).click();

	} )
</script>