<form class="DomainsOffline-params">
	<div class="row">
		<div class="col-lg-3">
			<div class="padding-10">
				<input id="search-input" class="input-sm search-input" placeholder="search" name="search">
				<input id="http_code" placeholder="= HTTP CODE" class="input-sm" name="http_code">
			</div>
		</div>
		<div class="col-lg-3">
			<div class="padding-10">
			Available since
				<input id="date_from" class="input-sm date_from" placeholder="Date from" name="date_from">
				<input id="date_to" class="input-sm date_to" placeholder="Date to" name="date_to">
			</div>
		</div>
		<div class="col-lg-4">
			<div class="padding-10">
				<input id="available" type="checkbox" class="checkbox-inline" name="available">&nbsp;<label for="available">Display only available</label>
			</div>

		</div>
		<div class="col-lg-2">
			<div class="export-DomainsOffline padding-10" style="float: right;">
				<label for="max_rows">Max rows:&nbsp;</label><input type="text" id="max_rows" class="input-50">
				<input id="export-DomainsOffline" type="button" class="btn btn-primary" value="Export">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-2">
			<div class="padding-10">

			</div>
		</div>
		<div class="col-lg-3">
			<div class="padding-10">

			</div>
		</div>
		<div class="col-lg-7">
			<div class="padding-10">
<!--				<input id="search-domain" type="button" class="btn btn-default" value="search">-->
			</div>
		</div>
	</div>
</form>
<div class="DomainsOffline-table"></div>

<script>
	var ex_DomainsOffline = new Table( '?act=ListDomainsOffline', '.DomainsOffline-table' );

	$( document ).ready( function() {
		$( '#date_from,#date_to' ).datepicker( { dateFormat: 'yy-mm-dd' } );
		ex_DomainsOffline.list( { order_by: 'date_found', direction: 'DESC' } )
		$( '#filter-domain' ).click( function() {
			var params = getParams();
			ex_DomainsOffline.list( params, 1 );
		} )
		$( '#export-DomainsOffline' ).click( function() {
			var params = getParams();
			params.max_row = $( '#max_rows' ).val( );
			var order_by = $( '.DomainsOffline-table' ).find( '.order_by' ).val()
			var direction = $( '.DomainsOffline-table' ).find( '.direction' ).val()
			document.location = '?act=ExportDomainsOffline&' + $.param( { 'params': params } ) + '&' + $.param( { 'order_by': order_by, 'direction': direction } );
		} )
	} )
	function search() {
		var params = getParams();
		ex_DomainsOffline.list( params, 1 );
	}
	function getParams() {
		var inps = $( '.DomainsOffline-params input' );
		var params = { };
		$.each( inps, function( i, el ) {
			var name = $( el ).attr( 'name' )
			var type = $( el ).attr( 'type' )
			if ( name ) {
				if ( type == 'checkbox' ) {
					params[name] = $( el ).is( ':checked' )
				} else
					params[name] = $( el ).val();
			}
		} )
		console.log( params )
		return params;
	}
	$( '.DomainsOffline-params input:not([type=checkbox],.date_from,.date_to)' ).afterKeyDown( function( ev ) {
		search()
	}, 1000, true );
	$( '.DomainsOffline-params input' ).afterChange( function( ev ) {
		search()
	}, 1000, true );
</script>