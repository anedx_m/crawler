<?php
set_time_limit( 0 );
include dirname( dirname( __FILE__ ) ) . "/config.php";
Helper::system_down_check();

$db		 = DB::getInstance();
$process = new Process( $db );
$process->removeDeadProcesses( Helper::getSetting( "max_exec_time" ) );

$type	 = basename( __FILE__ );
//check # of live 
$r		 = $db->query( 'SELECT COUNT(*) FROM moz_keys' );
$max_bot = $r->fetchColumn();

if ( $process->getCountLive( $type ) >= $max_bot ) {
	die( $max_bot .' {moz api} bots aready run!' );
}

$pid = md5( time() . mt_rand( 0, 1000000 ) );
$process->addProcess( $pid, $type );

$MozApi	 = new MozAPI();
$limit	 = 10;// max allowed by free API
$last_run = time();

while ( $all_domains	= $MozApi->getNextDomains( 100 ) ) {
	while($domains	= array_splice($all_domains,0,$limit)) {
		var_dump(count($all_domains));
		var_dump(count($domains));
		
		$MozApi->processNextDomains( $limit,$domains );
		$process->updateTime( $pid );
//		$process->endProcess( $pid ); die("debug 1");
// 		echo "count = $pr";
		// wait 10 seconds 
		while(time() - $last_run < 10) 
			sleep(1);
		$last_run = time();	
		Helper::system_down_check();
	}	
// 	die("1");
}

$process->endProcess( $pid );
die( "Done" );