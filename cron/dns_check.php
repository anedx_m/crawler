<?php
set_time_limit( 0 );
include dirname( dirname( __FILE__ ) ) . '/config.php';

$db		 = DB::getInstance();
$process = new Process( $db );
$process->removeDeadProcesses( Helper::getSetting( "max_exec_time" ) );

Helper::system_down_check();

$dig_crawlers	 = Helper::getSetting( "dig_crawlers" );
$type = basename( __FILE__ );
if ( $process->getCountLive( $type ) >= $dig_crawlers ) {
	die( $dig_crawlers.' {' . $type . '} bots aready run!' );
}

$pid = md5( time() . mt_rand( 0, 1000000 ) );
$process->addProcess( $pid, $type );

$limit	= Helper::getSetting( "domains_per_request" );
$dig = new DigExec();
while ($domains = $dig->getNextDomains( $limit ) ) {
	foreach($domains  as $domain) {
		$dig->checkDNS($domain[ 'domain' ]);
		$process->updateTime( $pid );
		Helper::system_down_check();
	}
}

$process->endProcess( $pid );
die( "Done" );

