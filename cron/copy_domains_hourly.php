<?php
include dirname( dirname( __FILE__ ) ) . "/config.php";

Helper::system_down_check(); //stop 

$db = DB::getInstance();
$db->exec( 'INSERT  INTO domains_all(domain,tld,date_found)  SELECT domain,tld,date_found FROM domains_job' );

$settings			 = Helper::getOption( 'settings' );

// remove domains by tld
$skip_tlds  = explode(" ",Helper::getValue( $settings, 'offlinecheck_skip_tlds' ) );
$skip_tlds = array_filter( array_map("trim",$skip_tlds) );
if($skip_tlds) {
	$skip_tlds = join("','",$skip_tlds);
	$db->query("DELETE FROM domains_offline WHERE tld IN ('$skip_tlds')");
}

// remove domains by name
$skip_names  = explode(" ",Helper::getValue( $settings, 'offlinecheck_skip_names' ) );
$skip_names = array_filter( array_map("trim",$skip_names) );
foreach($skip_names  as $name )
	$db->query("DELETE FROM domains_offline WHERE domain LIKE '%$name%'");
	
die( "Done" );
?>
