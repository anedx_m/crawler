<?php
set_time_limit( 0 );
include dirname( dirname( __FILE__ ) ) . "/config.php";
Helper::system_down_check();

$db		 = DB::getInstance();
$process = new Process( $db );
$process->removeDeadProcesses( Helper::getSetting( "max_exec_time" ) );

$type = basename( __FILE__ );
if ( $process->getCountLive( $type ) >= 1 ) {
	die( '{majestic} bots aready run!' );
}

$pid = md5( time() . mt_rand( 0, 1000000 ) );
$process->addProcess( $pid, $type );

//$MajesticAPI	 = new MajesticAPI();
$MajesticAPI = new MajesticScraper();

// get wait settings
$wait		 = Helper::getSetting( 'ms_wait_time' );
$a_wait		 = explode( '-', $wait );
$min		 = trim( $a_wait[ 0 ] );
if ( empty( $a_wait[ 1 ] ) )
	$a_wait[ 1 ] = $min;
$max		 = trim( $a_wait[ 1 ] );

while ( $pr		 = $MajesticAPI->processNextDomain(  ) ) {
	$process->updateTime( $pid );
	
	$sec = rand( $min, $max );
	echo "Wait $sec";
	for($i=1;$i<=$sec;$i++) {
		sleep( 1 );
		Helper::system_down_check();
	}	
}

$process->endProcess( $pid );
die( "Done" );
