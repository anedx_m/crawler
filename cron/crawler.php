<?php
//ignore_user_abort(true);
set_time_limit( 0 );
include dirname( dirname( __FILE__ ) ) . "/config.php";
Helper::system_down_check();

//no jobs
$job = Helper::get_current_job();
if ( !$job )
	die( "no running jobs" );

//remove expired bots
$type	 = basename( __FILE__ );

$db		 = DB::getInstance();
$process = new Process( $db );
$process->removeDeadProcesses( Helper::getSetting( "max_exec_time" ) );

//check # of live 
if ( $process->getCountLive( $type ) >= $job[ 'crawlers' ] ) {
	die( $job[ 'crawlers' ] . " bots aready run!" );
}

// add itself
$pid	 = md5( time() . mt_rand( 0, 1000000 ) );
$process->addProcess( $pid, $type );

//run 
ini_set('memory_limit','2048M'); //for arrays
$scraper = new Crawler();
while ( $scraper->processJobDomains( $job, $process,$pid ) ) {
	$process->updateTime( $pid );
	Helper::system_down_check();

	//reread job settings
	$job = Helper::get_current_job();
	if ( !$job )
		break;
}

//done 
$process->endProcess( $pid );
die( "Done" );


//only for test
function testX() {
	sleep( 3 );
	return true;
}
?>