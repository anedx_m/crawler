<?php
set_time_limit( 0 );
include dirname( dirname( __FILE__ ) ) . '/config.php';

$db		 = DB::getInstance();
$process = new Process( $db );
$process->removeDeadProcesses( Helper::getSetting( "max_exec_time" ) );

Helper::system_down_check();

$domainr_crawlers = Helper::getSetting( "domainr_crawlers" );
$type = basename( __FILE__ );
if ( $process->getCountLive( $type ) >= $domainr_crawlers  ) {
	die( $domainr_crawlers .' {domainr_avail} bots aready run!' );
}

$pid = md5( time() . mt_rand( 0, 1000000 ) );
$process->addProcess( $pid, $type );

$DomainrAPI		 = new DomainrAPI();
$limit = Helper::getSetting( 'domains_per_request' );

while ( $pr		 = $DomainrAPI->processNextDomains( $limit ) ) {
	$process->updateTime( $pid );
	Helper::system_down_check();
	echo "wait 1 sec\n";
	sleep( 1 );
}

$process->endProcess( $pid );
die( "Done" );
