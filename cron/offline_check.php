<?php
set_time_limit( 0 );
include dirname( dirname( __FILE__ ) ) . "/config.php";
Helper::system_down_check();

$db		 = DB::getInstance();
$process = new Process( $db );
$type	 = basename( __FILE__ );
$process->removeDeadProcesses( Helper::getSetting( "max_exec_time" ) );

//check # of live 
$max_bots = Helper::getSetting( "offlinecheck_crawlers" );
if ( $process->getCountLive( $type ) >= $max_bots ) {
	die( $max_bots . ' {offline check} bots aready run!' );
}

$pid = md5( time() . mt_rand( 0, 1000000 ) );
$process->addProcess( $pid, $type );

$OfflineCheck = new OfflineCheck();

while ( $f = $OfflineCheck->processJobNextUrls() ) {
	$process->updateTime( $pid );

	Helper::system_down_check();

	//$process->endProcess( $pid );die("debug");
}
//done 
$process->endProcess( $pid );
die( "Done" );
?>