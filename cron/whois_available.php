<?php
set_time_limit( 0 );
include dirname( dirname( __FILE__ ) ) . "/config.php";
Helper::system_down_check();

$db		 = DB::getInstance();
$process = new Process( $db );
$process->removeDeadProcesses( Helper::getSetting( "max_exec_time" ) );

$type	 = basename( __FILE__ );
if ( $process->getCountLive( $type ) >= 1 ) {
	die( '1 {whois_avail} bots aready run!' );
}

$pid = md5( time() . mt_rand( 0, 1000000 ) );
$process->addProcess( $pid, $type );

$WhoisAPI	 = new WhoisAPIAvailable();
$limit	 = 1;
while ( $pr		 = $WhoisAPI->processNextDomains( $limit ) ) {
	$process->updateTime( $pid );
	echo "count = $pr";
	
	// wait 1.2 seconds  = 50 requests per minute
	usleep(1200000);
	
	Helper::system_down_check();
}

//debug $pr		 = $WhoisAPI->processNextDomains( $limit );



$process->endProcess( $pid );
die( "Done" );
