<?php
include dirname( dirname( __FILE__ ) ) . "/config.php";
$db = DB::getInstance();

//0 kill processes
$process = new Process( $db );
$process->removeDeadProcesses( Helper::getSetting( "max_exec_time" ) );

Helper::system_down_check(); //stop 

// have unfinished ?
$job	 = Helper::get_current_job();
if ( $job	) {
	if($job	['only_domain']) {
		$pr	 = $db->prepare( 'SELECT COUNT(*) as total FROM domains_job WHERE parsed!=2 AND depth=0' );
		$r	 = $pr->execute();
	 } else {
		$pr	 = $db->prepare( 'SELECT COUNT(*) as total FROM domains_job WHERE parsed!=2 AND depth<:depth' );
		$r	 = $pr->execute( array(':depth'=>$job['depth']));
	}	
	$row = $pr->fetch( PDO::FETCH_NAMED );
	if($row['total']) {
		// restart domains belong to dead bots AFTER 1 min
		$count	 = $db->exec( 'UPDATE domains_job SET live_at_time=0,parsed=0  WHERE parsed=1 AND live_at_time<>0 AND extract(epoch from now())-live_at_time>=60' );
		if ( $count )
			echo "$count domains unlocked<br>\n";
	}		
	else {
		// mark current job as finished 
		Helper::finish_job($db);
		$job = false;
	}	
	
}

if(!$job)
	$job = Helper::start_new_job( $db );
print_r($job );	
	
//2 start bots
if ( $job ) {
	$to_run	 = $job[ 'crawlers' ] - $process->getCountLive('crawler.php');
	if ( $to_run ) {
		echo "try start $to_run  crawlers<br>\n";

		$cmd		 = '/usr/bin/php ' . dirname( __FILE__ ) . '/crawler.php';
		$outputfile	 = '/dev/null';
		$pidfile	 = '/dev/null';
		for ( $i = 0; $i < $to_run; $i++ ) {
			$cmd0 = $cmd . ' ' . $i;
			//debug 			
			exec( sprintf( "%s > %s 2>&1 & echo $! >> %s", $cmd0, $outputfile, $pidfile ) );
		}
	}
}

die( "Done" );
?>
