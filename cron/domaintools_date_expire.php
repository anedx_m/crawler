<?php
set_time_limit( 0 );
include dirname( dirname( __FILE__ ) ) . "/config.php";
Helper::system_down_check();

$db		 = DB::getInstance();
$process = new Process( $db );
$process->removeDeadProcesses( Helper::getSetting( "max_exec_time" ) );

$domaintools_crawlers	 = Helper::getSetting( "domaintools_crawlers" );
$type					 = basename( __FILE__ );
if ( $process->getCountLive( $type ) >= $domaintools_crawlers ) {
	die( '1 {whois_date_expire} bots aready run!' );
}

$pid = md5( time() . mt_rand( 0, 1000000 ) );
$process->addProcess( $pid, $type );

$WhoisAPI = new DomaintoolsAPI();

$domaintools_crawlers = Helper::getSetting( "domaintools_threads" );

while ( $pr = $WhoisAPI->processNextDomains( $domaintools_crawlers ) ) {
	$process->updateTime( $pid );
	echo "count = $pr";

	// wait 7.5 seconds  = 8 requests per minute
	Helper::system_down_check();
	echo "-----------\n";
	sleep( 3 );
}
//debug $pr		 = $WhoisAPI->processNextDomains( $limit );

$process->endProcess( $pid );
die( "Done" );
