<?php
// UPDATE domains_all SET offline_check_date=null
set_time_limit( 0 );
include dirname( dirname( __FILE__ ) ) . "/config.php";
Helper::system_down_check();

$db		 = DB::getInstance();
$process = new Process( $db );
$type	 = basename( __FILE__ );
$process->removeDeadProcesses( Helper::getSetting( "max_exec_time" ) );
//check # of live 
if ( $process->getCountLive( $type ) >= 1 ) {
	die( '1 {offline check} bots aready run!' );
}

$pid = md5( time() . mt_rand( 0, 1000000 ) );
$process->addProcess( $pid, $type );

$ProxyCheck		 = new ProxyCheck();
$test_url		 = 'www.yahoo.com';
$number_threads	 = 100;
while ( $f				 = $ProxyCheck->processNextProxies( $test_url, $number_threads ) ) {
	$process->updateTime( $pid );
	Helper::system_down_check();
}
//done 
$process->endProcess( $pid );
die( "Done" );
?>