<?php

class DomaintoolsAPI extends MultiCurl {

	public function __construct( $curl_options = array(), $connection_timeout = 30, $max_page_size = 1024100 ) {
		$db										 = DB::getInstance();
		$connection_timeout						 = Helper::getSetting( 'connection_timeout' );
		$max_page_size							 = Helper::getSetting( 'max_page_size' );
		parent::__construct( $curl_options, $connection_timeout, $max_page_size );
		$this->query_select_random_proxy		 = $db->prepare( 'SELECT * FROM proxies ORDER BY random() LIMIT 1;' );
		$this->query_next_get_domains_offline	 = $db->prepare( "
		UPDATE domains_offline SET whois_check_expire_date = current_date
			FROM  (    SELECT id   FROM   domains_offline WHERE whois_check_expire_date IS NULL AND moz_da>:moz_da LIMIT :limit FOR UPDATE) sub
			WHERE  domains_offline.id = sub.id
		RETURNING domains_offline.domain as url, domains_offline.id as domain_id" ); //debug 

		$interval = Helper::getSetting( 'check_days_before_expiry' );

		$this->query_next_get_domains_all = $db->prepare( "
		UPDATE domains_all SET whois_check_expire_date = current_date
			FROM  (    SELECT id   FROM   domains_all WHERE whois_available<1 AND whois_expire_date IS NOT NULL AND (age(whois_expire_date, current_date)<interval '" . $interval . " days') AND moz_da>:moz_da LIMIT :limit FOR UPDATE) sub
			WHERE  domains_all.id = sub.id
		RETURNING domains_all.domain as url, domains_all.id as domain_id" ); //debug 
	}

	public function getNextDomains( $limit ) {
		$db				 = Db::getInstance();
		$refresh_whois	 = Helper::getSetting( 'refresh_whois' );
		$moz_da			 = Helper::getSetting( 'min_da_check_expiry_date' );
		$r				 = $this->query_next_get_domains_offline->execute( array( ':limit' => $limit, ':moz_da' => $moz_da ) );
		$start_urls		 = $this->query_next_get_domains_offline->fetchAll( PDO::FETCH_NAMED );
		if ( !count( $start_urls ) ) {
			echo "domains all\n";
			$r			 = $this->query_next_get_domains_all->execute( array( ':limit' => $limit, ':moz_da' => $moz_da ) );
			$start_urls	 = $this->query_next_get_domains_all->fetchAll( PDO::FETCH_NAMED );
		}
		return $start_urls;
	}

	public function processNextDomains( $limit ) {
		$domains = $this->getNextDomains( $limit );

		$urls = array();
		foreach ( $domains as $domain ) {
			$domain_url	 = $domain[ 'url' ];
			$urls[]		 = array(
				'url'		 => $this->getDomaintoolsUrl( $domain_url ),
				'domain_id'	 => $domain[ 'domain_id' ],
				'domain'	 => $domain_url,
			);
		}
		var_dump( $urls );
		if ( count( $urls ) ) {
			$this->start( $urls, array( $this, 'handler' ), TRUE );
		}

		return count( $domains );
	}

	protected function handler( $content, $curl_info, $curl_multi_info, $urls_params ) {
		echo '--------';
		$db = DB::getInstance();
		if ( $curl_info[ 'http_code' ] == 200 ) {
			
			$pattern_not_registered = '/This domain is not registered/';
			preg_match( $pattern_not_registered, $content, $not_registered );

			$not_registered = isset( $not_registered[ 1 ] ) ? true : false;

			if ( $not_registered ) {
				$pr	 = $db->prepare( "UPDATE domains_offline SET whois_available=1, whois_check_expire_date=current_date, whois_expire_date=NULL,whois_check_avail_date=current_date,whois_available_since_date = COALESCE(whois_available_since_date,current_date) WHERE domain=:domain" );
				$pr->execute( array( ':domain' => $domain ) );
				$pr	 = $db->prepare( "UPDATE domains_all SET whois_available=1, whois_expire_date=NULL, whois_check_expire_date=current_date WHERE domain=:domain" );
				$pr->execute( array( ':domain' => $domain ) );
			} else {
				$pattern_expires = '/Expires on (\d+-\d+-\d+)/';
				preg_match( $pattern_expires, $content, $expires );

				$pattern_created = '/Created on (\d+-\d+-\d+)/';
				preg_match( $pattern_created, $content, $created );

				$whois_expire_date = isset( $expires[ 1 ] ) ? strtotime( $expires[ 1 ] ) : null;

				if ( !$whois_expire_date AND isset( $created[ 1 ] ) ) {
					$t_cur_year	 = date( 'Y' );
					$t_cur_date	 = strtotime( date( 'Y-m-d' ) );
					$t_creadted	 = strtotime( $created[ 1 ] );
					$t			 = strtotime( $t_cur_year . '-' . date( 'm', $t_creadted ) . '-' . date( 'd', $t_creadted ) );
					$t2			 = strtotime( date( 'Y-m-d', $t ) . " + 1 year" );
					if ( ($t_cur_date - $t) > 0 ) {
						$whois_expire_date = $t2;
					} else {
						$whois_expire_date = $t;
					}
				}

				$domain = $urls_params[ 'domain' ];

				if ( $whois_expire_date ) {
					$whois_expire_date	 = date( 'Y-m-d', $whois_expire_date );
					$pr					 = $db->prepare( "UPDATE domains_offline SET whois_expire_date=:whois_expire_date, whois_check_expire_date=current_date WHERE domain=:domain" );
					$pr->execute( array( ':domain' => $domain, ':whois_expire_date' => $whois_expire_date ) );
					$pr					 = $db->prepare( "UPDATE domains_all SET whois_expire_date=:whois_expire_date, whois_check_expire_date=current_date WHERE domain=:domain" );
					$pr->execute( array( ':domain' => $domain, ':whois_expire_date' => $whois_expire_date ) );
				} else {
					$pr	 = $db->prepare( "UPDATE domains_offline SET  whois_expire_date=NULL, whois_check_expire_date=current_date WHERE domain=:domain" );
					$pr->execute( array( ':domain' => $domain ) );
					$pr	 = $db->prepare( "UPDATE domains_all SET whois_expire_date=NULL, whois_check_expire_date=current_date WHERE domain=:domain" );
					$pr->execute( array( ':domain' => $domain, ) );
				}
			}
		} else {
			var_dump( $curl_info );
			$domain	 = $urls_params[ 'domain' ];
			$pr		 = $db->prepare( "UPDATE domains_offline SET whois_check_expire_date=NULL WHERE domain=:domain" );
			$pr->execute( array( ':domain' => $domain ) );
			$pr		 = $db->prepare( "UPDATE domains_all SET  whois_check_expire_date=NULL WHERE domain=:domain" );
			$pr->execute( array( ':domain' => $domain ) );
		}
		echo '--------';
	}

	protected function getProxy() {
		$this->query_select_random_proxy->execute();
		$r			 = $this->query_select_random_proxy->fetch( PDO::FETCH_NAMED );
		$address	 = Helper::getValue( $r, 'address' );
		$username	 = Helper::getValue( $r, 'username' );
		$password	 = Helper::getValue( $r, 'password' );

		$curl_proxy_opt = array(
			CURLOPT_PROXY => $address,
		);
		if ( $username and $password ) {
			$curl_proxy_opt[ CURLOPT_HTTPAUTH ]		 = CURLAUTH_BASIC;
			$curl_proxy_opt[ CURLOPT_PROXYUSERPWD ]	 = "$username:$password";
		}
		return $curl_proxy_opt;
	}

	protected function getRandomUseragent() {
		$useragents = Helper::getOption( 'useragents' );
		if ( $useragents ) {
			$c = count( $useragents );
			if ( $c ) {
				$n = mt_rand( 0, $c - 1 );
				return $useragents[ $n ];
			}
		}
		return '';
	}

	public function getDomaintoolsUrl( $domain ) {
		return "http://whois.domaintools.com/$domain";
	}

}
