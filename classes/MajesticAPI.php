<?php

class MajesticAPI extends MultiCurl {

	public function __construct( $curl_options = array(), $connection_timeout = 30, $max_page_size = 1024100 ) {
		$db					 = DB::getInstance();
		$connection_timeout	 = Helper::getSetting( 'connection_timeout' );
		$max_page_size		 = Helper::getSetting( 'max_page_size' );
		parent::__construct( $curl_options, $connection_timeout, $max_page_size );
	}

	public function getNextDomains( $limit ) {
		$db			 = Db::getInstance();
		$moz_da		 = 0;
		$pr			 = $db->prepare( "
		UPDATE domains_all SET whois_date = NULL
			FROM  (    SELECT id   FROM   domains_offline WHERE  whois_date IS NULL AND moz_da>:moz_da LIMIT :limit FOR UPDATE) sub
			WHERE  domains_all.id = sub.id
		RETURNING domains_all.domain as url, domains_all.id as domain_id" ); //debug 
		$r			 = $pr->execute( array( ':limit' => $limit, ':moz_da' => $moz_da ) );
		$start_urls	 = $pr->fetchAll( PDO::FETCH_NAMED );

		return $start_urls;
	}

	public function processNextDomains( $limit ) {
		$domains	 = $this->getNextDomains( $limit );
		$ms_api_key	 = Helper::getSetting( 'ms_api_key' );

		if ( count( $domains ) ) {
			$urls = array( 0 => array(
					'url' => $this->getMajesticUrl( $domains, $ms_api_key )
				) );
			$this->start( $urls, array( $this, 'handler' ), FALSE );
		}

		return count( $domains );
	}

	protected function handler( $content, $curl_info, $curl_multi_info, $urls_params ) {
		echo '--------';
		if ( $curl_info[ 'http_code' ] == 200 ) {
			$db		 = DB::getInstance();
			$data	 = json_decode( $content, true );
			var_dump( $data );
//			if ( $data[ 'status' ] == 0 AND isset( $data[ 'date_expires' ] ) ) {
//				$whois_date_end	 = $data[ 'date_expires' ];
//				$domain_id		 = $urls_params[ 'domain_id' ];
//				$domain			 = $urls_params[ 'domain' ];
//
//				$pr = $db->prepare( "UPDATE domains_all SET whois_date_end=:whois_date_end, whois_date=current_date WHERE id=:domain_id" );
//				$pr->execute( array( ':domain_id' => $domain_id, ':whois_date_end' => $whois_date_end ) );
//
//				$pr = $db->prepare( "UPDATE domains_offline SET whois_date_end=:whois_date_end, whois_date=current_date WHERE domain=:domain" );
//				$pr->execute( array( ':domain' => $domain, ':whois_date_end' => $whois_date_end ) );
//			}
		} else {
			var_dump( $curl_info );
		}
		echo '--------';
	}

	public function getMajesticUrl( $domains, $key ) {
		$arr_d	 = array();
		$i		 = 0;
		foreach ( $domains as $domain ) {
			$arr_d[ 'item' + $i ] = $domain[ 'url' ];
			$i++;
		}
		$items = http_build_query( $arr_d );
		return "http://api.majestic.com/api/json?app_api_key=$key&cmd=GetIndexItemInfo&items=$i&$items&datasource=fresh";
	}

}
