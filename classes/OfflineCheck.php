<?php

class OfflineCheck extends MultiCurl {

	public function __construct( $curl_options = array() ) {
		$this->log_folder = dirname(__FILE__)."/../_logs/";
	
		$settings			 = Helper::getOption( 'settings' );
		$connection_timeout	 = Helper::getValue( $settings, 'offlinecheck_connection_timeout' );
		$max_page_size		 = 10; // get first 10k
		parent::__construct( $curl_options, $connection_timeout, $max_page_size );

//		$this->initTLDs();

		$db								 = Db::getInstance();
		$this->query_update_all			 = $db->prepare( "UPDATE domains_all SET http_code=:http_code,offline_check_date = current_date WHERE domain=:domain" );
		$this->query_update_off			 = $db->prepare( "UPDATE domains_offline  SET http_code=:http_code,offline_check_date = current_date WHERE domain=:domain" );
		
 		$this->query_insert_offline		 = $db->prepare( 'INSERT INTO domains_offline (domain,tld,http_code,date_found,moz_da,nameserver_exists) VALUES (:domain,:tld,:http_code,current_date,:moz_da,1)' );
		$this->query_delete_offline		 = $db->prepare( 'DELETE FROM domains_offline WHERE domain = :domain' );
		$this->query_select_random_proxy = $db->prepare( 'SELECT * FROM proxies ORDER BY random() LIMIT 1' );
		
		$this->skip_tlds_sql = '';
		$skip_tlds  = explode(" ",Helper::getValue( $settings, 'offlinecheck_skip_tlds' ) );
		$skip_tlds = array_filter( array_map("trim",$skip_tlds) );
		if($skip_tlds) {
			$skip_tlds = join("','",$skip_tlds);
			$this->skip_tlds_sql = " AND tld NOT IN ('$skip_tlds')";
		}
	}

	public function processJobNextUrls() {
		$max_threads = Helper::getSetting( 'offlinecheck_threads' );
		$days_ago	 = Helper::getSetting( 'offlinecheck_days_ago' );


		$t_urls = $this->getNextDomains( $max_threads, $days_ago );
		$this->start( $t_urls, array( $this, 'handler' ) );
		return count( $t_urls ) > 0;
	}
	

	function handler( $content, $curl_info, $curl_multi_info, $t_domains ) {
		echo "------------- START ------------";
		
		$proxy = '';
		if ( isset( $t_domains[ 'curl_proxy_opt' ] ) ) {
			var_dump( $t_domains[ 'curl_proxy_opt' ] );
			$proxy = $t_domains[ 'curl_proxy_opt' ][CURLOPT_PROXY];
		}
		$ch = $curl_multi_info[ 'handle' ];

		$domain_id	 = $t_domains[ 'domain_id' ];
		$domain		 = $t_domains[ 'domain' ];
		$tld		 = $t_domains[ 'tld' ];
		$http_code	 = $curl_info[ 'http_code' ];
		
		
		$c			 = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
		echo " -=-= http_code $c =-=-=-=- ";
		echo "$http_code $domain<br>\n";
		
		//fclose($t_domains['log_file_handle' ]); // debugLog

		$http_codes = array( 200, 301, 302 );
		if ( !in_array( $http_code, $http_codes ) ) {
			
			if($t_domains[ 'try_num' ] < 1 AND  $http_code==0) {
				echo "---------=- RETRY -=-----------";
				$t_domains[ 'try_num' ] ++;
				$this->addUrlToQueue( $t_domains[ 'url' ], $t_domains, true);
			}
			elseif($this->ping80($domain)) {
				echo "---------=- DELETE(80 port is alive! ) -=-----------";
				$this->query_delete_offline->execute( array( ':domain' => $domain ) );
				//die($domain );
			}
			else
			{
				echo "---------=- INSERT -=-----------";
				$this->query_insert_offline->execute( array( ':domain' => $domain, ':tld' => $tld,  ':http_code' => $http_code, ':moz_da'=>$t_domains['moz_da'] ) );
			}	
		} else {
			//unlink($t_domains['log_file']);//debugLog 
			echo "---------=- DELETE -=-----------";
			$this->query_delete_offline->execute( array( ':domain' => $domain ) );
		}
		//
		$this->query_update_off->execute( array( ':domain' => $domain, ':http_code' => $http_code ) );
		$this->query_update_all->execute( array( ':domain' => $domain, ':http_code' => $http_code ) );
		
		echo "------------- END ------------";
	}
	


	function getNextDomains( $limit = 10, $days_ago = 10 ) {
		$db			 = DB::getInstance();
		
		//#1 test all new 
		$q_domains	 = $db->query( "UPDATE domains_all SET offline_check_date = current_date
			FROM  (SELECT id FROM domains_all WHERE (offline_check_date IS NULL) {$this->skip_tlds_sql} LIMIT $limit FOR UPDATE) sub
			WHERE  domains_all.id = sub.id
		RETURNING  concat('http://',domains_all.domain) as url,domains_all.domain as domain, domains_all.id as domain_id, domains_all.tld as tld, 0 as try_num,  domains_all.moz_da as moz_da" );
		$results = $q_domains->fetchAll( PDO::FETCH_NAMED );
		if(!empty($results))
			return $results;
			
		//#2 test offline 
		$q_domains	 = $db->query( "UPDATE domains_offline SET offline_check_date = current_date
			FROM  (SELECT id FROM domains_offline WHERE (offline_check_date IS NULL OR age(current_date,offline_check_date) > interval '$days_ago days') {$this->skip_tlds_sql} LIMIT $limit FOR UPDATE) sub
			WHERE  domains_offline.id = sub.id
		RETURNING  concat('http://',domains_offline.domain) as url,domains_offline.domain as domain, domains_offline.id as domain_id, domains_offline.tld as tld, 0 as try_num, domains_offline.moz_da as moz_da" );
		$results = $q_domains->fetchAll( PDO::FETCH_NAMED );
		if(!empty($results))
			return $results;
			
		//#3 test all	by age
		$q_domains	 = $db->query( "UPDATE domains_all SET offline_check_date = current_date
			FROM  (SELECT id FROM domains_all WHERE (age(current_date,offline_check_date) > interval '$days_ago days') {$this->skip_tlds_sql} LIMIT $limit FOR UPDATE) sub
			WHERE  domains_all.id = sub.id
		RETURNING  concat('http://',domains_all.domain) as url,domains_all.domain as domain, domains_all.id as domain_id, domains_all.tld as tld, 0 as try_num,  domains_all.moz_da as moz_da" );

		return $q_domains->fetchAll( PDO::FETCH_NAMED );
	}

	protected function getProxy() {
		$this->query_select_random_proxy->execute();
		$r			 = $this->query_select_random_proxy->fetch( PDO::FETCH_NAMED );
		$address	 = Helper::getValue( $r, 'address' );
		$username	 = Helper::getValue( $r, 'username' );
		$password	 = Helper::getValue( $r, 'password' );

		$curl_proxy_opt = array(
			CURLOPT_PROXY => $address,
		);
		if ( $username and $password ) {
			$curl_proxy_opt[ CURLOPT_HTTPAUTH ]		 = CURLAUTH_BASIC;
			$curl_proxy_opt[ CURLOPT_PROXYUSERPWD ]	 = "$username:$password";
		}

		return $curl_proxy_opt;
	}

	protected function getRandomUseragent() {
		$useragents = Helper::getOption( 'useragents' );
		if ( $useragents ) {
			$c = count( $useragents );
			if ( $c ) {
				$n = mt_rand( 0, $c - 1 );
				return $useragents[ $n ];
			}
		}
		return '';
	}

	function ping80($host) {
		$ip = $this->get_ipaddr_by_hostnamel($host);
		if(!$ip ) {
			echo "no IP for $host ";
			return false;
		}	
			
		if($fp = @fsockopen($ip ,$port=80,$errCode,$errStr,$waitTimeoutInSeconds=1)){   
			$r =true;
			fclose($fp);
		} else {
			$r = false;
		} 
		return $r;
	}

	function get_ipaddr_by_hostnamel($domain, $timeout = 4) {
		$query = `nslookup -timeout=$timeout -retry=1 $domain`;
		if(preg_match('/\nAddress: (.*)\n/', $query, $matches))
			return trim($matches[1]);                                                 // return successful IP
		return false;                                                                      // else return false
	}
}
