<?php

class DomainsTable extends Table {

	public function getSortableColumns() {
		return array( 'domain', 'tld', 'whois_available', 'moz_da', 'date_found' );
	}

	public function getFields() {
		return array( 'domain' => 'Domain', 'tld' => 'TLD', 'date_found' => 'Date Found', 'whois_expire_date' => 'Expiry Date', 'moz_da' => 'Moz DA' );
	}

	public function display() {
		$this->max_visible_pages = 10;
		$this->createTable();
	}

	public function processingField( $data_row, $field ) {
		switch ( $field ) {
			case 'domain':
				return '<a href="http://' . $data_row[ $field ] . '" target=_blank>' . $data_row[ $field ] . '</a>';
				break;
			default:
				return $data_row[ $field ];
		}
	}

	public function getData() {
		$db = DB::getInstance();

		$current_page = $this->getCurrentPage();

		$where			 = '';
		$params			 = Helper::getRequest( 'params' );
		$search			 = Helper::getValue( $params, 'search' );
		$date_from		 = Helper::getValue( $params, 'date_from' );
		$date_to		 = Helper::getValue( $params, 'date_to' );
		$max_row		 = Helper::getValue( $params, 'max_row' );
		$prepare_params	 = array();
		if ( $search ) {
			$where						 = "WHERE domain LIKE :search";
			$prepare_params[ ':search' ] = "%$search%";
		}
		if ( $date_from ) {
			if ( $where ) {
				$where .= " AND date_found>=:date_from";
			} else {
				$where = "WHERE date_found>=:date_from";
			}
			$prepare_params[ ':date_from' ] = $date_from;
		}
		if ( $date_to ) {
			if ( $where ) {
				$where .= " AND date_found<=:date_to";
			} else {
				$where = "WHERE date_found<=:date_to";
			}
			$prepare_params[ ':date_to' ] = $date_to;
		}
		
		$order_by		 = $this->order_by;
		$order_by_sql	 = '';
		$direction		 = $this->direction;
//		var_dump( $order_by, $direction );
//		if ( $direction == 'DESC' ) {
//			$direction = 'ASC';
//		} else {
//			$direction = 'DESC';
//		}
		$sc				 = $this->getSortableColumns();
		if ( $order_by AND in_array( $order_by, $sc ) ) {
			if ( $direction == 'DESC' ) {
				$nulls = 'NULLS LAST';
			} else {
				$nulls = 'NULLS FIRST';
			}
			$domain			 = $order_by == 'domain' ? '' : ',domain';
			$order_by_sql	 = "ORDER BY $order_by $direction $nulls $domain ";
		}

		$limit		 = $this->limit;
		$limit_sql	 = '';

		if ( $max_row ) {
			$limit_sql					 = "LIMIT :limit";
			$prepare_params[ ':limit' ]	 = $max_row;
		} elseif ( $limit ) {
			$limit_sql					 = "LIMIT :limit OFFSET :offset";
			$prepare_params[ ':limit' ]	 = $limit;
			$prepare_params[ ':offset' ] = ($current_page - 1) * $limit;
		}
		$sql	 = "SELECT * FROM domains_all $where $order_by_sql $limit_sql";
		$jobs	 = $db->prepare( $sql );
		$r		 = $jobs->execute( $prepare_params );
//		var_dump( $_REQUEST,$order_by,$sql, $jobs, $prepare_params, $jobs->errorInfo() );
//		die();
		$jobs	 = $jobs->fetchAll( PDO::FETCH_NAMED );

		unset( $prepare_params[ ':limit' ] );
		unset( $prepare_params[ ':offset' ] );
		$amount			 = $db->prepare( 'SELECT COUNT(*) as amount FROM domains_all ' . $where );
		$r				 = $amount->execute( $prepare_params );
		$amount			 = $amount->fetch( PDO::FETCH_COLUMN );
		//-=-=-=-=-=-=-=-=-
		$this->amount	 = $amount;
		//-=-=-=-=-=-=-=-=-
		return $jobs;
	}

}
