<?php

class ProxyCheck extends MultiCurl {

	public function __construct( $curl_options = array() ) {
		$settings			 = Helper::getOption( 'settings' );
		$connection_timeout	 = Helper::getValue( $settings, 'connection_timeout' );
		$max_page_size		 = Helper::getValue( $settings, 'max_page_size' );
		parent::__construct( $curl_options, $connection_timeout, $max_page_size );
	}

	public function processNextProxies( $test_url, $number_threads = 5 ) {
		$max_threads = Helper::getSetting( 'offlinecheck_threads' );
		$days_ago	 = Helper::getSetting( 'offlinecheck_days_ago' );

		$t_urls = array();
		for ( $i = 0; $i < $number_threads; $i++ ) {
			$curl_opt_proxy = $this->getCurlOptProxy();
			if ( $curl_opt_proxy ) {
				$t_urls[]	 = $curl_opt_proxy;
				$table_row	 = $curl_opt_proxy[ 't' ];
				unset( $curl_opt_proxy[ 't' ] );
				$this->addUrlToQueue( $test_url, $table_row, false, $curl_opt_proxy );
			} else {
				break;
			}
		}
		$this->start( array(), array( $this, 'handler' ) );
		return count( $t_urls ) > 0;
	}

	function handler( $content, $curl_info, $curl_multi_info, $table_row ) {
		echo '------------';
		var_dump( $table_row );
		$db			 = DB::getInstance();
		$http_code	 = $curl_info[ 'http_code' ];
		if ( $http_code == 0 ) {
			$live = 0;
		} else {
			$live = 1;
		}
		$proxy_id	 = $table_row[ 'id' ];
		$pr			 = $db->prepare( 'UPDATE proxies SET live=:live WHERE id=:proxy_id' );
		$pr->execute( array( ':live' => $live, ':proxy_id' => $proxy_id ) );
		echo '============';
	}

	protected function getCurlOptProxy() {

		$db			 = DB::getInstance();
		$q_domains	 = $db->query( "UPDATE proxies SET check_time = current_timestamp
			FROM  (SELECT * from proxies WHERE check_time is null or age(current_timestamp,check_time)>interval '1 hour' LIMIT 1 FOR UPDATE) sub
			WHERE  proxies.id = sub.id
		RETURNING  proxies.*" );

		$r = $q_domains->fetch( PDO::FETCH_NAMED );
		if ( $r ) {
			$address	 = Helper::getValue( $r, 'address' );
			$username	 = Helper::getValue( $r, 'username' );
			$password	 = Helper::getValue( $r, 'password' );

			$curl_proxy_opt = array(
				CURLOPT_PROXY => $address,
			);
			if ( $username and $password ) {
				$curl_proxy_opt[ CURLOPT_HTTPAUTH ]		 = CURLAUTH_BASIC;
				$curl_proxy_opt[ CURLOPT_PROXYUSERPWD ]	 = "$username:$password";
			}
			$curl_proxy_opt[ 't' ] = $r;
			return $curl_proxy_opt;
		} else {
			return '';
		}
	}

	protected function getRandomUseragent() {
		$useragents = Helper::getOption( 'useragents' );
		if ( $useragents ) {
			$c = count( $useragents );
			if ( $c ) {
				$n = mt_rand( 0, $c - 1 );
				return $useragents[ $n ];
			}
		}
		return '';
	}

}
