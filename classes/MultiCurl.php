<?php

class MultiCurl {

	protected $curl_multi_handler	 = false;
	protected $arr_curl_handlers	 = array();
	protected $curl_options;
	protected $urls_params;
	protected $count_urls			 = 0;

	const BUFFERSIZE = 1024;

	public function __construct( $curl_options = array(), $connection_timeout = 30, $max_page_size = 1024100 ) {
		$header = array(
			"Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5",
			"Accept-Language: en-us",
			"Accept-Charset: SO-8859-1,utf-8;q=0.7,*;q=0.7",
			"Keep-Alive: 300",
			"Connection: keep-alive",
			"Pragma: no-cache",
			"Cache-Control: no-cache",
			"Expect:",
		);

		$default_curl_options = array(
			CURLOPT_RETURNTRANSFER	 => 1,
			CURLOPT_BINARYTRANSFER	 => 1,
			CURLOPT_CONNECTTIMEOUT	 => $connection_timeout,
			CURLOPT_TIMEOUT			 => $connection_timeout,
			CURLOPT_USERAGENT		 => 'Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.1.1) Gecko/20090716 Ubuntu/9.04 (jaunty) Shiretoko/3.5.1',
			CURLOPT_VERBOSE			 => 0, //was 2 
			CURLOPT_HEADER			 => 0,
			CURLOPT_FOLLOWLOCATION	 => 1,
			CURLOPT_MAXREDIRS		 => 5,
			CURLOPT_AUTOREFERER		 => 1,
			CURLOPT_FRESH_CONNECT	 => 1,
			CURLOPT_HTTPHEADER		 => $header,
			CURLOPT_SSL_VERIFYPEER	 => false,
			CURLOPT_SSL_VERIFYHOST	 => false,
			CURLOPT_BUFFERSIZE		 => self::BUFFERSIZE,
			CURLOPT_NOPROGRESS		 => false,
			CURLOPT_PROGRESSFUNCTION => function($DownloadSize, $Downloaded, $UploadSize, $Uploaded) use ($max_page_size) {
				return ($Downloaded > ($max_page_size * MultiCurl::BUFFERSIZE)) ? 1 : 0;
			},
		);

		foreach ( $curl_options as $option => $value ) {
			$default_curl_options[ $option ] = $value;
		}
		$this->curl_options = $default_curl_options;

		$this->curl_multi_handler = curl_multi_init();
	}

	public function __destruct() {
		$this->closeAllHandlers();
		$this->curl_multi_handler = false;
	}

	public function addUrlToQueue( $url, $params = array(), $use_proxy = false, $curl_opt = array() ) {
		if ( $this->curl_multi_handler ) {

			$curl_handler				 = curl_init();
			$this->arr_curl_handlers[]	 = $curl_handler;
			$useragent					 = $this->getRandomUseragent();
			if ( $useragent ) {
				curl_setopt( $curl_handler, CURLOPT_USERAGENT, $useragent );
			}

			$url = Helper::convertToASCII( $url );

			curl_setopt( $curl_handler, CURLOPT_URL, $url );
			curl_setopt_array( $curl_handler, $this->curl_options );
			if ( !empty( $curl_opt ) ) {
				curl_setopt_array( $curl_handler, $curl_opt );
			}
			if ( $use_proxy ) {
				$curl_proxy_opt = $this->getProxy();
				if ( empty( $curl_proxy_opt ) ) {
					curl_close( $curl_handler );
					return false;
				}
				if ( is_array( $params ) ) {
					$params[ 'curl_proxy_opt' ] = $curl_proxy_opt;
				} else {
					$params = array( 'curl_proxy_opt' => $curl_proxy_opt );
				}

				if ( count( $curl_handler ) ) {
					curl_setopt_array( $curl_handler, $curl_proxy_opt );
					//var_dump( $curl_proxy_opt );
				}
			}
			$n_handler						 = intval( $curl_handler );
			$this->urls_params[ $n_handler ] = $params;
			curl_multi_add_handle( $this->curl_multi_handler, $curl_handler );

			$this->count_urls++;
			return true;
		}
		return false;
	}

	protected function getProxy() {
		return '';
	}

	protected function getRandomUseragent() {
		return '';
	}

	/**
	 * 
	 * @param array $urls
	 * @param function $handler function parameters: $content, $curl_info, $curl_multi_info, $urls_params 
	 */
	public function start( $t_urls = array(), $handler, $use_proxy = false, $curl_opt = array() ) {
		foreach ( $t_urls as $t_url ) {
			$this->addUrlToQueue( $t_url[ 'url' ], $t_url, $use_proxy, $curl_opt );
		}
		$this->curlMultiExec( $handler );
	}

	private function curlMultiExec2( $handler ) {
		$running = null;

		do {
			//was curl_multi_exec( $this->curl_multi_handler, $running );
			while ( ($execrun = curl_multi_exec( $this->curl_multi_handler, $running )) == CURLM_CALL_MULTI_PERFORM ) {
				sleep( 1 );
			}
			if ( $execrun != CURLM_OK )
				break;

			while ( $curl_multi_info = curl_multi_info_read( $this->curl_multi_handler ) ) {
				$ch = is_array( $curl_multi_info ) ? $curl_multi_info[ 'handle' ] : null;
				if ( $ch ) {
					$content	 = curl_multi_getcontent( $ch );
					$curl_info	 = curl_getinfo( $ch );
					$n_handler	 = intval( $ch );
					$handler( $content, $curl_info, $curl_multi_info, $this->urls_params[ $n_handler ] );
					unset( $this->urls_params[ $n_handler ] );

					curl_multi_remove_handle( $this->curl_multi_handler, $ch );
					curl_close( $ch );
				}
			}

			if ( $running )
				usleep( 100 );
		} while ( $running > 0 );
	}

	public function curlMultiExec( $handler ) {
		$mh				 = $this->curl_multi_handler;
		$still_running	 = null;
		$this->full_curl_multi_exec( $mh, $still_running ); // start requests 
		do { // "wait for completion"-loop 
			echo "-";
			curl_multi_select( $mh ); // non-busy (!) wait for state change 
			$this->full_curl_multi_exec( $mh, $still_running ); // get new state 
			while ( $curl_multi_info = curl_multi_info_read( $mh ) ) {
				echo "+";
				$ch = is_array( $curl_multi_info ) ? $curl_multi_info[ 'handle' ] : null;
				if ( $ch ) {
					$content	 = curl_multi_getcontent( $ch );
					$curl_info	 = curl_getinfo( $ch );
					$n_handler	 = intval( $ch );
					$handler( $content, $curl_info, $curl_multi_info, $this->urls_params[ $n_handler ] );
					$this->after_call_handler();
					unset( $this->urls_params[ $n_handler ] );
					curl_multi_remove_handle( $this->curl_multi_handler, $ch );
					curl_close( $ch );
					$this->full_curl_multi_exec( $mh, $still_running ); // get new state 
				}
			}
		} while ( $still_running );
	}

	public function after_call_handler() {
		$this->count_urls--;
	}

	public function full_curl_multi_exec( $mh, &$still_running ) {
		do {
			echo ".";
			$rv = curl_multi_exec( $mh, $still_running );
		} while ( $rv == CURLM_CALL_MULTI_PERFORM );
		return $rv;
	}

	public function closeAllHandlers() {
// 		foreach ( $this->arr_curl_handlers as $ch ) {
// 			curl_multi_remove_handle( $this->curl_multi_handler, $ch );
// 			curl_close( $ch );
// 		}
		curl_multi_close( $this->curl_multi_handler );
	}

}
