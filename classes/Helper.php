<?php

define( 'HTTP_URL_REPLACE', 1 ); // Replace every part of the first URL when there's one of the second URL
define( 'HTTP_URL_JOIN_PATH', 2 );   // Join relative paths
define( 'HTTP_URL_JOIN_QUERY', 4 );   // Join query strings
define( 'HTTP_URL_STRIP_USER', 8 );   // Strip any user authentication information
define( 'HTTP_URL_STRIP_PASS', 16 );   // Strip any password authentication information
define( 'HTTP_URL_STRIP_AUTH', 32 );   // Strip any authentication information
define( 'HTTP_URL_STRIP_PORT', 64 );   // Strip explicit port numbers
define( 'HTTP_URL_STRIP_PATH', 128 );   // Strip complete path
define( 'HTTP_URL_STRIP_QUERY', 256 );  // Strip query string
define( 'HTTP_URL_STRIP_FRAGMENT', 512 );  // Strip any fragments (#identifier)
define( 'HTTP_URL_STRIP_ALL', 1024 );   // Strip anything but scheme and host

class Helper {

	static $query_get_option = false;

	public static function getOption( $option ) {

		if ( !self::$query_get_option ) {
			$db	 = DB::getInstance();
			$sql = 'SELECT value FROM options WHERE option=:option';
			self::$query_get_option = $db->prepare( $sql );
		}
		$r	 = self::$query_get_option->execute( array( ':option' => $option ) );
		$v	 = self::$query_get_option->fetch( PDO::FETCH_COLUMN );
		$v	 = unserialize( $v );
		return $v;
	}

	public static function getSetting( $name ) {
		$settings = self::getOption( 'settings' );
		return $settings[ $name ];
	}

	public static function updateOption( $option, $value ) {

		$value = serialize( $value );

		$db	 = DB::getInstance();
		$op	 = self::getOption( $option );
		if ( $op ) {
			$sql = 'UPDATE options SET value=:value WHERE option=:option';
			$up	 = $db->prepare( $sql );
			$r	 = $up->execute( array( ':option' => $option, ':value' => $value ) );
			return $r;
		} else {
			$sql = 'INSERT INTO options (option,value) VALUES (:option,:value)';
			$up	 = $db->prepare( $sql );
			$r	 = $up->execute( array( ':option' => $option, ':value' => $value ) );
			return $r;
		}
	}

	public static function getRequest( $name_param ) {
		return isset( $_REQUEST[ $name_param ] ) ? $_REQUEST[ $name_param ] : '';
	}

	public static function getValue( $arr, $name_param, $default_value = '' ) {
		return isset( $arr[ $name_param ] ) ? $arr[ $name_param ] : $default_value;
	}

	public static function renderPartial( $view, $param = array() ) {
		if ( !is_null( $param ) ) {
			extract( $param );
		}

		include APP_PATH . "/views/$view.php";
	}

	public static function get_current_job() {
		$db	 = DB::getInstance();
		$pr	 = $db->prepare( "SELECT * FROM jobs WHERE status='Processing' LIMIT 1" );
		$r	 = $pr->execute( array() );
		$row = $pr->fetch( PDO::FETCH_NAMED );
		return $row;
	}

	public static function finish_job( $db ) {
		// add new domains from job
		$db->exec( 'INSERT  INTO domains_all(domain,tld,date_found)  SELECT domain,tld,date_found FROM domains_job' );

		$r= $db->query( "SELECT SUM(passed_urls) FROM domains_job" );
		$urls = (int) $r->fetchColumn();
		
		$r= $db->query( "SELECT COUNT(*) FROM domains_job" );
		$domains = (int) $r->fetchColumn();
		
		$db->exec( "UPDATE jobs SET status='Finished',links_found=$urls,domains_found=$domains WHERE status='Processing'" );
		
		$db->exec( 'TRUNCATE TABLE domains_job' );
	}

	public static function start_new_job( $db ) {
		// get first new job
		$pr	 = $db->prepare( "SELECT * FROM jobs WHERE status='New' ORDER BY id LIMIT 1" );
		$r	 = $pr->execute( array() );
		$job = $pr->fetch( PDO::FETCH_NAMED );
		if ( !$job )
			return false;

		// must remove ?	
		$db->exec( 'TRUNCATE TABLE domains_job' );

		//add urls 
		$pr_insert	 = $db->prepare( 'INSERT INTO domains_job(domain,tld,date_found,start_url,depth,parsed,passed_urls) VALUES(:domain,:tld,:date_found,:start_url,0,0,0)' );
		$pr	 = $db->prepare( 'SELECT DISTINCT url FROM jobs_start_urls WHERE job_id=:job_id' );
		
		$r	 = $pr->execute( array( ':job_id' => $job[ 'id' ] ) );
		$MC = new Crawler($db);
		while( $url  = $pr->fetch( PDO::FETCH_NAMED ) ) {
				$domain = parse_url($url['url'],PHP_URL_HOST);
				if(!$domain)
					$domain = $url['url'];
				$domain = str_replace("www.","",$domain);
				$tld = $MC->getTLD($domain);
				$pr_insert->execute( array( ':domain' => $domain, ':tld' => $tld, ':date_found' => date( 'Y-m-d H:i:s' ), ':start_url' => $url['url'] ) );
		}

		//mark job as processing
		$pr	 = $db->prepare( "UPDATE jobs SET status='Processing' WHERE id=:job_id" );
		$r	 = $pr->execute( array( ':job_id' => $job[ 'id' ] ) );

		return $job;
	}

	public static function system_down_check() {
		$status = self::getOption( "crawler_status" );
		if ( $status != 'Start' )
			die( "can't start.  crawler_status = $status" );
		
		$db	 = DB::getInstance();
		// stop run too many clients
		$pr	 = $db->prepare( "SHOW max_connections" );
		$r	 = $pr->execute( array() );
		$row = $pr->fetch( PDO::FETCH_NAMED );
		$allowed_connections = $row['max_connections'] -5;
		
		$pr	 = $db->prepare( "SELECT count(*) AS connections FROM pg_stat_activity" );
		$r	 = $pr->execute( array() );
		$row = $pr->fetch( PDO::FETCH_NAMED );
		
		if($row['connections'] >= $allowed_connections)
			die( "can't start.  already run $allowed_connections connections" );
	}

	public static function convertToASCII( $domain ) {
		$parts = parse_url( $domain );
		if ( !isset( $parts[ 'host' ] ) )
			return $domain; // missing http? makes parse_url fails
		// convert if domain name is non_ascii
		if ( mb_detect_encoding( $parts[ 'host' ] ) != 'ASCII' ) {
			$idna_convert	 = new idna_convert();
			$parts[ 'host' ] = $idna_convert->encode( $parts[ 'host' ] );
			return self::http_build_url( '', $parts );
		}
		return $domain;
	}

	public static function http_build_url( $url, $parts = array(), $flags = HTTP_URL_REPLACE, &$new_url = false ) {
		$keys = array( 'user', 'pass', 'port', 'path', 'query', 'fragment' );

		// HTTP_URL_STRIP_ALL becomes all the HTTP_URL_STRIP_Xs
		if ( $flags & HTTP_URL_STRIP_ALL ) {
			$flags |= HTTP_URL_STRIP_USER;
			$flags |= HTTP_URL_STRIP_PASS;
			$flags |= HTTP_URL_STRIP_PORT;
			$flags |= HTTP_URL_STRIP_PATH;
			$flags |= HTTP_URL_STRIP_QUERY;
			$flags |= HTTP_URL_STRIP_FRAGMENT;
		}
		// HTTP_URL_STRIP_AUTH becomes HTTP_URL_STRIP_USER and HTTP_URL_STRIP_PASS
		else if ( $flags & HTTP_URL_STRIP_AUTH ) {
			$flags |= HTTP_URL_STRIP_USER;
			$flags |= HTTP_URL_STRIP_PASS;
		}

		// Parse the original URL
		$parse_url = parse_url( $url );

		// Scheme and Host are always replaced
		if ( isset( $parts[ 'scheme' ] ) )
			$parse_url[ 'scheme' ]	 = $parts[ 'scheme' ];
		if ( isset( $parts[ 'host' ] ) )
			$parse_url[ 'host' ]	 = $parts[ 'host' ];

		// (If applicable) Replace the original URL with it's new parts
		if ( $flags & HTTP_URL_REPLACE ) {
			foreach ( $keys as $key ) {
				if ( isset( $parts[ $key ] ) )
					$parse_url[ $key ] = $parts[ $key ];
			}
		}
		else {
			// Join the original URL path with the new path
			if ( isset( $parts[ 'path' ] ) && ($flags & HTTP_URL_JOIN_PATH) ) {
				if ( isset( $parse_url[ 'path' ] ) )
					$parse_url[ 'path' ] = rtrim( str_replace( basename( $parse_url[ 'path' ] ), '', $parse_url[ 'path' ] ), '/' ) . '/' . ltrim( $parts[ 'path' ], '/' );
				else
					$parse_url[ 'path' ] = $parts[ 'path' ];
			}

			// Join the original query string with the new query string
			if ( isset( $parts[ 'query' ] ) && ($flags & HTTP_URL_JOIN_QUERY) ) {
				if ( isset( $parse_url[ 'query' ] ) )
					$parse_url[ 'query' ] .= '&' . $parts[ 'query' ];
				else
					$parse_url[ 'query' ] = $parts[ 'query' ];
			}
		}

		// Strips all the applicable sections of the URL
		// Note: Scheme and Host are never stripped
		foreach ( $keys as $key ) {
			if ( $flags & (int) constant( 'HTTP_URL_STRIP_' . strtoupper( $key ) ) )
				unset( $parse_url[ $key ] );
		}


		$new_url = $parse_url;

		return
		((isset( $parse_url[ 'scheme' ] )) ? $parse_url[ 'scheme' ] . '://' : '')
		. ((isset( $parse_url[ 'user' ] )) ? $parse_url[ 'user' ] . ((isset( $parse_url[ 'pass' ] )) ? ':' . $parse_url[ 'pass' ] : '') . '@' : '')
		. ((isset( $parse_url[ 'host' ] )) ? $parse_url[ 'host' ] : '')
		. ((isset( $parse_url[ 'port' ] )) ? ':' . $parse_url[ 'port' ] : '')
		. ((isset( $parse_url[ 'path' ] )) ? $parse_url[ 'path' ] : '')
		. ((isset( $parse_url[ 'query' ] )) ? '?' . $parse_url[ 'query' ] : '')
		. ((isset( $parse_url[ 'fragment' ] )) ? '#' . $parse_url[ 'fragment' ] : '')
		;
	}

}
