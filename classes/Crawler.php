<?php
class Crawler{
	/**
	 *
	 * @var type Crawler
	 */
	public $urls;
	public $urls_counter;
	public $urls_added_at_level;
	public $urls_crawlered_counter;
	public $active_curls;
	public $run_root_domain;
	public $heartbeat_timeout = 10; // in sec
	const BUFFERSIZE = 1024;

	public function __construct( $curl_options = array() ) {
		
		// for CUrl
		$settings			 = Helper::getOption( 'settings' );
		$connection_timeout	 = Helper::getValue( $settings, 'connection_timeout' );
		$max_page_size		 = Helper::getValue( $settings, 'max_page_size' );
		$header = array(
			"Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5",
			"Accept-Language: en-us",
			"Accept-Charset: SO-8859-1,utf-8;q=0.7,*;q=0.7",
			"Keep-Alive: 300",
			"Connection: keep-alive",
			"Pragma: no-cache",
			"Cache-Control: no-cache",
			"Expect:",
		);		
		$curl_options = array(
			CURLOPT_RETURNTRANSFER	 => 1,
			CURLOPT_BINARYTRANSFER	 => 1,
			CURLOPT_CONNECTTIMEOUT	 => $connection_timeout,
			CURLOPT_TIMEOUT			 => $connection_timeout,
			CURLOPT_USERAGENT		 => 'Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.1.1) Gecko/20090716 Ubuntu/9.04 (jaunty) Shiretoko/3.5.1',
			CURLOPT_VERBOSE			 => 0, //was 2 
			CURLOPT_HEADER			 => 0,
			CURLOPT_FOLLOWLOCATION	 => 1,
			CURLOPT_MAXREDIRS		 => 5,
			CURLOPT_AUTOREFERER		 => 1,
			CURLOPT_FRESH_CONNECT	 => 1,
			CURLOPT_HTTPHEADER		 => $header,
			CURLOPT_SSL_VERIFYPEER	 => false,
			CURLOPT_SSL_VERIFYHOST	 => false,
			CURLOPT_BUFFERSIZE		 => self::BUFFERSIZE,
			CURLOPT_NOPROGRESS		 => false,
			CURLOPT_PROGRESSFUNCTION => function($DownloadSize, $Downloaded, $UploadSize, $Uploaded) use ($max_page_size) {
				return ($Downloaded > ($max_page_size * MultiCurl::BUFFERSIZE)) ? 1 : 0;
			},
		);
		$this->PCurl = new ParallelCurl(10,$curl_options);

		//db queries 
		$db								 = Db::getInstance();
		//add external domain 
		$this->query_insert_job_domain	 = $db->prepare( 'INSERT INTO domains_job (domain,tld,date_found,depth,parsed,passed_urls) VALUES (:domain,:tld,:date_found,:depth,0,0)' );
		
		// for heartbeat, show live state 
		$this->heartbeat_time = 0;
		$this->query_update_process	 	 = $db->prepare( 'UPDATE processes SET domain=:domain, passed_urls=:passed_urls, urls=:urls  WHERE pid=:pid' );
		$this->query_get_job = $db->prepare( "SELECT * FROM jobs WHERE id=:id" );
		$this->query_update_job_domain	 = $db->prepare( 'UPDATE domains_job SET passed_urls=:passed_urls,live_at_time=extract(epoch from now())  WHERE domain=:domain' ); // live!
		
		//finish 
		$this->query_finish_job_domain	 = $db->prepare( 'UPDATE domains_job SET passed_urls=:passed_urls,parsed=2  WHERE domain=:domain' ); // 2 - finished!
		
		$this->initTLDs();
		$this->excludeDomains = Helper::getOption( 'exclude_domains' );
		$this->useragents = Helper::getOption( 'useragents' );
		
		// get proxies
		$this->query_select_proxies = $db->prepare( 'SELECT * FROM proxies WHERE live=1' );
		$this->query_select_proxies->execute();
		$this->proxies = $this->query_select_proxies->fetchAll(PDO::FETCH_NAMED);

		//filter URLs
		$this->sess_masks = array(
			'#ASPSESSIONID[a-zA-Z]{8}=[a-zA-Z]{24}#is',
			'#jsessionid=[0-9a-fA-F]{32}(\&|\?|\.)(.*)#is',
			'#PHPSESSID=[0-9a-zA-Z]{32}#is',
			'#sid=[0-9a-zA-Z]{32}#is',
			'#(;?\w+SESSIONID=.*)#is',
			'@\#.*$@is', // kill #
			'@;.*$@is', // kill ; 
		);
		$this->skip_ext = explode( " ", "pdf doc xls ppt pps bin exe rss xml docx pptx xlsx flv swf png jpg gif asx zip rar tar 7z gz jar js css dtd xsd ico raw mp3 mp4 wav wmv ape aac ac3 wma aiff mpg mpeg avi mov ogg mkv mka asx asf mp2 m1v m3u f4v svg" );
		
	}
	
	public function processJobDomains( $job , $process, $pid) {
		$db = DB::getInstance();
		
		// for stop/live
		$this->process = $process;
		$this->pid = $pid;
		
		$this->job_id = $job[ 'id' ];
		$this->job_only_domain = $job[ 'only_domain' ];
		$this->job_max_depth = $job[ 'depth' ];
		$this->max_threads = $job[ 'threads_per_crawler' ];
		$this->max_urls_per_domain = $job[ 'max_urls_per_domain' ];
		$this->max_time_per_domain = $job[ 'max_time_in_mins' ] * 60;
		
		//debug REMOVE
		//$this->query_select_domain_1 = $db->prepare( "UPDATE domains_job  SET    parsed = 0 WHERE depth=1" );
		//$this->query_select_domain_1 ->execute();
		
		
		// take only zero level, initial domains
		if($this->job_only_domain) {
			$this->run_root_domain = true;
			$this->max_domains = 1;
			$this->max_threads_per_domain = $this->max_threads;
			$this->query_select_domain_1 = $db->prepare( "UPDATE domains_job  SET    parsed = 1, live_at_time=extract(epoch from now())
				FROM  (    SELECT domain   FROM   domains_job WHERE  parsed=0 AND depth=0  LIMIT  1 FOR   UPDATE   ) sub
				WHERE  domains_job.domain = sub.domain
			RETURNING domains_job.*" );
		} else {
			$this->run_root_domain = false;
			$this->max_domains = $job[ 'max_run_domains' ];
			$this->max_threads_per_domain = floor($this->max_threads/$this->max_domains);
			$this->max_threads = $this->max_threads_per_domain * $this->max_domains; //correct total threads
			$this->query_select_domain_1	 = $db->prepare( "UPDATE domains_job  SET    parsed = 1,live_at_time=extract(epoch from now())
				FROM  (    SELECT domain   FROM   domains_job WHERE  parsed=0 AND depth<{$this->job_max_depth}  ORDER BY depth LIMIT  1  FOR   UPDATE   ) sub
				WHERE  domains_job.domain = sub.domain
			RETURNING domains_job.*" );
		}	
		
		//reset arrays 
		$this->urls = $this->urls_added_at_level = $this->urls_crawlered_counter= $this->urls_counter = $this->active_curls = array();
		
		//adds domains
		$this->process_domains = array();
		for($i=0;$i<$this->max_domains;$i++) {
			$this->addNextDomainToRun();
			// job_only_domain=false, but we get ROOT domain, run it alone
			if($this->run_root_domain) {
				$this->max_domains = 1;
				$this->max_threads_per_domain = $this->max_threads;
				$this->max_threads = $job[ 'threads_per_crawler' ];
				$this->query_select_domain_1  = $db->prepare( "UPDATE domains_job  SET    parsed = 1,live_at_time=extract(epoch from now())
				FROM  (    SELECT domain   FROM   domains_job WHERE  parsed=0 AND depth=0  LIMIT  1  FOR   UPDATE   ) sub
				WHERE  domains_job.domain = sub.domain
			RETURNING domains_job.*" );
				break;
			}	
		}	
		if(empty($this->process_domains))
			return false;
		
		// we must skip root domains ( if root domain stopped and unlocked  by cron/jobs.php)
		if(!$this->run_root_domain) {
			$this->query_select_domain_1	 = $db->prepare( "UPDATE domains_job  SET    parsed = 1,live_at_time=extract(epoch from now())
				FROM  (    SELECT domain   FROM   domains_job WHERE  parsed=0 AND depth>0 AND depth<{$this->job_max_depth}  ORDER BY depth LIMIT  1  FOR   UPDATE   ) sub
				WHERE  domains_job.domain = sub.domain
			RETURNING domains_job.*" );
		}
		
		//start 	
		$this->heartbeat();
		$this->PCurl->setMaxRequests($this->max_threads);
		
		//populate inital requests
		foreach($this->process_domains as $domainname=>$domain) 
			$this->startNewUrl($domainname);
		
		//MAIN loop 
		while($this->process_domains) {
			//wait till get free
      
      $timestamp1 = microtime();
      
			$this->PCurl->waitForOutstandingRequestsToDropBelow($this->max_threads);
			//var_dump($this->active_curls);
      
      $timestamp2 = microtime();
		
			foreach($this->process_domains as $domainname=>$domain) {
			
				// try setup MAX curls for domain
				while($this->active_curls[$domainname] < $this->max_threads_per_domain) {
					//	extra checks for not ROOT domain
					//debug if(true )	 {
					if($domain['depth'] != 0 )	 {
						// 	Don't add new urls if we reached MAX time
						if(time() - $domain['started_at'] >= $this->max_time_per_domain)
							break;
						// 	Don't add new urls if we reached MAX urls
						if( $this->urls_crawlered_counter[$domainname] >= $this->max_urls_per_domain)
							break;
					}
					//Yes, we can!
					if(!$this->startNewUrl($domainname))
						break;
					//var_dump($this->active_curls[$domainname]);
				}	
				
				echo  $domainname."| curls=".$this->active_curls[$domainname]."| urls=".$this->urls_counter[$domainname]."| crawled=".$this->urls_crawlered_counter[$domainname]."\n";
				//file_put_contents(__DIR__."/../_logs/crawler.".$this->pid, $domainname."| curls=".$this->active_curls[$domainname]."| urls=".$this->urls_counter[$domainname]."| crawled=".$this->urls_crawlered_counter[$domainname]."\n",FILE_APPEND); 
				//die($domainname);
				
				//Done ?
				if($this->active_curls[$domainname] == 0 ) {
					$this->finishDomain($domainname);
				}
			}
      
      $timestamp3 = microtime();
			
			//have to start  more domains
			while( count($this->process_domains) < $this->max_domains) {
				if( !$this->addNextDomainToRun() )
					break;
			}
      
      $timestamp4 = microtime();
      echo file_put_contents( '/var/www/crawler/timelog_3.txt', $timestamp1." - ".$timestamp2." - ".$timestamp3." - ".$timestamp4."\n", FILE_APPEND );
			//var_dump($this->urls_counter,$this->active_curls,$this->urls_counter,$this->urls_crawlered_counter);	
			//die("make loop here");
			//echo "-----------------------------------------";
		}
		return true;
	}
	
	private function startNewUrl($domainname) {
			$user_data = $this->getNextUrlData($domainname);	
			if(!$user_data )
				return false;
			$this->urls_crawlered_counter[$domainname]++;
			$this->active_curls[$domainname]++;
			
			$user_data['proxy'] = $this->getProxy();
			$user_data['user_agent'] = $this->getRandomUseragent();
			
			echo "START:  $user_data[proxy] $user_data[url]\n"; //..
			$this->PCurl->startRequest($user_data['url'], array($this,'handler'), $user_data); 
			return true;
	}		


	private function addNextDomainToRun() {
		$this->query_select_domain_1->execute();
		$domain = $this->query_select_domain_1->fetch( PDO::FETCH_NAMED );
		if(!$domain)
			return false;
		
		if($domain['depth'] == 0)
			$this->run_root_domain = true;
			
		$domainname = $domain['domain'];
		if($domain['start_url'])
				$url = $domain['start_url'];
			else	
				$url = 'http://'.$domain['domain'];
		//add and mark as added already 		
		$this->urls[$domainname] = array($url); 		
		$this->urls_added_at_level[$domainname] = array($url=>1); //assoc!
		$this->urls_counter[$domainname]= 1; 		
		$this->urls_crawlered_counter[$domainname]= 0; 		// nothing! crawlered yet
		$this->active_curls[$domainname]= 0;
		
		//record time start
		$domain['started_at'] = time();
		$this->process_domains[$domainname] = $domain;
	}
	
	private function finishDomain($domainname) {
		//record state 
		$this->query_finish_job_domain->execute( array(":passed_urls"=>$this->urls_crawlered_counter[$domainname],':domain'=>$domainname) );
		
		//remove all arrays
		unset($this->urls[$domainname]);
		unset($this->urls_added_at_level[$domainname]);
		unset($this->urls_counter[$domainname]); 		
		unset($this->urls_crawlered_counter[$domainname]); 		
		unset($this->active_curls[$domainname]);
		unset($this->process_domains[$domainname]);
	}

	private function getNextUrlData($domain) {
		$pos = $this->urls_crawlered_counter[$domain];
		if(!isset($this->urls[$domain][$pos]))
			return false;
			
		$url = $this->urls[$domain][$pos];
		$level = $this->urls_added_at_level[$domain][$url];// get level
		return array( 'url' => $url, 'level' => $level,'domain'=>$domain );
	}
	
	
	protected function heartbeat( ) {
		// each 5 sec 
		if( time() - $this->heartbeat_time < $this->heartbeat_timeout) 
			return ;
			
		Helper::system_down_check();
		$this->process->updateTime( $this->pid );	
		$this->heartbeat_time = time();
		
		//update Processes counters
		$s = array();	
		$t = 0;
		foreach($this->urls_counter as $domain=>$count) {
			$t+=$count;
			$s[] = $this->urls_crawlered_counter[$domain]." of $count -> $domain";
		}	
		$t2 = 0;
		foreach($this->urls_crawlered_counter as $domain=>$count)
			$t2+=$count;
		
		$this->query_update_process->execute( array(':domain'=>join("|",$s), ':urls'=>$t,':passed_urls'=>$t2,':pid'=>$this->pid) );
		
		//update Domains_job as processing&live
		foreach($this->urls_crawlered_counter as $domain=>$count)
			$this->query_update_job_domain->execute( array(':passed_urls'=>$count,':domain'=>$domain) );
		
		//debug 
		//file_put_contents( __DIR__."/urls.txt",var_export($this->urls,true));
		//file_put_contents( __DIR__."/urls_passed.txt",var_export($this->passed_urls,true));
		
		// check if job exists and still active 
		$this->query_get_job->execute( array(':id'=>$this->job_id) );
		$job = $this->query_get_job->fetch( PDO::FETCH_NAMED );
		if ( !$job OR $job['status'] != 'Processing') {
			$this->process->endProcess( $this->pid );	
			die("Job aborted");
		}
	}

	public function handler( $content, $url, $ch, $user_data ) {
		$this->heartbeat(); 
		
//		var_dump( $urls_params );
		$http_code		 = curl_getinfo($ch, CURLINFO_HTTP_CODE);    
		$url			 = $user_data[ 'url' ];
		$active_domain   = $this->process_domains[$user_data[ 'domain']];
		$domain			 = $this->extractDomain( $url, '' );
		$redirect_url	 = $this->getMetaRefresh( $content );

		$count_added_urls = 0;
		if ( $redirect_url ) {
			$redirect_url = $this->normolizeURL2( $domain, $redirect_url );
			$x = $active_domain['domain'];
			// add new url here for META refresh
			if ( $user_data[ 'level' ] < $this->job_max_depth  AND !isset($this->urls_added_at_level[$x][$redirect_url])) {
				$this->urls[$x][] = $redirect_url;
				$this->urls_added_at_level[$x][$redirect_url] = $user_data[ 'level' ]+1;
				$this->urls_counter[$x]++;
			}
		} else {
			//$this->addJobDomains( $domain,$active_domain['depth']+1);
			$list_nurls				 = $this->extractURLs( $content, $domain );
			//$list_nurls			 = $this->normolizeURLsNew1( $domain, $urls);
			$count_added_urls	 = $this->addURLs( $domain, $list_nurls, $user_data[ 'level' ], $url, $active_domain );
		}
		//echo "$url<br>";
		//echo "http_code: $http_code;count_added_urls: " . $count_added_urls . '<br>';
		
		// decrease # of running curls for domain
		$this->active_curls[$active_domain['domain']]--;
	}
	
	function addURLs( $domain, $list_url, $current_level, $source_url,$active_domain ) {
		$count	 = 0;
		$db		 = Db::getInstance();
		$next_level = $current_level+1;
		$x = $active_domain['domain'];
		
		foreach ( $list_url as $url ) {
			$extract_domain	 = $this->extractDomain( $url, $source_url );
			$ex				 = $this->checkExcludeDomain( $extract_domain );
			if ( empty( $url ) or $ex )
				continue;

			$this->addJobDomains( $extract_domain, $active_domain['depth']+1);
			$new_domain = $this->differentDomain( $extract_domain, $domain );
			if ( $new_domain )
				continue;

			$count++;
			
			//we check max_urls_per_domain for depth>0 ONLY!
			if ( $current_level < $this->job_max_depth AND !isset($this->urls_added_at_level[$x][$url]) AND ($active_domain['depth']==0 OR $this->urls_counter[$x]<$this->max_urls_per_domain) ) {
				// add new url here 
				$this->urls[$x][] = $url;
				$this->urls_added_at_level[$x][$url] = $next_level ;
				$this->urls_counter[$x]++;
			}
		}
		return $count;
	}
	
	
	function addJobDomains( $domain,$depth) {
		if ( !$domain )
			return; // zune , skype links?
		$tld = $this->getTLD( $domain );
		if ( !$tld )
			return; // wrong domains

		// get MAIN domain only, news.bbc.co.uk   ->  bbc.co.uk 
		$parts_num_get		 = 1 + count( explode( ".", $tld ) );
		$parts				 = explode( ".", $domain );
		$main_domain_parts	 = array_slice( $parts, count( $parts ) - $parts_num_get );
		$main_domain		 = join( ".", $main_domain_parts );
		$main_domain		 = mb_strtolower( $main_domain, 'UTF-8' );

		$date_found = date( 'Y-m-d H:i:s' );
		@$this->query_insert_job_domain->execute( array( 'domain' => $main_domain, 'tld' => $tld, 'date_found' => $date_found, 'depth'=>$depth ) );
	}
	
	protected function getProxy() {
		if ( !$this->proxies)
			return "";
		//rand 	
		$c = count( $this->proxies );
		$n = mt_rand( 0, $c - 1 );
		$proxy = $this->proxies[$n];
		return $proxy['address'];
		/*
		// make settings
		$curl_proxy_opt = array(
			CURLOPT_PROXY => $proxy['address'],
		);
		if ( $proxy['username'] and $proxy['password']) {
			$curl_proxy_opt[ CURLOPT_HTTPAUTH ]		 = CURLAUTH_BASIC;
			$curl_proxy_opt[ CURLOPT_PROXYUSERPWD ]	 = $proxy['username'].":".$proxy['password'];
		}
		return $curl_proxy_opt;
		*/
	}

	protected function getRandomUseragent() {
		if ( !$this->useragents ) 
			return '';
		$c = count( $this->useragents );
		$n = mt_rand( 0, $c - 1 );
		return $this->useragents[ $n ];
	}

	public function differentDomain( $domain1, $domain2 ) {
		// to lower case
		$domain1 = mb_strtolower( $domain1, 'UTF-8' );
		$domain1 = preg_replace( '#^www\.#', '', $domain1 );
		$domain2 = mb_strtolower( $domain2, 'UTF-8' );
		$domain2 = preg_replace( '#^www\.#', '', $domain2 );

		if ( $domain1 == $domain2 )
			return false;

		//1 
		$mask1		 = '#\.' . preg_quote( $domain1, '#' ) . "$#";
		$domain2_rev = "." . $domain2;
		if ( preg_match( $mask1, $domain2_rev ) )
			return false;

		//2 
		$mask2		 = '#\.' . preg_quote( $domain2, '#' ) . "$#";
		$domain1_rev = "." . $domain1;
		if ( preg_match( $mask2, $domain1_rev ) )
			return false;

		return true; // can't find by masks
	}

	public function checkExcludeDomain( $domain ) {
		foreach ( $this->excludeDomains as $exD ) {
			$r = $this->differentDomain( $domain, $exD );
			if ( !$r ) {
				return true;
			}
		}
		return false;
	}

//-+-+-+-+--+-+--+--+-

	function normolizeURLs( $domain, $list_urls ) {
		$list_nurls = array();
		foreach ( $list_urls as $url ) {
			$list_nurls[] = $this->normolizeURL2( $domain, $url );
		}
		return $list_nurls;
	}
  
  //---------------------------------------------------------------------------------------------------------------------------------------
  function normolizeURLsNew1( $domain, $list_urls ) {
		$list_nurls = array();
    
    $new_url = "";
		foreach ( $list_urls as $url ) {
      if ( substr($url,0,4) == "http" ) {
				$new_url = $url; 
      }  
      elseif ( $url[0] == "/" )  {
				$new_url = "http://".$domain.$url;     
      }
      elseif ( substr($url,0,3) == "../" )  {
				;     
      }
      else  {
				$new_url = "http://".$domain."/".$url;     
      }
    
      //echo file_put_contents( '/var/www/crawler/log_5.txt', $new_url."\n", FILE_APPEND );
    
			$list_nurls[] = $new_url;
		}
		return $list_nurls;
	}
  
  
  

	public function normolizeURL2( $domain, $url ) {
		$parts_url	 = PHPCrawlerUrlPartsDescriptor::fromURL( $domain );
		$norm_url	 = self::buildURLFromLink( $url, $parts_url );
		return self::buildURLFromLink( $url, $parts_url );
	}

	public static function buildURLFromLink( $link, PHPCrawlerUrlPartsDescriptor $BaseUrlParts ) {

		$url_parts = $BaseUrlParts->toArray();

		// Entities-replacements
		$entities = array( "'&(quot|#34);'i",
			"'&(amp|#38);'i",
			"'&(lt|#60);'i",
			"'&(gt|#62);'i",
			"'&(nbsp|#160);'i",
			"'&(iexcl|#161);'i",
			"'&(cent|#162);'i",
			"'&(pound|#163);'i",
			"'&(copy|#169);'i" );

		$replace = array( "\"",
			"&",
			"<",
			">",
			" ",
			chr( 161 ),
			chr( 162 ),
			chr( 163 ),
			chr( 169 ) );

		// Remove "#..." at end, but ONLY at the end,
		// not if # is at the beginning !
		$link = preg_replace( "/^(.{1,})#.{0,}$/", "\\1", $link );

		// Cases
		// Strange link like "//foo.htm" -> make it to "http://foo.html"
		if ( substr( $link, 0, 2 ) == "//" ) {
			$link = "http:" . $link;
		}

		// 1. relative link starts with "/" --> doc_root
		// "/index.html" -> "http://www.foo.com/index.html"    
		elseif ( substr( $link, 0, 1 ) == "/" ) {
			$link = $url_parts[ "protocol" ] . $url_parts[ "host" ] . ":" . $url_parts[ "port" ] . $link;
		}

		// 2. "./foo.htm" -> "foo.htm"
		elseif ( substr( $link, 0, 2 ) == "./" ) {
			$link = $url_parts[ "protocol" ] . $url_parts[ "host" ] . ":" . $url_parts[ "port" ] . $url_parts[ "path" ] . substr( $link, 2 );
		}

		// 3. Link is an absolute Link with a given protocol and host (f.e. "http://...")
		// DO NOTHING
		elseif ( preg_match( "#^[a-z0-9]{1,}(:\/\/)# i", $link ) ) {
			$link = $link;
		}

		// 4. Link is stuff like "javascript: ..." or something
		elseif ( preg_match( "/^[a-zA-Z]{0,}:[^\/]{0,1}/", $link ) ) {
			$link = "";
		}

		// 5. "../../foo.html" -> remove the last path from our actual path
		// and remove "../" from link at the same time until there are
		// no more "../" at the beginning of the link
		elseif ( substr( $link, 0, 3 ) == "../" ) {
			$new_path = $url_parts[ "path" ];

			while ( substr( $link, 0, 3 ) == "../" ) {
				$new_path	 = preg_replace( '/\/[^\/]{0,}\/$/', "/", $new_path );
				$link		 = substr( $link, 3 );
			}

			$link = $url_parts[ "protocol" ] . $url_parts[ "host" ] . ":" . $url_parts[ "port" ] . $new_path . $link;
		}

		// 6. link starts with #
		// -> leads to the same site as we are on, trash
		elseif ( substr( $link, 0, 1 ) == "#" ) {
			$link = "";
		}

		// 7. link starts with "?"
		elseif ( substr( $link, 0, 1 ) == "?" ) {
			$link = $url_parts[ "protocol" ] . $url_parts[ "host" ] . ":" . $url_parts[ "port" ] . $url_parts[ "path" ] . $url_parts[ "file" ] . $link;
		}

		// 7. thats it, else the abs_path is simply PATH.LINK ...
		else {
			$link = $url_parts[ "protocol" ] . $url_parts[ "host" ] . ":" . $url_parts[ "port" ] . $url_parts[ "path" ] . $link;
		}

		if ( $link == "" )
			return null;


		// Now, at least, replace all HTMLENTITIES with normal text !!
		// Fe: HTML-Code of the link is: <a href="index.php?x=1&amp;y=2">
		// -> Link has to be "index.php?x=1&y=2"
		//$link = preg_replace( $entities, $replace, $link );                                              //harald  we do not need that

		// Replace linebreaks in the link with "" (happens if a links in the sourcecode
		// linebreaks)
		$link = str_replace( array( "\n", "\r" ), "", $link );

		// "Normalize" URL
		$link = self::normalizeUrl( $link );

		return $link;
	}

	public static function normalizeURL( $url ) {
		$url_parts = self::splitURL( $url );

		if ( $url_parts == null )
			return null;

		$url_normalized = self::buildURLFromParts( $url_parts, true );
		return $url_normalized;
	}

	private function getMetaRefresh( $page ) {
		$page	 = preg_replace( '#<\!--(.*?)-->#s', '', $page );
		$pattern = '/<meta.*url=([^\'\" ;]*)/';
		preg_match( $pattern, $page, $matches );
		if ( isset( $matches[ 1 ] ) ) {
			return $matches[ 1 ];
		}
		return null;
	}

	public static function buildURLFromParts( $url_parts, $normalize = false ) {
		// Host has to be set aat least
		if ( !isset( $url_parts[ "host" ] ) ) {
			throw new Exception( "Cannot generate URL, host not specified!" );
		}

		if ( !isset( $url_parts[ "protocol" ] ) || $url_parts[ "protocol" ] == "" )
			$url_parts[ "protocol" ]		 = "http://";
		if ( !isset( $url_parts[ "port" ] ) )
			$url_parts[ "port" ]			 = 80;
		if ( !isset( $url_parts[ "path" ] ) )
			$url_parts[ "path" ]			 = "";
		if ( !isset( $url_parts[ "file" ] ) )
			$url_parts[ "file" ]			 = "";
		if ( !isset( $url_parts[ "query" ] ) )
			$url_parts[ "query" ]			 = "";
		if ( !isset( $url_parts[ "auth_username" ] ) )
			$url_parts[ "auth_username" ]	 = "";
		if ( !isset( $url_parts[ "auth_password" ] ) )
			$url_parts[ "auth_password" ]	 = "";

		// Autentication-part
		$auth_part = "";
		if ( $url_parts[ "auth_username" ] != "" && $url_parts[ "auth_password" ] != "" ) {
			$auth_part = $url_parts[ "auth_username" ] . ":" . $url_parts[ "auth_password" ] . "@";
		}

		// Port-part
		$port_part = ":" . $url_parts[ "port" ];

		// Normalize
		if ( $normalize == true ) {
			if ( $url_parts[ "protocol" ] == "http://" && $url_parts[ "port" ] == 80 ||
			$url_parts[ "protocol" ] == "https://" && $url_parts[ "port" ] == 443 ) {
				$port_part = "";
			}

			// Don't add port to links other than "http://" or "https://"
			if ( $url_parts[ "protocol" ] != "http://" && $url_parts[ "protocol" ] != "https://" ) {
				$port_part = "";
			}
		}

		// If path is just a "/" -> remove it ("www.site.com/" -> "www.site.com")
		if ( $url_parts[ "path" ] == "/" && $url_parts[ "file" ] == "" && $url_parts[ "query" ] == "" )
			$url_parts[ "path" ] = "";

		// Put together the url
		$url = $url_parts[ "protocol" ] . $auth_part . $url_parts[ "host" ] . $port_part . $url_parts[ "path" ] . $url_parts[ "file" ] . $url_parts[ "query" ];

		return $url;
	}

	public function extractDomain( $url, $source_url ) {
		$parts_url = self::splitURL( $url );
		if ( $parts_url[ 'protocol' ] != 'http://' AND $parts_url[ 'protocol' ] != 'https://' )
			return '';
		return trim( $parts_url[ 'host' ] );
	}

	private static function splitURL( $url ) {
		// Protokoll der URL hinzuf�gen (da ansonsten parse_url nicht klarkommt)
		if ( !preg_match( "#^[a-z]+://# i", $url ) )
			$url = "http://" . $url;

		$parts = @parse_url( $url );

		if ( !isset( $parts ) )
			return null;

		$protocol		 = $parts[ "scheme" ] . "://";
		$host			 = (isset( $parts[ "host" ] ) ? $parts[ "host" ] : "");
		$path			 = (isset( $parts[ "path" ] ) ? $parts[ "path" ] : "");
		$query			 = (isset( $parts[ "query" ] ) ? "?" . $parts[ "query" ] : "");
		$auth_username	 = (isset( $parts[ "user" ] ) ? $parts[ "user" ] : "");
		$auth_password	 = (isset( $parts[ "pass" ] ) ? $parts[ "pass" ] : "");
		$port			 = (isset( $parts[ "port" ] ) ? $parts[ "port" ] : "");

		// File
		preg_match( "#^(.*/)([^/]*)$#", $path, $match ); // Alles ab dem letzten "/"
		if ( isset( $match[ 0 ] ) ) {
			$file	 = trim( $match[ 2 ] );
			$path	 = trim( $match[ 1 ] );
		} else {
			$file = "";
		}

		// Der Domainname aus dem Host
		// Host: www.foo.com -> Domain: foo.com
		$parts = @explode( ".", $host );
		if ( count( $parts ) <= 2 ) {
			$domain = $host;
		} else if ( preg_match( "#^[0-9]+$#", str_replace( ".", "", $host ) ) ) { // IP
			$domain = $host;
		} else {
			$pos	 = strpos( $host, "." );
			$domain	 = substr( $host, $pos + 1 );
		}

		// DEFAULT VALUES f�r protocol, path, port etc. (wenn noch nicht gesetzt)
		// Wenn Protokoll leer -> Protokoll ist "http://"
		if ( $protocol == "" )
			$protocol = "http://";

		// Wenn Port leer -> Port setzen auf 80 or 443
		// (abh�ngig vom Protokoll)
		if ( $port == "" ) {
			if ( strtolower( $protocol ) == "http://" )
				$port	 = 80;
			if ( strtolower( $protocol ) == "https://" )
				$port	 = 443;
		}

		// Wenn Pfad leet -> Pfad ist "/"
		if ( $path == "" )
			$path = "/";

		// R�ckgabe-Array
		$url_parts[ "protocol" ] = $protocol;
		$url_parts[ "host" ]	 = $host;
		$url_parts[ "path" ]	 = $path;
		$url_parts[ "file" ]	 = $file;
		$url_parts[ "query" ]	 = $query;
		$url_parts[ "domain" ]	 = $domain;
		$url_parts[ "port" ]	 = $port;

		$url_parts[ "auth_username" ]	 = $auth_username;
		$url_parts[ "auth_password" ]	 = $auth_password;

		return $url_parts;
	}
	
	
	//parse page using libXML

	function extractURLs( $page_content, $domain ) {
		$url_list	 = array();
    
    $timestamp1 = microtime();
    

    
		if ( !$page_content )
			return $url_list;
		$dom		 = new DOMDocument();
		libxml_use_internal_errors( true );
		$dom->loadHTML( $page_content ); // loads your HTML
    
    $timestamp2 = microtime();
    
    
		foreach ( $dom->getElementsByTagName( 'a' ) as $node ) {
			$url_list[] = $node->getAttribute( 'href' );
		}
    
    
    $timestamp3 = microtime();
    
		//foreach ( $dom->getElementsByTagName( 'frame' ) as $node ) {                   //by harald, we do not need that
		//	$url_list[] = $node->getAttribute( 'src' );
		//}
		//foreach ( $dom->getElementsByTagName( 'iframe' ) as $node ) {
		//	$url_list[] = $node->getAttribute( 'src' );
		//}

		$url_list_ok = array();
    
    // memory leak http://stackoverflow.com/questions/8379829/domdocument-php-memory-leak
	unset($dom);
	libxml_use_internal_errors(false);
	libxml_use_internal_errors(true);    
    
    
    
    
		foreach ( $url_list as $url ) {
    
   
      $url = trim($url);
       

      if ( $url == "" OR $url == "/" OR $url == "#" OR $url == "./" OR $url == "index.html" OR $url == "index.php" OR $url == "index.htm" )
				continue;
        
      if ( $url[0] == "#" OR $url[0] == "?" )
				continue;  
        
      if ( substr($url,0,6) == "mailto" OR substr($url,0,10) == "javascript" OR substr($url,0,3) == "../")
				continue;  


			$url = explode( "?", $url ); // skip after ?
			$url = $url[ 0 ];
			// remove 
			//foreach ( $this->sess_masks as $re )                                           //by harald, we do not need that
			//	$url = preg_replace( $re, "", $url );

			$ext = explode( ".", $url );
			$ext = strtolower( end( $ext ) );
			
			//skip long ext
			if ( strlen( $ext ) < 5 ) {
        if ( $ext == "html" OR $ext == "htm" OR $ext == "php" OR $ext == "jsp" OR $ext == "asp" OR $ext == "aspx" OR $ext == "com" OR $ext == "org" OR $ext == "net") {
            ;
        }
        else {
          if ( in_array( $ext, $this->skip_ext )) {
             continue;  
          }           
        }  
		  }  
      
      if ( substr($url,0,4) == "http" ) {
				$url_list_ok[] = $url; 
      }  
      elseif ( $url[0] == "/" )  {
				$url_list_ok[] = "http://".$domain.$url;     
      }
      else  {
				$url_list_ok[] = "http://".$domain."/".$url;     
      }
    
      //echo file_put_contents( '/var/www/crawler/log_6.txt', $new_url."\n", FILE_APPEND );
    
			
    
                                                                                                                                                    
    
       
    
    }

    $timestamp4 = microtime();
    //echo file_put_contents( '/var/www/crawler/timelog_2.txt', $timestamp1." - ".$timestamp2." - ".$timestamp3." - ".$timestamp4."\n", FILE_APPEND );
    
		//print_r($url_list);print_r($url_list_ok);die($page_content );
		//$url_list_ok = array_filter( $url_list_ok );
		$url_list_ok = array_unique( $url_list_ok );
		return $url_list_ok;
	}

	
	//for TLDs
	
	function utf8_strrev( $str ) {
		preg_match_all( '/./us', $str, $ar );
		return join( '', array_reverse( $ar[ 0 ] ) );
	}

	function rebuildTLDs() {
		//get data 
		$source_url	 = "https://publicsuffix.org/list/effective_tld_names.dat";
		$lines		 = file( $source_url );

		//parse
		$regex = array();
		foreach ( $lines as $line ) {
			$line		 = trim( $line );
			if ( !$line OR substr( $line, 0, 2 ) == '//' )
				continue;
			$rev_line	 = mb_strtolower( $this->utf8_strrev( $line ), 'UTF-8' );
			$rev_line	 = preg_quote( $rev_line, '#' );
			if ( mb_substr( $rev_line, -2 ) == "\*" ) {
				$rev_line = mb_substr( $rev_line, 0, -2 ) . "[a-z0-9]+";
			}
			$regex[] = $rev_line;
		}
		// longer first
		usort( $regex, function ($a, $b) {
			return mb_strlen( $b, 'UTF-8' ) - mb_strlen( $a, 'UTF-8' );
		} );

		$masks		 = array();
		while ( $regex_part	 = array_splice( $regex, 0, 1000 ) )
			$masks[]	 = '#^(' . join( "|", $regex_part ) . ')\.#u';

		// everything is OK?
		if ( count( $masks ) > 5 ) {
			$tlds = array( 'updated_at' => time(), 'masks' => $masks );
			Helper::updateOption( 'tlds', $tlds );
		}
	}

	function initTLDs() {
		$current_tlds = Helper::getOption( 'tlds' );
		//$current_tlds = '';
		// not exists OR update week ago 
		if ( !$current_tlds OR time() - $current_tlds[ 'updated_at' ] > 7 * 24 * 3600 ) {
			$this->rebuildTLDs();
			$current_tlds = Helper::getOption( 'tlds' );
		}
		$this->regex_tld_masks = $current_tlds[ 'masks' ];
	}

	function getTLD( $domain ) {
		$rev_domain = mb_strtolower( $this->utf8_strrev( $domain ), 'UTF-8' );

		foreach ( $this->regex_tld_masks as $mask ) {
			if ( preg_match( $mask, $rev_domain, $m ) )
				return $this->utf8_strrev( $m[ 1 ] );
		}
		return "";
	}

}
