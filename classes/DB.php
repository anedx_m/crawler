<?php

class DB extends PDO {

	/**
	 *
	 * @var PDO 
	 */
	private static $_instance;
	private static $cache = array();

	public function __construct( $dsn, $username, $passwd, $options = array() ) {
		parent::__construct( $dsn, $username, $passwd, $options );
	}

	public static function getInstance() {
		if ( null === self::$_instance ) {
			self::$_instance = new DB( 'pgsql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USERNAME, DB_PASSWORD );
			//self::$_instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			self::$_instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
			self::$_instance->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);			
			//self::$_instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
			self::$_instance->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );
		}
		return self::$_instance;
	}

	public function prepare( $statement, $driver_options = array()) {
		if ( isset( self::$cache[ $statement ] ) ) {
			return self::$cache[ $statement ];
		} else {
			self::$cache[ $statement ] = parent::prepare( $statement, $driver_options );
		}
		return self::$cache[ $statement ];
	}

}
