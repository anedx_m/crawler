<?php
class DigExec {

	public function __construct( ) {
		$db					 = DB::getInstance();
		$this->update_nameserver_exists = $db->prepare( "UPDATE domains_offline SET nameserver_exists = 1 WHERE domain=:domain" ); 
		$this->update_nameserver_not_exists = $db->prepare( "UPDATE domains_offline SET nameserver_exists = 0, whois_check_avail_date = NULL WHERE domain=:domain" ); 
		
	}

	public function getNextDomains( $limit ) {
		$db				 = Db::getInstance();
		
		$min_moz_da_domainr	 = Helper::getSetting( 'min_moz_da_domainr' );
		$refresh_dns	 	 = Helper::getSetting( 'refresh_dns' );

		//try NEW domains_offline 
		$pr	= $db->prepare( "UPDATE domains_offline SET nameserver_check_date = current_date
					FROM  (    SELECT id   FROM   domains_offline WHERE  nameserver_check_date IS NULL AND moz_da>=:min_moz_da_domainr LIMIT :limit FOR UPDATE) sub
					WHERE  domains_offline.id = sub.id
				RETURNING domains_offline.domain as domain" );
		$r = $pr->execute( array( ':limit' => $limit, ':min_moz_da_domainr' => $min_moz_da_domainr ) );
		$results			 = $pr->fetchAll( PDO::FETCH_NAMED );
		if ( $results )
			return $results;
		
		//try AGE domains_offline 
		$pr	= $db->prepare( "UPDATE domains_offline SET nameserver_check_date = current_date
					FROM  (    SELECT id   FROM   domains_offline WHERE  age(current_date, nameserver_check_date)>interval '$refresh_dns days' AND moz_da>=:min_moz_da_domainr LIMIT :limit FOR UPDATE) sub
					WHERE  domains_offline.id = sub.id
				RETURNING domains_offline.domain as domain" );
		$r		 = $pr->execute( array( ':limit' => $limit, ':min_moz_da_domainr' => $min_moz_da_domainr ) );
		$results = $pr->fetchAll( PDO::FETCH_NAMED );
		return $results;
	}

	public function checkDNS($domain) {
		$exist = $this->nameserver_exists( $domain );
		echo "$domain -> $exist \n\n";
		if ( $exist AND strpos($exist,"connection timed out") === false) {
			$this->update_nameserver_exists->execute( array( ':domain'=>$domain  ) );
		} else {
			$this->update_nameserver_not_exists->execute( array( ':domain'=>$domain ) );
		}
	}


	private function nameserver_exists( $domain ) {
		return exec( "dig +tries=1 +time=5  ". escapeshellarg($domain) ." any +short" ); // not empty output if have any DNS records
	}
}
