<?php

class MajesticScraper extends MultiCurl {

	var $cookie = false;

	public function getNextDomain() {
		$db					 = DB::getInstance();
		$refresh_majestic	 = Helper::getSetting( 'refresh_majestic' );
		$min_moz_da			 = Helper::getSetting( 'min_moz_da' );

		$pr		 = $db->prepare( "UPDATE domains_offline SET ms_check_date = current_date
			FROM  (    SELECT id   FROM   domains_offline WHERE  (ms_check_date IS NULL OR age(current_date, ms_check_date)>interval '$refresh_majestic days') AND moz_da>=$min_moz_da AND whois_available=1 LIMIT 1 FOR UPDATE) sub
			WHERE  domains_offline.id = sub.id
		RETURNING domains_offline.domain as url, domains_offline.id as domain_id" );
		$r		 = $pr->execute();
		$results = $pr->fetch( PDO::FETCH_NAMED );
		if ( !empty( $results ) )
			return $results;

		$pr		 = $db->prepare( "UPDATE domains_offline SET ms_check_date = current_date
			FROM  (    SELECT id   FROM   domains_offline WHERE  (ms_check_date IS NULL OR age(current_date, ms_check_date)>interval '$refresh_majestic days') AND moz_da>=$min_moz_da LIMIT 1 FOR UPDATE) sub
			WHERE  domains_offline.id = sub.id
		RETURNING domains_offline.domain as url, domains_offline.id as domain_id" );
		$r		 = $pr->execute();
		$results = $pr->fetch( PDO::FETCH_NAMED );

		return $results;
	}

	public function processNextDomain() {
		$t_domain = $this->getNextDomain();
		if ( !$t_domain )
			return false;

		//first request gets cookie
		if ( !$this->cookie ) {
			$this->cookie = $this->login();
			echo "login with {$this->cookie}";
		}
		$curl_opts	 = array( CURLOPT_COOKIE => $this->cookie );
		$url		 = $t_domain[ 'url' ];

		$this->results	 = array();
		$t_urls			 = array( 0 =>
			array(
				'url'		 => "https://majestic.com/reports/site-explorer?folder=&IndexDataSource=F&q=$url",
				'type'		 => 'no-www',
				'domain_id'	 => $t_domain[ 'domain_id' ],
			)
		);
		$this->start( $t_urls, array( $this, 'handler' ), false, $curl_opts );
		sleep( 5 );
		$t_urls			 = array( 0 =>
			array(
				'url'		 => "https://majestic.com/reports/site-explorer?folder=&IndexDataSource=F&q=www.$url",
				'type'		 => 'www',
				'domain_id'	 => $t_domain[ 'domain_id' ],
			)
		);
		$this->start( $t_urls, array( $this, 'handler' ), false, $curl_opts );

		print_r( $this->results );
		echo $url;

		$db	 = DB::getInstance();
		$pr	 = $db->prepare( "UPDATE domains_offline SET ms_trust=:ms_trust,ms_citation=:ms_citation,ms_total_links=:ms_total_links,ms_linking_domains=:ms_linking_domains WHERE id=:domain_id" );
		if ( $this->results[ 'no-www' ][ ':ms_trust' ] >= $this->results[ 'www' ][ ':ms_trust' ] )
			$pr->execute( $this->results[ 'no-www' ] );
		else
			$pr->execute( $this->results[ 'www' ] );

		return count( $t_urls ) > 0;
	}

	public function handler( $content, $curl_info, $curl_multi_info, $urls_params ) {
		if ( strripos( $content, 'Fair Usage Limit Reached' ) ) {
			die( 'Fair Usage Limit Reached' );
		}
		$domain_id	 = $urls_params[ 'domain_id' ];
		$type		 = $urls_params[ 'type' ];

		$pattern = '/<meta name="twitter:description" content="Trust Flow: ([\d\/\-,]*?) \|\| Citation Flow: ([\d\/\-,]*?) \|\| External Backlinks: ([\d\/\-,]*?) \|\| Referring Domains: ([\d\/\-,]*?)">/';
		preg_match( $pattern, $content, $matches );
		if ( !empty( $matches ) ) {
			$p = explode( '/', $matches[ 1 ] );
			if ( $p[ 0 ] == '-' ) {
				$ms_trust = 0;
			} else {
				$ms_trust = $p[ 0 ];
			}
			$p = explode( '/', $matches[ 2 ] );
			if ( $p[ 0 ] == '-' ) {
				$ms_citation = 0;
			} else {
				$ms_citation = $p[ 0 ];
			}
			if ( $matches[ 3 ] == '-' ) {
				$ms_total_links = 0;
			} else {
				$ms_total_links = str_replace( ',', '', $matches[ 3 ] );
			}
			if ( $matches[ 4 ] == '-' ) {
				$ms_linking_domains = 0;
			} else {
				$ms_linking_domains = str_replace( ',', '', $matches[ 4 ] );
			}
			$this->results[ $type ] = array( ':domain_id' => $domain_id, ':ms_trust' => $ms_trust, ':ms_citation' => $ms_citation, ':ms_total_links' => $ms_total_links, ':ms_linking_domains' => $ms_linking_domains );
		} else
			$this->results[ $type ] = array( ':domain_id' => $domain_id, ':ms_trust' => 0, ':ms_citation' => 0, ':ms_total_links' => 0, ':ms_linking_domains' => 0 );
	}

	public function login() {
		$header = array(
			"Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5",
			"Accept-Language: en-us",
			"Accept-Charset: SO-8859-1,utf-8;q=0.7,*;q=0.7",
			"Keep-Alive: 300",
			"Connection: keep-alive",
			"Pragma: no-cache",
			"Cache-Control: no-cache",
			"Expect:",
		);

		$data			 = array(
			'EmailAddress'	 => Helper::getSetting( 'ms_username' ),
			'Password'		 => Helper::getSetting( 'ms_password' ),
			'RememberMe'	 => 1
		);
		$curl_options	 = array(
//			CURLOPT_PORT			 => 80,
			CURLOPT_RETURNTRANSFER	 => 1,
			CURLOPT_BINARYTRANSFER	 => 1,
			CURLOPT_CONNECTTIMEOUT	 => 15,
			CURLOPT_TIMEOUT			 => 15,
			CURLOPT_USERAGENT		 => 'Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.1.1) Gecko/20090716 Ubuntu/9.04 (jaunty) Shiretoko/3.5.1',
			CURLOPT_VERBOSE			 => 0, //was 2 
			CURLOPT_HEADER			 => 1,
			CURLOPT_FOLLOWLOCATION	 => 0,
			CURLOPT_AUTOREFERER		 => 1,
			CURLOPT_FRESH_CONNECT	 => 1,
			CURLOPT_HTTPHEADER		 => $header,
			CURLOPT_SSL_VERIFYPEER	 => false,
			CURLOPT_SSL_VERIFYHOST	 => false,
			CURLOPT_POST			 => 1,
			CURLOPT_POSTFIELDS		 => http_build_query( $data ),
//			CURLOPT_SSLVERSION		 => 3,
//			CURLOPT_IPRESOLVE		 => CURL_IPRESOLVE_V4,
		);

		$ch = curl_init( 'https://majestic.com/account/login' );

		curl_setopt_array( $ch, $curl_options );
		$r = curl_exec( $ch );
		$this->extractCookies( $r, $cookies );

		if ( strpos( $r, "Location: /account\r\n" ) == false ) {
			die( "Login failed. Wrong redirect." );
		}

		if ( empty( $cookies[ 'STOK' ] ) ) {
			die( "Login failed. No cookie." );
		}
		curl_close( $ch );
		return "STOK={$cookies[ 'STOK' ]};";
	}

	private function extractCookies( $page, &$cookies ) {
		if ( preg_match_all( "/Set-Cookie: (.+?)=(.*?)(;|\n)/i", $page, $m ) )
			for ( $i = 0; $i < count( $m[ 0 ] ); $i++ ) {
				$cookies[ $m[ 1 ][ $i ] ] = trim( $m[ 2 ][ $i ] );
			}
	}

}
