<?php

class WhoisAPIAvailable extends MultiCurl {

	public function __construct( $curl_options = array(), $connection_timeout = 30, $max_page_size = 1024100 ) {
		$db								 = DB::getInstance();
		$connection_timeout				 = Helper::getSetting( 'connection_timeout' );
		$max_page_size					 = Helper::getSetting( 'max_page_size' );
		parent::__construct( $curl_options, $connection_timeout, $max_page_size );
		$this->query_select_random_proxy = $db->prepare( 'SELECT * FROM proxies ORDER BY random() LIMIT 1;' );
	}

	public function getNextDomains( $limit ) {
		$db				 = Db::getInstance();
		$refresh_whois	 = Helper::getSetting( 'refresh_whois' );

		/*
		//try domains_offline at first 
		$pr				 = $db->prepare( "
		UPDATE domains_all SET whois_check_avail_date = current_date
			FROM  (    SELECT id   FROM   domains_all WHERE  (whois_check_avail_date IS NULL OR age(current_date, whois_check_avail_date)>interval '$refresh_whois days') AND domain IN (SELECT domain FROM domains_offline) LIMIT :limit FOR UPDATE) sub
			WHERE  domains_all.id = sub.id
		RETURNING domains_all.domain as url, domains_all.id as domain_id" ); //debug 
		$r				 = $pr->execute( array( ':limit' => $limit ) );
		$start_urls		 = $pr->fetchAll( PDO::FETCH_NAMED );
		if(!empty($start_urls)) {
			return $start_urls;
		}
		*/

		
		$pr				 = $db->prepare( "
		UPDATE domains_all SET whois_check_avail_date = current_date
			FROM  (    SELECT id   FROM   domains_all WHERE  whois_check_avail_date IS NULL OR age(current_date, whois_check_avail_date)>interval '$refresh_whois days' LIMIT :limit FOR UPDATE) sub
			WHERE  domains_all.id = sub.id
		RETURNING domains_all.domain as url, domains_all.id as domain_id" ); //debug 
		$r				 = $pr->execute( array( ':limit' => $limit ) );
		$start_urls		 = $pr->fetchAll( PDO::FETCH_NAMED );

		return $start_urls;
	}

	public function processNextDomains( $limit ) {
		$domains	 = $this->getNextDomains( $limit );
		$whoiskey	 = Helper::getSetting( 'whois_api_key' );
		$urls		 = array();
		foreach ( $domains as $domain ) {
			$domain_url	 = $domain[ 'url' ];
			$urls[]		 = array(
				'url'		 => $this->getWhoisUrl( $domain_url, $whoiskey ),
				'domain_id'	 => $domain[ 'domain_id' ],
				'domain'	 => $domain_url,
			);
		}
		//var_dump( $urls );
		if ( count( $urls ) ) {
			$curl_params = array( CURLOPT_ENCODING, "gzip,deflate" );
			$this->start( $urls, array( $this, 'handler' ), FALSE, $curl_params );
		}

		return count( $domains );
	}
	
	function do_extra_api_check($domain) {
		$addr = "https://api.domrobot.com/xmlrpc/";
		//$addr = "https://api.ote.domrobot.com/xmlrpc/";
		$usr = "tripleaom";
		$pwd = "kgza5vaa";
		$domrobot = new Domrobot($addr);
		$domrobot->setDebug(false);
		$domrobot->setLanguage('en');
		$res = $domrobot->login($usr,$pwd);

		if ($res['code']==1000) {
			$obj = "domain";
			$meth = "check";
			$params = array();
			$params['domain'] = $domain;
			$res = $domrobot->call($obj,$meth,$params);
			if ($res['code']==1000 AND isset($res['resData']['domain'][0]['avail'])) {
				return $res['resData']['domain'][0]['avail'];
			}
		} 
		print_r($res);
		return 1; // Yes it's avaliable still if API failed
	}

	protected function handler( $content, $curl_info, $curl_multi_info, $urls_params ) {
		echo '--------';
		if ( $curl_info[ 'http_code' ] == 200 ) {
			$db		 = DB::getInstance();
			$data	 = json_decode( $content, true );
			//var_dump( $data, $content );
			if ( $data[ 'status' ] == 0 AND isset( $data[ 'taken' ] ) ) {
				$whois_available = $data[ 'taken' ] ? 0 : 1; // revert value
 				if ( $whois_available ) {
					$whois_available = $this->do_extra_api_check($urls_params[ 'domain' ]);
					echo "$urls_params[domain] is avaliable?, 2nd api return $whois_available\n";
				}	
					
				$domain_id		 = $urls_params[ 'domain_id' ];
				$domain			 = $urls_params[ 'domain' ];
				
				$pr = $db->prepare( "UPDATE domains_all SET whois_available=:whois_available, whois_check_avail_date=current_date WHERE id=:domain_id" );
				$pr->execute( array( ':domain_id' => $domain_id, ':whois_available' => $whois_available ) );
				
				$pr = $db->prepare( "UPDATE domains_offline SET whois_available=:whois_available WHERE domain=:domain" );
				$pr->execute( array( ':domain' => $domain, ':whois_available' => $whois_available ) );
// 				}
			}
		} else {
			var_dump( $curl_info );
		}
		echo '--------';
	}

	public function getWhoisUrl( $domain, $key ) {
		return "http://api.whoapi.com/?domain=$domain&r=taken&apikey=$key";
	}

}
