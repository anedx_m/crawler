<?php

class WhoisAPIDateExpire extends MultiCurl {

	public function __construct( $curl_options = array(), $connection_timeout = 30, $max_page_size = 1024100 ) {
		$db								 = DB::getInstance();
		$connection_timeout				 = Helper::getSetting( 'connection_timeout' );
		$max_page_size					 = Helper::getSetting( 'max_page_size' );
		parent::__construct( $curl_options, $connection_timeout, $max_page_size );
		$this->query_select_random_proxy = $db->prepare( 'SELECT * FROM proxies ORDER BY random() LIMIT 1;' );
	}

	public function getNextDomains( $limit ) {
		$db			 = Db::getInstance();
		$refresh_whois = Helper::getSetting('refresh_whois');
		$pr			 = $db->prepare( "
		UPDATE domains_offline SET whois_check_expire_date = current_date
			FROM  (    SELECT id   FROM   domains_offline WHERE (whois_check_expire_date IS NULL OR age(current_date, whois_check_expire_date)>interval '$refresh_whois days') AND whois_available=0 LIMIT :limit FOR UPDATE) sub
			WHERE  domains_offline.id = sub.id
		RETURNING domains_offline.domain as url, domains_offline.id as domain_id" ); //debug 
		$r			 = $pr->execute( array( ':limit' => $limit ) );
		$start_urls	 = $pr->fetchAll( PDO::FETCH_NAMED );

		return $start_urls;
	}

	public function processNextDomains( $limit ) {
		$domains	 = $this->getNextDomains( $limit );
		$whoiskey	 = Helper::getSetting( 'whois_api_key' );
		$urls		 = array();
		foreach ( $domains as $domain ) {
			$domain_url	 = $domain[ 'url' ];
			$urls[]		 = array(
				'url'		 => $this->getWhoisUrl( $domain_url, $whoiskey ),
				'domain_id'	 => $domain[ 'domain_id' ],
				'domain'	 => $domain_url,
			);
		}
		var_dump( $urls );
		if ( count( $urls ) ) {
			$curl_params = array( CURLOPT_ENCODING, "gzip,deflate" );
			$this->start( $urls, array( $this, 'handler' ), FALSE, $curl_params );
		}

		return count( $domains );
	}

	protected function handler( $content, $curl_info, $curl_multi_info, $urls_params ) {
		echo '--------';
		if ( $curl_info[ 'http_code' ] == 200 ) {
			$db		 = DB::getInstance();
			$data	 = json_decode( $content, true );
			var_dump( $data );
			if ( $data[ 'status' ] == 0 AND isset( $data[ 'date_expires' ] ) ) {
				$whois_expire_date	 = $data[ 'date_expires' ];
				$domain_id		 = $urls_params[ 'domain_id' ];
				$domain			 = $urls_params[ 'domain' ];

				if($whois_expire_date) {
					$pr = $db->prepare( "UPDATE domains_offline SET whois_expire_date=:whois_expire_date, whois_check_expire_date=current_date WHERE id=:domain_id" );
					$pr->execute( array( ':domain_id' => $domain_id, ':whois_expire_date' => $whois_expire_date ) );
				} else {
//					$pr = $db->prepare( "UPDATE domains_offline SET whois_check_expire_date=current_date WHERE id=:domain_id" );
//					$pr->execute( array( ':domain_id' => $domain_id) );
				}	
			}
		} else {
			var_dump( $curl_info );
		}
		echo '--------';
	}

	public function getWhoisUrl( $domain, $key ) {
		return "http://api.whoapi.com/?domain=$domain&r=whois&apikey=$key";
	}

}
