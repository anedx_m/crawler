<?php

class MyScraper {

    protected $DB;

    const MAX_REDIRECT = 5;
    const MAX_LINKS = 100;
    const MAX_LEVEL = 3;
    public function __construct($connectionString, $user, $pass) {
        $this->DB = new PDO($connectionString, $user, $pass);
    }

    function checkDomainURL($url) {
        $domain = $this->exctractDomain($url);
//        var_dump($domain);
        $tdomain = $this->DB->query("SELECT * FROM domains WHERE domain='$domain'")->fetchColumn();
        if ($tdomain) {
            $this->DB->query("UPDATE domains SET crawl_status='found')");
        } else {
            $this->DB->query("INSERT IGNORE INTO domains (domain,crawl_status) VALUES ('$domain','new')");
        }
        return $domain;
    }
    function getNextDomain () {
//        $pid = '-'.getmypid();
//        $r = $this->DB->exec("UPDATE pages SET status=$pid WHERE status=0 LIMIT 1");
//        if(!$r){
//            return NULL;
//        }
        return $this->DB->query("SELECT * FROM `domains` WHERE DATE(dk_update_date) <  ADDDATE(NOW(), -1) LIMIT 1")->fetch(PDO::FETCH_NAMED);        
    }
    function getNextURL() {
        $pid = getmypid();
        $r = $this->DB->exec("UPDATE pages SET `lock`=$pid WHERE status=0 LIMIT 1");
        if(!$r){
            return NULL;
        }
        
        return $this->DB->query("SELECT * FROM pages WHERE `lock`=$pid LIMIT 1")->fetch(PDO::FETCH_NAMED);
    }

    function loadURLs($pathfile) {
        $handle = fopen('/home/andrey/all.work/dk-crawler/3k_dk_domains.txt', 'r');
        echo $handle;
//    $handle = fopen($pathfile, 'r');
        if ($handle) {
            while (($row = fgets($handle, 255)) !== FALSE) {

                $url = parse_url($row);

                $host = 'http://' . $url['host'];
                $urlhost = $url['host'];
                $r = $this->DB->query("INSERT IGNORE INTO pages (url,level,domain) VALUES ('$host',0,'$urlhost')");
//                var_dump($url);
            }
        }
    }

    function exctractDomain($url) {
//        var_dump($url);
//        $pattern = '/^http/';
//        $absURL = preg_match($pattern, $url);
//        $domain = '';
//        if($absURL){
          $parts_url = self::splitURL($url);
//        }
//        var_dump($domain);
        return $parts_url['host'];
    }

    function getBody($page) {
        return preg_replace('#HTTP/\d\.\d.*?$.*?\r?\n\r?\n#ims', '', $page);
    }

    function getMetaRefresh($page) {
        $pattern = '/<meta.*url=([^\'\" ;]*)/';
        preg_match($pattern, $page, $matches);
        if (isset($matches[1])) {
            return $matches[1];
        }
        return null;
    }

    function getPageContent($url, $data = '', $head = 0, $follow = 1, $referer = '') {
        $redirect = 0;
        while ($redirect <= self::MAX_REDIRECT) {
            $domain = $this->exctractDomain($url);
            $page = $this->_getPageContent($url, $data = '', $head = 0, $follow = 1, $referer = '');
//            var_dump($page);
            $redirect_url = $this->getMetaRefresh($page['content']);
            
            if ($redirect_url) {
                $url = $this->normolizeURL2($domain,$redirect_url);
            } else {
                break;
            }
            $redirect++;
        }
        return $page;
    }

    private function _getPageContent($url, $data = '', $head = 0, $follow = 1, $referer = '') {
//        var_dump($url);

        $ch = curl_init();
        $header[] = "Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
        $header[] = "Accept-Language: en-us";
        $header[] = "Accept-Charset: SO-8859-1,utf-8;q=0.7,*;q=0.7";
        $header[] = "Keep-Alive: 300";
        $header[] = "Connection: keep-alive";
        $header[] = "X-Requested-With: XMLHttpRequest";
        $header[] = "Pragma: no-cache";
        $header[] = "Cache-Control: no-cache";
        $header[] = "Expect:";

        if (empty($referer))
            $referer = $url;

        curl_setopt($ch, CURLOPT_HEADER, $head);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, "ozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_REFERER, $referer);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, self::MAX_REDIRECT);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        //curl_setopt ($ch, CURLOPT_COOKIEFILE, $this->cookieFile);
        //curl_setopt ($ch, CURLOPT_COOKIEJAR, $this->cookieFile);
//        $cc = array();
//        foreach ($this->cookies as $k => $v)
//            $cc[] = "$k=$v";
//        curl_setopt($ch, CURLOPT_COOKIE, join("; ", $cc));
//

        if ($data) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }

        
        sleep(mt_rand(1, 2));
        $d['content'] = curl_exec($ch);
        $err        = curl_error($ch);
        $d['info']  = curl_getinfo($ch);
        curl_close($ch);

        if ($err) {
            //throw new Exception("curl returned '$err' for $url");
        }
        return $d;
    }

    function exctractURLs($page_content, $url, $level) {
        $url_list = array();
        if(!$page_content) 
            return $url_list;
        $doc = new DOMDocument();
        libxml_use_internal_errors(true);
        $doc->loadHTML($page_content); // loads your HTML
        $xpath = new DOMXPath($doc);
        $nlist = $xpath->query("//a|//frame|//iframe");
        
        var_dump($nlist);
        
        if (!is_null($nlist)) {
//            var_dump(1);
            foreach ($nlist as $nElement) {
//                var_dump(1);
//                var_dump($nElement->tagName);
                switch ($nElement->tagName){
                    case 'frame':
                        if(!empty($nElement->getAttributeNode('src'))){
                            $url_list[] = $nElement->getAttributeNode('src')->value;    
                        }
                        break;
                    case 'iframe':
                        if(!empty($nElement->getAttributeNode('src'))){
                            $url_list[] = $nElement->getAttributeNode('src')->value;    
                        }
                        break;
                    case 'a':
                        if(!empty($nElement->getAttributeNode('href'))){
                            $url_list[] = $nElement->getAttributeNode('href')->value;    
                        }
                        break;
                }
               
            }
        }
        return $url_list;
    }

    function addDKURLs($domain, $list_url, $current_level) {
        $count = 0;
        $remained = self::MAX_LINKS - $this->countURLs($domain);

        foreach ($list_url as $url) {

            $exctract_domain = $this->exctractDomain($url);
//            $domain_1 = substr($exctract_domain, -3);
//            var_dump($domain_1);
            if (substr($exctract_domain, -3) == '.dk') {

                
                if($domain == $exctract_domain){
                    $level = $current_level + 1;
                    $count++;
                    if ($count >= $remained or $current_level > self::MAX_LEVEL) {
                        continue;
                    }
                }else{
                    $level = 0;
                }
                $r = $this->DB->query("INSERT IGNORE INTO pages (url,level,domain) VALUES ('$url',$level,'$exctract_domain')");
                $this->checkDomainURL($url);
                
            }
        }
        return $count;
    }

    function countURLs($domain){
        return $this->DB->query("SELECT COUNT(*) FROM pages WHERE domain='$domain'")->fetchColumn();
    }
    function precessURL($tURL) {
        $url    = $tURL['url'];
        $domain = $tURL['domain'];
        $level  = $tURL['level'];
        $tdomain = $this->checkDomainURL($url);
        var_dump($tdomain);
//        $url = $this->normolizeURL2($domain, $url);
        $page = $this->getPageContent($url);
//        echo "<pre>".htmlspecialchars($page);die();
//        echo($page['content']);
        $domain = $this->exctractDomain($page['info']['url']);
        var_dump($domain);
        $URLs = $this->exctractURLs($page['content'], $url, $level);
        var_dump($URLs);
        
        $list_nurls = $this->normolizeURLs($domain,$URLs);
        var_dump($list_nurls);
        $count_added_urls = $this->addDKURLs($domain,$list_nurls, $level);     
        echo 'count_added_urls: ' . $count_added_urls;
        
//        var_dump($page['info']);
        $code = $page['info']['http_code'];
//        echo $code;
        if($code==0){
            $code=404;
        }
        $this->updateStatusPage($tURL['id'],$code);
        echo '_______________________';
    }
    function updateStatusPage($idPage,$status){
        $r = $this->DB->query("UPDATE pages SET status='$status' WHERE id=$idPage");
        var_dump($this->DB->errorInfo());
    }

    function normolizeURLs($domain,$list_urls) {
        $list_nurls = array();
        foreach ($list_urls as $url){
            $list_nurls[] = $this->normolizeURL2($domain, $url);
        }
        return $list_nurls;
    }
    public function normolizeURL2($domain,$url){
        $parts_url = PHPCrawlerUrlPartsDescriptor::fromURL($domain);
        $norm_url = self::buildURLFromLink($url, $parts_url);
        return self::buildURLFromLink($url, $parts_url);
    }
    public static function normalizeURL($url) {
        $url_parts = self::splitURL($url);

        if ($url_parts == null)
            return null;

        $url_normalized = self::buildURLFromParts($url_parts, true);
        return $url_normalized;
    }
    public function getStatusDKDomain($domain){
        $domain = $this->normolizeDomain($domain);
//        var_dump($domain);
        $query = "https://www.dk-hostmaster.dk/index.php?id=42&query=$domain&submit=S%F8g";
//        var_dump($query);
        $page = $this->getPageContent($query);
        $content = $page['content'];
        $pattern = '/Status:.*<\/td>\n<td[^>]*>(.*)<\/td>/';
        preg_match($pattern, $content, $matches);
        if(isset($matches[1])){
            return $matches[1];
        }else{
            return null;
        }
//        var_dump($matches);
    }
    public function setMozStatus($idDomain,$status){
        $r = $this->DB->query("UPDATE domains SET moz_status='$status' WHERE id=$idDomain");
        var_dump($this->DB->errorInfo());
    }
    public function setDomainStatus($idDomain,$status){
        $now = date('Y-m-d');
        $r = $this->DB->query("UPDATE domains SET dk_status='$status', dk_update_date='$now' WHERE id=$idDomain");
        var_dump($this->DB->errorInfo());
    }
    public function normolizeDomain($domain){
//        echo $domain;
//        echo stristr($domain, 'www');
//        if(stristr($domain, 'www.')){
//            
//            $domain = substr($domain, 5);
//        }
        
        $p = strripos($domain, '.');
        $s = substr($domain, 0, $p);
//        echo $p.' '.$s;
        $p = strripos($s, '.');
        if($p){
            $domain = substr($domain, $p+1); 
        }
        
        return $domain;
    }
    public static function splitURL($url) {
        // Protokoll der URL hinzuf�gen (da ansonsten parse_url nicht klarkommt)
        if (!preg_match("#^[a-z]+://# i", $url))
            $url = "http://" . $url;

        $parts = @parse_url($url);

        if (!isset($parts))
            return null;

        $protocol = $parts["scheme"] . "://";
        $host = (isset($parts["host"]) ? $parts["host"] : "");
        $path = (isset($parts["path"]) ? $parts["path"] : "");
        $query = (isset($parts["query"]) ? "?" . $parts["query"] : "");
        $auth_username = (isset($parts["user"]) ? $parts["user"] : "");
        $auth_password = (isset($parts["pass"]) ? $parts["pass"] : "");
        $port = (isset($parts["port"]) ? $parts["port"] : "");

        // File
        preg_match("#^(.*/)([^/]*)$#", $path, $match); // Alles ab dem letzten "/"
        if (isset($match[0])) {
            $file = trim($match[2]);
            $path = trim($match[1]);
        } else {
            $file = "";
        }

        // Der Domainname aus dem Host
        // Host: www.foo.com -> Domain: foo.com
        $parts = @explode(".", $host);
        if (count($parts) <= 2) {
            $domain = $host;
        } else if (preg_match("#^[0-9]+$#", str_replace(".", "", $host))) { // IP
            $domain = $host;
        } else {
            $pos = strpos($host, ".");
            $domain = substr($host, $pos + 1);
        }

        // DEFAULT VALUES f�r protocol, path, port etc. (wenn noch nicht gesetzt)
        // Wenn Protokoll leer -> Protokoll ist "http://"
        if ($protocol == "")
            $protocol = "http://";

        // Wenn Port leer -> Port setzen auf 80 or 443
        // (abh�ngig vom Protokoll)
        if ($port == "") {
            if (strtolower($protocol) == "http://")
                $port = 80;
            if (strtolower($protocol) == "https://")
                $port = 443;
        }

        // Wenn Pfad leet -> Pfad ist "/"
        if ($path == "")
            $path = "/";

        // R�ckgabe-Array
        $url_parts["protocol"] = $protocol;
        $url_parts["host"] = $host;
        $url_parts["path"] = $path;
        $url_parts["file"] = $file;
        $url_parts["query"] = $query;
        $url_parts["domain"] = $domain;
        $url_parts["port"] = $port;

        $url_parts["auth_username"] = $auth_username;
        $url_parts["auth_password"] = $auth_password;

        return $url_parts;
    }

    public static function buildURLFromParts($url_parts, $normalize = false)
    {
        // Host has to be set aat least
        if (!isset($url_parts["host"]))
        {
          throw new Exception("Cannot generate URL, host not specified!");
        }

        if (!isset($url_parts["protocol"]) || $url_parts["protocol"] == "") $url_parts["protocol"] = "http://";
        if (!isset($url_parts["port"])) $url_parts["port"]= 80;
        if (!isset($url_parts["path"])) $url_parts["path"] = "";
        if (!isset($url_parts["file"])) $url_parts["file"] = "";
        if (!isset($url_parts["query"])) $url_parts["query"]= "";
        if (!isset($url_parts["auth_username"])) $url_parts["auth_username"]= "";
        if (!isset($url_parts["auth_password"])) $url_parts["auth_password"]= "";

        // Autentication-part
        $auth_part = "";
        if ($url_parts["auth_username"] != "" && $url_parts["auth_password"] != "")
        {
          $auth_part = $url_parts["auth_username"].":".$url_parts["auth_password"]."@";
        }

        // Port-part
        $port_part = ":" . $url_parts["port"];

        // Normalize
        if ($normalize == true)
        {
          if ($url_parts["protocol"] == "http://" && $url_parts["port"] == 80 ||
              $url_parts["protocol"] == "https://" && $url_parts["port"] == 443)
          {
            $port_part = "";
          }

          // Don't add port to links other than "http://" or "https://"
          if ($url_parts["protocol"] != "http://" && $url_parts["protocol"] != "https://")
          {
            $port_part = "";
          }
        }

        // If path is just a "/" -> remove it ("www.site.com/" -> "www.site.com")
        if ($url_parts["path"] == "/" && $url_parts["file"] == "" && $url_parts["query"] == "") $url_parts["path"] = "";

        // Put together the url
        $url = $url_parts["protocol"] . $auth_part . $url_parts["host"]. $port_part . $url_parts["path"] . $url_parts["file"] . $url_parts["query"];

        return $url;
    }
  
    /**
     * Builds an URL from it's single parts.
     *
     * @param array $url_parts Array conatining the URL-parts.
     *                         The keys should be:
     *
     *                         "protocol" (z.B. "http://") OPTIONAL
     *                         "host"     (z.B. "www.bla.de")
     *                         "path"     (z.B. "/test/palimm/") OPTIONAL
     *                         "file"     (z.B. "index.htm") OPTIONAL
     *                         "port"     (z.B. 80) OPTIONAL
     *                         "auth_username" OPTIONAL
     *                         "auth_password" OPTIONAL
     * @param bool $normalize   If TRUE, the URL will be returned normalized.
     *                          (I.e. http://www.foo.com/path/ insetad of http://www.foo.com:80/path/)
     * @return string The URL
     *                         
     */
    public static function buildURLFromLink($link, PHPCrawlerUrlPartsDescriptor $BaseUrlParts)
    { 

        $url_parts = $BaseUrlParts->toArray();

        // Entities-replacements
        $entities= array ("'&(quot|#34);'i",
                            "'&(amp|#38);'i",
                            "'&(lt|#60);'i",
                            "'&(gt|#62);'i",
                            "'&(nbsp|#160);'i",
                            "'&(iexcl|#161);'i",
                            "'&(cent|#162);'i",
                            "'&(pound|#163);'i",
                            "'&(copy|#169);'i");

        $replace=array ("\"",
                        "&",
                        "<",
                        ">",
                        " ",
                        chr(161),
                        chr(162),
                        chr(163),
                        chr(169));

       // Remove "#..." at end, but ONLY at the end,
       // not if # is at the beginning !
       $link = preg_replace("/^(.{1,})#.{0,}$/", "\\1", $link);

       // Cases

       // Strange link like "//foo.htm" -> make it to "http://foo.html"
       if (substr($link, 0, 2) == "//")
       {
         $link = "http:".$link;
       }

       // 1. relative link starts with "/" --> doc_root
       // "/index.html" -> "http://www.foo.com/index.html"    
       elseif (substr($link,0,1)=="/")
       {
         $link = $url_parts["protocol"].$url_parts["host"].":".$url_parts["port"].$link;
       }

        // 2. "./foo.htm" -> "foo.htm"
        elseif (substr($link,0,2)=="./")
        {
          $link=$url_parts["protocol"].$url_parts["host"].":".$url_parts["port"].$url_parts["path"].substr($link, 2);
        }

        // 3. Link is an absolute Link with a given protocol and host (f.e. "http://...")
        // DO NOTHING
        elseif (preg_match("#^[a-z0-9]{1,}(:\/\/)# i", $link))
        {
          $link = $link;
        }

        // 4. Link is stuff like "javascript: ..." or something
        elseif (preg_match("/^[a-zA-Z]{0,}:[^\/]{0,1}/", $link))
        {
          $link = "";
        }

        // 5. "../../foo.html" -> remove the last path from our actual path
        // and remove "../" from link at the same time until there are
        // no more "../" at the beginning of the link
        elseif (substr($link, 0, 3)=="../")
        {
          $new_path = $url_parts["path"];

          while (substr($link, 0, 3) == "../")
          {
            $new_path = preg_replace('/\/[^\/]{0,}\/$/',"/", $new_path);
            $link  = substr($link, 3);
          }

          $link = $url_parts["protocol"].$url_parts["host"].":".$url_parts["port"].$new_path.$link;
        }

        // 6. link starts with #
        // -> leads to the same site as we are on, trash
        elseif (substr($link,0,1) == "#")
        {
          $link="";
        }

        // 7. link starts with "?"
        elseif (substr($link,0,1)=="?")
        {
          $link = $url_parts["protocol"].$url_parts["host"].":".$url_parts["port"].$url_parts["path"].$url_parts["file"].$link;
        }

        // 7. thats it, else the abs_path is simply PATH.LINK ...
        else
        { 
          $link = $url_parts["protocol"].$url_parts["host"].":".$url_parts["port"].$url_parts["path"].$link;
        }

        if ($link == "") return null;


        // Now, at least, replace all HTMLENTITIES with normal text !!
        // Fe: HTML-Code of the link is: <a href="index.php?x=1&amp;y=2">
        // -> Link has to be "index.php?x=1&y=2"
        $link = preg_replace($entities, $replace, $link);

        // Replace linebreaks in the link with "" (happens if a links in the sourcecode
        // linebreaks)
        $link = str_replace(array("\n", "\r"), "", $link);

        // "Normalize" URL
        $link = self::normalizeUrl($link);

        return $link;
    }

}
