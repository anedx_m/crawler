<?php

class JobsTable extends Table {

	public function processingField( $data_row, $field ) {
		switch ( $field ) {
			case 'only_domain':
				if ( $data_row[ $field ] == 1 ) {
					return 'Yes';
				} else {
					return 'No';
				}
				break;

			case 'links_found':
			case 'domains_found':
				return number_format( $data_row[ $field ] );
				break;

			case 'actions':
//				$status = Helper::getOption( 'status' );
//				if ( isset( $status[ 'status' ] ) AND $status[ 'job_id' ] == $data_row[ 'id' ] ) {
//					$action = $status[ 'status' ];
//				} else {
//					$action = 'start';
//				}
				if ( $data_row[ 'status' ] == 'Processing' )
					return '<input type="button" id="' . $data_row[ 'id' ] . '" class="btn btn-default stop margin-left-5" value="Stop">';
				else
					return '<input type="button" id="' . $data_row[ 'id' ] . '" class="btn btn-default delete margin-left-5" value="Delete">';
				break;
			default:
				return $data_row[ $field ];
		}
	}

	public function getSortableColumns() {
		return array( 'title', 'urls', 'urls_parse', 'depth', 'crawlers', 'threads_per_crawler', 'only_domain', 'status', 'links_found', 'domains_found','max_urls_per_domain' );
	}

	public function getFields() {
		return array(
			'title'					 => 'Title',
			'depth'					 => 'Max Depth',
			'only_domain'			 => 'Only the domain',
			'crawlers'				 => 'Crawlers',
			'threads_per_crawler'	 => 'Threads per crawler',
			'status'				 => 'Status',
			'links_found'			 => 'Links',
			'domains_found'			 => 'Domains',
			'max_urls_per_domain'	 => 'Max urls per domain',
			'actions'				 => '' );
	}

	public function display() {
		$this->max_visible_pages = 10;
		$this->createTable();
	}

	public function getData() {
		$db = DB::getInstance();

		$current_page = $this->getCurrentPage();

		$where			 = '';
		$params			 = Helper::getRequest( 'params' );
		$search			 = Helper::getValue( $params, 'search' );
		$prepare_params	 = array();
		if ( $search ) {
			$where						 = "WHERE title LIKE :search";
			$prepare_params[ ':search' ] = "%$search%";
		}
		$order_by		 = $this->order_by;
		$order_by_sql	 = '';
		$direction		 = $this->direction;
//		if ( $direction == 'DESC' ) {
//			$direction = 'ASC';
//		} else {
//			$direction = 'DESC';
//		}
		$sc				 = $this->getSortableColumns();
		if ( $order_by AND in_array( $order_by, $sc ) ) {
			$order_by_sql = "ORDER BY $order_by $direction";
		}

		$limit		 = $this->limit;
		$limit_sql	 = '';
		if ( $limit ) {
			$limit_sql					 = "LIMIT :limit OFFSET :offset";
			$prepare_params[ ':limit' ]	 = $limit;
			$prepare_params[ ':offset' ] = ($current_page - 1) * $limit;
		}
		$sql	 = "SELECT * FROM jobs as j $where $order_by_sql $limit_sql";
		$jobs	 = $db->prepare( $sql );
		$r		 = $jobs->execute( $prepare_params );
//		var_dump( $sql, $jobs, $prepare_params, $jobs->errorInfo() );
		$jobs	 = $jobs->fetchAll( PDO::FETCH_NAMED );

		$prepare_amount_params = array();
		if ( $search ) {
			$where								 = "WHERE title LIKE :search";
			$prepare_amount_params[ ':search' ]	 = "%$search%";
		}
		$amount			 = $db->prepare( 'SELECT COUNT(*) as amount FROM jobs ' . $where );
		$r				 = $amount->execute( $prepare_amount_params );
		$amount			 = $amount->fetch( PDO::FETCH_COLUMN );
		//-=-=-=-=-=-=-=-=-
		$this->amount	 = $amount;
		//-=-=-=-=-=-=-=-=-
		return $jobs;
	}

}
