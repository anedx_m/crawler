<?php

class Process {

	protected $DB;

	public function __construct( $db ) {
		$this->DB = $db;
	}

	public function getCountLive( $type ) {
		$r = $this->DB->query( "SELECT COUNT(*) FROM processes WHERE type='$type'" );
		return $r->fetchColumn();
	}

	public function removeDeadProcesses( $max_exec_time ) {
		$this->DB->exec( 'DELETE FROM processes WHERE extract(epoch from now())-live_at_time >= ' . $max_exec_time );
	}

	public function addProcess( $pid, $type ) {
		$this->DB->exec( "INSERT INTO processes(pid,live_at_time,type) VALUES( '$pid', extract(epoch from now()),'$type' )" );
	}

	public function endProcess( $pid ) {
		$this->DB->exec( "DELETE FROM processes WHERE pid='$pid'" );
	}

	public function updateTime( $pid ) {
		$this->DB->exec( "UPDATE processes SET live_at_time=extract(epoch from now()) WHERE pid='$pid'" );
	}

}
