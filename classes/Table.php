<?php

class Table {

	public $amount;
	public $limit				 = 50;
	public $max_visible_pages	 = 10;
	public $direction;
	public $order_by;
	public $search;

	public function __construct() {
		$this->limit	 = (int) Helper::getValue( $_REQUEST, 'limit', $this->limit );
		$this->order_by	 = Helper::getRequest( 'order_by' );
		$this->direction = Helper::getValue( $_REQUEST, 'direction', 'ASC' );
	}

	public function getFields() {
		return array();
	}

	public function processingField( $data_row, $field ) {
		return $data_row[ $field ];
	}

	public function getSortableColumns() {
		return array();
	}

	public function getData() {

		return array();
	}

	public function createTable() {


		$data = $this->getData();

		$current_page	 = $this->getCurrentPage();
		$amount_pages	 = (int) ($this->amount / $this->limit);
		if ( $amount_pages < ($this->amount / $this->limit) ) {
			$amount_pages+=1;
		}
//		var_dump( $amount_pages );
		$start_page	 = $current_page - ((int) ( $this->max_visible_pages / 2 ));
		$end_page	 = $current_page + ((int) ( $this->max_visible_pages / 2 ));

		if ( $end_page > $amount_pages ) {
			$end_page = $amount_pages;
		}
		if ( $start_page < 1 ) {
			$start_page = 1;
		}
		$fields		 = $this->getFields();
		$start_el	 = ($current_page - 1) * $this->limit + 1;
		$end_el		 = ($start_el - 1) + $this->limit;
		if ( $end_el > $this->amount ) {
			$end_el = $this->amount;
		}
		Helper::renderPartial( 'table', array( 'Table'		 => $this, 'data'		 => $data, 'fields'	 => $fields,
			'pagination' => array( 'limit' => $this->limit, 'current_page' => $current_page, 'amount_pages' => $amount_pages, 'start_page' => $start_page, 'end_page' => $end_page ),
			'order_by'	 => $this->order_by, 'direction'	 => $this->direction, 'start_el'	 => number_format($start_el), 'end_el'	 => number_format($end_el), 'total'		 => number_format($this->amount) ) );
	}

	public function getCurrentPage() {
		$current_page	 = (int) Helper::getValue( $_REQUEST, 'current_page', 1 );
		$start_search	 = Helper::getRequest( 'start_search' );
		if ( $start_search == 1 ) {
			$current_page = 1;
		}
		return $current_page;
	}

}
