<?php

class DomainsOfflineTable extends Table {

	public function getSortableColumns() {
		return array( 'domain', 'tld','date_found','http_code','whois_available', 'whois_expire_date', 'whois_available_since_date', 'ms_trust', 'ms_citation', 'moz_da', 'date_found','ms_linking_domains','ms_total_links' );
	}

	public function getFields() {
		return array( 'domain' => 'Domain', 'tld'=>'TLD','date_found' => 'Date Found','http_code' => 'HTTP','whois_available' => 'Avail.', 'whois_available_since_date'=>'Avail. since','whois_expire_date' => 'Expire on','moz_da' => 'Moz DA', 'ms_trust' => 'MS Trust', 'ms_citation' => 'MS Citation','ms_linking_domains'=>'# of linking domains','ms_total_links'=>'# of links');
	}

	public function display() {
		$this->max_visible_pages = 10;
		$this->createTable();
	}

	public function processingField( $data_row, $field ) {
		switch ( $field ) {
			case 'whois_available':
				if ( is_null( $data_row[ $field ] ) ) {
					return '';
				} elseif ( $data_row[ $field ] > 0 ) {
					return 'Yes';
				} else {
					return 'No';
				}
				break;
			case 'domain':
				return '<a href="http://' . $data_row[ $field ] . '" target=_blank>' . $data_row[ $field ] . '</a>';
				break;
			case 'ms_linking_domains':
			case 'ms_total_links':
				return number_format( $data_row[ $field ] );
				break;
			default:
				return $data_row[ $field ];
		}
	}

	public function getData() {
		$db = DB::getInstance();

		$current_page = $this->getCurrentPage();

		$where		 = '';
		$params		 = Helper::getRequest( 'params' );
		$search		 = Helper::getValue( $params, 'search' );
		$date_from	 = Helper::getValue( $params, 'date_from' );
		$date_to	 = Helper::getValue( $params, 'date_to' );
		$http_code	 = Helper::getValue( $params, 'http_code' );
		$available	 = Helper::getValue( $params, 'available', false );
		$max_row	 = Helper::getValue( $params, 'max_row' );

		$prepare_params = array();
		if ( $search ) {
			$where						 = "WHERE domain LIKE :search";
			$prepare_params[ ':search' ] = "%$search%";
		}
		if ( $date_from ) {
			if ( $where ) {
				$where .= " AND whois_available_since_date>=:date_from";
			} else {
				$where = "WHERE whois_available_since_date>=:date_from";
			}
			$prepare_params[ ':date_from' ] = $date_from;
		}
		if ( $date_to ) {
			if ( $where ) {
				$where .= " AND whois_available_since_date<=:date_to";
			} else {
				$where = "WHERE whois_available_since_date<=:date_to";
			}
			$prepare_params[ ':date_to' ] = $date_to;
		}
		if ( $http_code ) {
			if ( $where ) {
				$where .= " AND http_code=:http_code";
			} else {
				$where = "WHERE http_code=:http_code";
			}
			$prepare_params[ ':http_code' ] = $http_code;
		}
		if ( $available == 'true' ) {
			if ( $where ) {
				$where .= " AND whois_available=1";
			} else {
				$where = "WHERE whois_available=1";
			}
		}

		$order_by		 = $this->order_by;
		$order_by_sql	 = '';
		$direction		 = $this->direction;
//		if ( $direction == 'DESC' ) {
//			$direction = 'ASC';
//		} else {
//			$direction = 'DESC';
//		}
		$sc				 = $this->getSortableColumns();
		if ( $order_by AND in_array( $order_by, $sc ) ) {
			if ( $direction == 'DESC' ) {
				$nulls = 'NULLS LAST';
			} else {
				$nulls = 'NULLS FIRST';
			}
			$domain			 = $order_by == 'domain' ? '' : ',domain';
			$order_by_sql	 = "ORDER BY $order_by $direction $nulls $domain";
		}

		$limit		 = $this->limit;
		$limit_sql	 = '';

		if ( $max_row ) {
			$limit_sql					 = "LIMIT :limit";
			$prepare_params[ ':limit' ]	 = $max_row;
		} elseif ( $limit ) {
			$limit_sql					 = "LIMIT :limit OFFSET :offset";
			$prepare_params[ ':limit' ]	 = $limit;
			$prepare_params[ ':offset' ] = ($current_page - 1) * $limit;
		}
		$sql	 = "SELECT * FROM domains_offline $where $order_by_sql $limit_sql";
		$jobs	 = $db->prepare( $sql );
//		var_dump( $_REQUEST,$order_by,$sql, $jobs, $prepare_params, $jobs->errorInfo() );
		$r		 = $jobs->execute( $prepare_params );
//		var_dump( $_REQUEST, $order_by, $sql, $jobs, $prepare_params, $jobs->errorInfo() );
//		die();
		$jobs	 = $jobs->fetchAll( PDO::FETCH_NAMED );

		unset( $prepare_params[ ':limit' ] );
		unset( $prepare_params[ ':offset' ] );
		$amount			 = $db->prepare( 'SELECT COUNT(*) as amount FROM domains_offline ' . $where );
		$r				 = $amount->execute( $prepare_params );
		$amount			 = $amount->fetch( PDO::FETCH_COLUMN );
		//-=-=-=-=-=-=-=-=-
		$this->amount	 = $amount;
		//-=-=-=-=-=-=-=-=-
		return $jobs;
	}

}
