<?php

class MozAPI extends MultiCurl {

	public function __construct( $curl_options = array(), $connection_timeout = 30, $max_page_size = 1024100 ) {
		$db								 = DB::getInstance();
		$connection_timeout				 = Helper::getSetting( 'connection_timeout' );
		$max_page_size					 = Helper::getSetting( 'max_page_size' );
		parent::__construct( $curl_options, $connection_timeout, $max_page_size );
		$this->query_select_random_proxy = $db->prepare( 'SELECT * FROM proxies WHERE live=1 ORDER BY random() LIMIT 1;' );
		$this->query_update_all = $db->prepare( "UPDATE domains_all SET moz_da=:moz_da, moz_check_da_date=current_date WHERE domain=:domain" );
		$this->query_update_off = $db->prepare( "UPDATE domains_offline SET moz_da=:moz_da, moz_check_da_date=current_date WHERE domain=:domain" );
	}

	public function getNextDomains( $limit ) {
		$db			 = Db::getInstance();
		$refresh_moz = Helper::getSetting('refresh_moz');
		
		//try offline new 
		$pr			 = $db->prepare( "UPDATE domains_offline SET moz_check_da_date = current_date
			FROM  (    SELECT id   FROM   domains_offline WHERE  moz_check_da_date IS NULL LIMIT :limit FOR UPDATE) sub
			WHERE  domains_offline.id = sub.id
		RETURNING domains_offline.domain as url, domains_offline.id as domain_id" ); //debug 
		$r			 = $pr->execute( array( ':limit' => $limit ) );
		$start_urls	 = $pr->fetchAll( PDO::FETCH_NAMED );
		if(!empty($start_urls)) {
			return $start_urls;
		}
		
		//try offline  
		$pr			 = $db->prepare( "UPDATE domains_offline SET moz_check_da_date = current_date
			FROM  (    SELECT id   FROM   domains_offline WHERE  age(current_date, moz_check_da_date)>interval '$refresh_moz days'  LIMIT :limit FOR UPDATE) sub
			WHERE  domains_offline.id = sub.id
		RETURNING domains_offline.domain as url, domains_offline.id as domain_id" ); //debug 
		$r			 = $pr->execute( array( ':limit' => $limit ) );
		$start_urls	 = $pr->fetchAll( PDO::FETCH_NAMED );
		if(!empty($start_urls)) {
			return $start_urls;
		}
		
		// try all
		$pr			 = $db->prepare( "UPDATE domains_all SET moz_check_da_date = current_date
			FROM  (    SELECT id   FROM   domains_all WHERE  moz_check_da_date IS NULL OR age(current_date, moz_check_da_date)>interval '$refresh_moz days' LIMIT :limit FOR UPDATE) sub
			WHERE  domains_all.id = sub.id
		RETURNING domains_all.domain as url, domains_all.id as domain_id" ); //debug 
		$r			 = $pr->execute( array( ':limit' => $limit ) );
		$start_urls	 = $pr->fetchAll( PDO::FETCH_NAMED );

		return $start_urls;
	}

	public function processNextDomains( $limit,$domains ) {
		//$domains = $this->getNextDomains( $limit );
		$k		 = $this->getMozKey();
		$url	 = $this->getMozUrl( $k );
		if ( $url AND count( $domains ) ) {
			$urls	 = array( 0 => array( 'url' => $url, 'domains' => $domains ) );
			$data	 = array();
			foreach ( $domains as $domain ) {
				$data[] = $domain[ 'url' ];
			}
			$curl_post_param = array(
				CURLOPT_POST		 => 1,
				CURLOPT_POSTFIELDS	 => json_encode( $data )
			);
			//var_dump( json_encode( $data ) );
			$this->start( $urls, array( $this, 'handler' ), FALSE, $curl_post_param );
		}

		$this->releaseKey( $k );
		return count( $domains );
	}

	protected function handler( $content, $curl_info, $curl_multi_info, $urls_params ) {
		echo '--------';
		if ( $curl_info[ 'http_code' ] == 200 ) {
			$data	 = json_decode( $content, true );
//			$moz_da = $data['pda'];
			$db		 = DB::getInstance();
			var_dump( $urls_params, $data );
			foreach ( $urls_params[ 'domains' ] as $index => $domain ) {
				$domain_id	 = $domain[ 'domain_id' ];
				$domain		 = $domain[ 'url' ];
				if ( isset( $data[ $index ][ 'pda' ] ) ) {
					$moz_da	 = round( $data[ $index ][ 'pda' ] );
					$this->query_update_all->execute( array( ':domain' => $domain, ':moz_da' => $moz_da ) );
					$this->query_update_off->execute( array( ':domain' => $domain, ':moz_da' => $moz_da ) );
				}
			}
		} else {
			var_dump( $curl_info );
		}
		echo '--------';
	}

	protected function getProxy() {
		$this->query_select_random_proxy->execute();
		$r			 = $this->query_select_random_proxy->fetch( PDO::FETCH_NAMED );
		$address	 = Helper::getValue( $r, 'address' );
		$username	 = Helper::getValue( $r, 'username' );
		$password	 = Helper::getValue( $r, 'password' );

		$curl_proxy_opt = array(
			CURLOPT_PROXY => $address,
		);
		if ( $username and $password ) {
			$curl_proxy_opt[ CURLOPT_HTTPAUTH ]		 = CURLAUTH_BASIC;
			$curl_proxy_opt[ CURLOPT_PROXYUSERPWD ]	 = "$username:$password";
		}
		return $curl_proxy_opt;
	}

	protected function getRandomUseragent() {
		$useragents = Helper::getOption( 'useragents' );
		if ( $useragents ) {
			$c = count( $useragents );
			if ( $c ) {
				$n = mt_rand( 0, $c - 1 );
				return $useragents[ $n ];
			}
		}
		return '';
	}

	public function getMozUrl( $key ) {


		if ( $key ) {
			$k			 = explode( ':', $key );
			$accessID	 = $k[ 0 ];
			$secretKey	 = $k[ 1 ];
		} else {
			return null;
		}

		// Set your expires for five minutes into the future.
		$expires			 = time() + 300;
		// A new linefeed is necessary between your AccessID and Expires.
		$stringToSign		 = $accessID . "\n" . $expires;
		// Get the "raw" or binary output of the hmac hash.
		$binarySignature	 = hash_hmac( 'sha1', $stringToSign, $secretKey, true );
		// We need to base64-encode it and then url-encode that.
		$urlSafeSignature	 = urlencode( base64_encode( $binarySignature ) );
		// This is the URL that we want link metrics for.
		$cols				 = 68719476736 + 34359738368 + 32768 + 16384 + 2048 + 32;
		// Put it all together and you get your request URL. 
		$requestUrl			 = "http://lsapi.seomoz.com/linkscape/url-metrics/?Cols=" . $cols . "&AccessID=" . $accessID . "&Expires=" . $expires . "&Signature=" . $urlSafeSignature;

		return $requestUrl;
	}

	public function getMozKey() {
		self::checkKeys();
		$db	 = DB::getInstance();
		$q	 = $db->query( "UPDATE moz_keys SET locked_at_time = current_timestamp
			FROM  (SELECT id FROM moz_keys WHERE locked_at_time IS NULL LIMIT 1 FOR UPDATE) sub
			WHERE  moz_keys.id = sub.id
		RETURNING  moz_keys.key as key" );
//		var_dump( $db->errorInfo() );
		if ( $q ) {
			$r = $q->fetchColumn();
		} else {
			$r = null;
		}
		return $r;
	}

	public function releaseKey( $key ) {
		$db	 = DB::getInstance();
		$q	 = $db->prepare( "UPDATE moz_keys SET locked_at_time = NULL WHERE key=:key" );
		$q->execute( array( ':key' => $key ) );
	}

	public static function checkKeys() {
		$db	 = DB::getInstance();
		$q	 = $db->query( "UPDATE moz_keys SET locked_at_time = NULL
			FROM  (SELECT id FROM moz_keys WHERE (current_timestamp - locked_at_time) > interval '60 second' LIMIT 1 FOR UPDATE) sub
			WHERE  moz_keys.id = sub.id
		RETURNING  moz_keys.key as key" );
//		var_dump( $db->errorInfo() );
		$q->execute();
	}

}
