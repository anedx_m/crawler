<?php

class MainController extends Controller {

	public $defaultAction = 'Entry';

	public function actionEntry() {
		$this->render( 'main' );
	}

	public function actionJobs() {
		$settings		 = Helper::getOption( 'settings' );
		$crawler_status	 = Helper::getOption( 'crawler_status' );
		$current_job	 = $this->GetCurrentJob();
		$processes		 = $this->GetCurrentProcesses();
		if ( !$crawler_status )
			$crawler_status	 = 'Stop';
		$crawlers		 = $this->getCrawlers();
		$this->renderPartial( 'jobs', array( 'settings' => $settings, 'crawler_status' => $crawler_status, 'current_job' => $current_job, 'processes' => $processes, 'crawlers' => $crawlers ) );
	}

	public function getCrawlers() {
		$db			 = DB::getInstance();
		$r			 = $db->query( "SELECT domain,urls,passed_urls FROM processes  WHERE type='crawler.php' ORDER BY pid" );
		$crawlers	 = $r->fetchAll( PDO::FETCH_ASSOC );

		foreach ( $crawlers as &$crawler ) {
			$crawler[ 'domain' ]        = str_replace("|","<br>",$crawler[ 'domain' ]);
			$crawler[ 'urls' ]			 = number_format( $crawler[ 'urls' ] );
			$crawler[ 'passed_urls' ]	 = number_format( $crawler[ 'passed_urls' ] );
		}
		return $crawlers;
	}

	public function GetCurrentProcesses() {
		$processes	 = array();
		$db			 = DB::getInstance();

		$r									 = $db->query( "SELECT count(*) FROM processes" );
		$processes[ 'running_processes' ]	 = $r->fetchColumn();
		$r									 = $db->query( "SELECT count(*) FROM processes WHERE type='crawler.php'" );
		$processes[ 'crawler' ]				 = $r->fetchColumn();
		$r									 = $db->query( "SELECT count(*) FROM processes WHERE type='offline_check.php'" );
		$processes[ 'test_offline' ]		 = $r->fetchColumn();
		$r									 = $db->query( "SELECT count(*) FROM processes WHERE type='moz_api.php'" );
		$processes[ 'refresh_moz_da' ]		 = $r->fetchColumn();
		$r									 = $db->query( "SELECT count(*) FROM processes WHERE type='dns_check.php'" );
		$processes[ 'refresh_dns_check' ]	 = $r->fetchColumn();
		$r									 = $db->query( "SELECT count(*) FROM processes WHERE type='domainr_available.php'" );
		$processes[ 'refresh_whois_avail' ]	 = $r->fetchColumn();
		$r									 = $db->query( "SELECT count(*) FROM processes WHERE type='domaintools_date_expire.php'" );
		$processes[ 'refresh_whois_date' ]	 = $r->fetchColumn();
		$r									 = $db->query( "SELECT count(*) FROM processes WHERE type='majestic.php'" );
		$processes[ 'refresh_magestic' ]	 = $r->fetchColumn();

		return $processes;
	}

	public function GetCurrentJob() {
		$current_job					 = array();
		$db								 = DB::getInstance();
		$r								 = $db->query( "SELECT title FROM jobs WHERE status='Processing' LIMIT 1" );
		$current_job[ 'title' ]			 = $r->fetchColumn();
//		$r								 = $db->query( "SELECT count(*) FROM urls" );
//		$current_job[ 'urls_in_queue' ]	 = $r->fetchColumn();
		$r								 = $db->query( "SELECT sum(passed_urls) FROM  domains_job" );
		$current_job[ 'passed_urls' ]	 = $r->fetchColumn();
		$r								 = $db->query( "SELECT count(*) FROM  domains_job" );
		$current_job[ 'count_domains' ]	 = $r->fetchColumn();
		return $current_job;
	}

	public function actionGetCurrentJobAndProcesses() {
		$job = $this->GetCurrentJob();

		$r = array();

		//$r[ 'urls_in_queue' ]	 = number_format( $job[ 'urls_in_queue' ] );
		$r[ 'passed_urls' ]		 = number_format( $job[ 'passed_urls' ] );
		$r[ 'count_domains' ]	 = number_format( $job[ 'count_domains' ] );

		$processes					 = $this->GetCurrentProcesses();
		$r[ 'running_processes' ]	 = number_format( $processes[ 'running_processes' ] );
		$r[ 'crawler' ]				 = number_format( $processes[ 'crawler' ] );
		$r[ 'test_offline' ]		 = number_format( $processes[ 'test_offline' ] );
		$r[ 'refresh_moz_da' ]		 = number_format( $processes[ 'refresh_moz_da' ] );
		$r[ 'refresh_dns_check' ]	 = number_format( $processes[ 'refresh_dns_check' ] );
		$r[ 'refresh_whois_avail' ]	 = number_format( $processes[ 'refresh_whois_avail' ] );
		$r[ 'refresh_whois_date' ]	 = number_format( $processes[ 'refresh_whois_date' ] );
		$r[ 'refresh_magestic' ]	 = number_format( $processes[ 'refresh_magestic' ] );

		$r[ 'crawlers' ] = $this->getCrawlers();

		echo json_encode( $r );
	}

	public function actionListJobs() {

		$JobsTable = new JobsTable();
		$JobsTable->display();
	}

	public function getSortableColumns() {
		return array( 'title', 'urls', 'urls_parse', 'depth', 'threads', 'only_domain' );
	}

	public function getFields() {
		return array( 'title' => 'Title', 'urls' => 'Urls', 'urls_parse' => 'Urls To Parse', 'depth' => 'Max Depth', 'only_domain' => 'Only within the domain', 'threads' => 'Threads' );
	}

	public function actionProxies() {
		$proxies = Helper::getOption( 'proxies' );
		$this->renderPartial( 'proxies', array( 'proxies' => $proxies ) );
	}

	public function actionSaveProxies() {
		$proxies = $this->getRequest( 'proxies' );
		if ( $proxies ) {
			echo Helper::updateOption( 'proxies', $proxies );
			$db			 = DB::getInstance();
			$db->exec( 'TRUNCATE TABLE proxies' );
			$db->exec( 'BEGIN' );
			$arr_proxies = explode( "\n", $proxies );
			foreach ( $arr_proxies as $proxy ) {

				$p = parse_url( $proxy );

				if ( $p ) {
					$arr_proxy = array(
						'username'	 => $this->getValue( $p, 'user' ),
						'password'	 => $this->getValue( $p, 'pass' ),
						'address'	 => $this->getValue( $p, 'host' ) . (!isset( $p[ 'port' ] ) ? ':80' : ':' . $p[ 'port' ]),
					);

					$this->updateProxies( $arr_proxy );
				}
			}

			$db->exec( 'COMMIT' );
			return;
		}
		echo -1;
	}

	public function actionUseragents() {
		$useragents = Helper::getOption( 'useragents' );
//		var_dump($useragents);
		if ( $useragents ) {
			$useragents = implode( "\n", $useragents );
		} else {
			$useragents = '';
		}

		$this->renderPartial( 'useragents', array( 'useragents' => $useragents ) );
	}

	public function actionSaveUseragents() {
		$useragents = $this->getRequest( 'useragents' );

		if ( $useragents ) {
			$arr_useragents	 = explode( "\n", $useragents );
			$arr_useragents	 = array_map( "trim", $arr_useragents );
			$arr_useragents	 = array_filter( $arr_useragents );
//			print_r($arr_useragents);
			Helper::updateOption( 'useragents', $arr_useragents );
		} else {
			Helper::updateOption( 'useragents', array() );
		}
	}

	public function updateProxies( $arr_proxy ) {
		$db	 = DB::getInstance();
		$sql = 'INSERT INTO proxies (username,password,address,live) VALUES (:username,:password,:address,1)';
		$up	 = $db->prepare( $sql );
		$r	 = $up->execute( array( ':username' => $arr_proxy[ 'username' ], ':password' => $arr_proxy[ 'password' ], ':address' => $arr_proxy[ 'address' ] ) );
		return $r;
	}

	public function actionSettings() {
		$settings = Helper::getOption( 'settings' );
		$this->renderPartial( 'settings', array( 'settings' => $settings ) );
	}

	public function actionSaveSettings() {
		$settings = $this->getRequest( 'settings' );
		if ( $settings ) {
			$db = DB::getInstance();
			$db->exec( 'TRUNCATE TABLE moz_keys' );
			if ( !empty( $settings[ 'moz_api_key' ] ) ) {
				$arr_moz_keys				 = explode( "\n", $settings[ 'moz_api_key' ] );
				$arr_moz_keys				 = array_unique( $arr_moz_keys );
				$arr_moz_keys				 = array_map( "trim", $arr_moz_keys );
				$arr_moz_keys				 = array_filter( $arr_moz_keys );
				$settings[ 'moz_api_key' ]	 = implode( "\r\n", $arr_moz_keys );
				foreach ( $arr_moz_keys as $key ) {
					$this->addMozKey( $key );
				}
			}
			$db->exec( 'TRUNCATE TABLE dynadot_keys' );
			if ( !empty( $settings[ 'dynadot_keys' ] ) ) {
				$arr_dynadot_keys			 = explode( "\n", $settings[ 'dynadot_keys' ] );
				$arr_dynadot_keys			 = array_map( "trim", $arr_dynadot_keys );
				$arr_dynadot_keys			 = array_unique( $arr_dynadot_keys );
				$arr_dynadot_keys			 = array_filter( $arr_dynadot_keys );
				$settings[ 'dynadot_keys' ]	 = implode( "\r\n", $arr_dynadot_keys );
				foreach ( $arr_dynadot_keys as $key ) {
					$this->addDynadotKey( $key );
				}
			}
			print_r( $settings );
			Helper::updateOption( 'settings', $settings );
		}
	}

	public function addMozKey( $key ) {
		$db	 = DB::getInstance();
		$sql = 'INSERT INTO moz_keys (key) VALUES (:key)';
		$up	 = $db->prepare( $sql );
		$r	 = $up->execute( array( ':key' => $key ) );
		return $r;
	}

	public function addDynadotKey( $key ) {
		$db	 = DB::getInstance();
		$sql = 'INSERT INTO dynadot_keys (key) VALUES (:key)';
		$up	 = $db->prepare( $sql );
		$r	 = $up->execute( array( ':key' => $key ) );
		return $r;
	}

	public function actionSetCrawlerStatus() {
		$crawler_status = Helper::getOption( 'crawler_status' );
		switch ( $crawler_status ) {
			case 'Start':
				Helper::updateOption( 'crawler_status', 'Stop' );
				break;
			case 'Stop';
				Helper::updateOption( 'crawler_status', 'Start' );
				break;
			default :
				Helper::updateOption( 'crawler_status', 'Start' );
		}
	}

	public function addJob( $arr_job ) {

		$db			 = DB::getInstance();
		$sql		 = 'INSERT INTO jobs 
				(title,depth,crawlers,threads_per_crawler,only_domain,status,max_urls_per_domain,max_run_domains,max_time_in_mins) 
				VALUES 
				(:title,:depth,:crawlers,:threads_per_crawler,:only_domain,:status,:max_urls_per_domain,:max_run_domains,:max_time_in_mins) 
			RETURNING id';
		$up			 = $db->prepare( $sql );
		$only_domain = (int) $this->getValue( $arr_job, 'only_domain', 0 );
		$r			 = $up->execute(
		array(
			':title'				 => $arr_job[ 'name_job' ],
			':depth'				 => $arr_job[ 'depth' ],
			':crawlers'				 => $arr_job[ 'crawlers' ],
			':threads_per_crawler'	 => $arr_job[ 'threads_per_crawler' ],
			':only_domain'			 => $only_domain,
			':status'				 => 'New',
			':max_urls_per_domain'	 => $arr_job[ 'max_urls_per_domain' ],
			':max_run_domains'		 => $arr_job[ 'max_run_domains' ],
			':max_time_in_mins'		 => $arr_job[ 'max_time_in_mins' ],
		)
		);
//		var_dump($up->errorInfo());
//		die();
		$id			 = false;
		if ( $r ) {
			$id = $up->fetch( PDO::FETCH_COLUMN );
		}
		return $id;
	}

	public function addUrl( $job_id, $url ) {
		$db	 = DB::getInstance();
		$sql = 'INSERT INTO jobs_start_urls (job_id,url) VALUES (:job_id,:url)';
		$up	 = $db->prepare( $sql );
		$r	 = $up->execute( array( ':job_id' => $job_id, ':url' => $url ) );

		return $r;
	}

	public function actionAddJob() {
		$job = $this->getValue( $_REQUEST, 'job' );
		if ( $job ) {
			$job_id = $this->addJob( $job );
			if ( $job_id ) {
				$urls		 = $job[ 'list_urls' ];
				$arr_urls	 = explode( "\n", $urls );
				$arr_urls	 = array_unique( $arr_urls );
				$arr_urls	 = array_map( "trim", $arr_urls );
				$arr_urls	 = array_filter( $arr_urls );

				foreach ( $arr_urls as $url ) {
					if ( !empty( $url ) )
						$this->addUrl( $job_id, $url );
				}
			}
		}
		header( 'Location: ?act=Entry' );
	}

	public function actionDeleteJob() {
		$job_id = $this->getRequest( 'job_id' );
		if ( $job_id ) {
			$db = DB::getInstance();

			$pr	 = $db->prepare( "SELECT * FROM jobs WHERE id=:job_id" );
			$r	 = $pr->execute( array( ':job_id' => $job_id ) );
			$row = $pr->fetch( PDO::FETCH_NAMED );
			if ( $row[ 'status' ] == 'Processing' )
				Helper::finish_job( $db );

			$pr	 = $db->prepare( 'DELETE FROM jobs WHERE id=:job_id' );
			$r	 = $pr->execute( array( ':job_id' => $job_id ) );
			$pr	 = $db->prepare( 'DELETE FROM jobs_start_urls WHERE job_id=:job_id' );
			$r	 = $pr->execute( array( ':job_id' => $job_id ) );
			echo $r;
		}
	}

	public function actionStopJob() {
		$job_id = $this->getRequest( 'job_id' );
		if ( $job_id ) {
			$db = DB::getInstance();

			$pr	 = $db->prepare( "SELECT * FROM jobs WHERE id=:job_id" );
			$r	 = $pr->execute( array( ':job_id' => $job_id ) );
			$row = $pr->fetch( PDO::FETCH_NAMED );
			if ( $row[ 'status' ] == 'Processing' )
				Helper::finish_job( $db );
		}
	}

	public function actionExludeDomains() {
		$exclude_domains = Helper::getOption( 'exclude_domains' );
		if ( $exclude_domains ) {
			$exclude_domains = implode( "\n", $exclude_domains );
		} else {
			$exclude_domains = '';
		}

		$this->renderPartial( 'excludeDomains', array( 'exclude_domains' => $exclude_domains ) );
	}

	public function actionSaveExcludeDomains() {
		$exclude_domains = $this->getRequest( 'exclude_domains' );
		print_r( $exclude_domains );
		if ( $exclude_domains ) {
			$arr_exclude_domains = explode( "\n", $exclude_domains );
			$arr_exclude_domains = array_map( "trim", $arr_exclude_domains );
			$arr_exclude_domains = array_filter( $arr_exclude_domains );
			Helper::updateOption( 'exclude_domains', $arr_exclude_domains );
		} else {
			Helper::updateOption( 'exclude_domains', array() );
		}
	}

	public function actionDomains() {
		$this->renderPartial( 'domains' );
	}

	public function actionDomainsOffline() {
		$this->renderPartial( 'DomainsOffline' );
	}

	public function actionListDomains() {
		$DomainsTable = new DomainsTable();
		$DomainsTable->display();
	}

	public function actionListDomainsOffline() {
		$DomainsTable = new DomainsOfflineTable();
		$DomainsTable->display();
	}

	public function actionExportDomains() {
		$DomainsTable	 = new DomainsTable();
		$data			 = $DomainsTable->getData();

		$filename	 = 'Domains.csv';
		$fh			 = @fopen( 'php://output', 'w' );
		header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header( 'Content-Description: File Transfer' );
		header( 'Content-type: text/csv' );
		header( "Content-Disposition: attachment; filename={$filename}" );
		header( 'Expires: 0' );
		header( 'Pragma: public' );

		$header_row = array( 'domain', 'tld', 'date_found', 'whois_available', 'moz_da' );

		fputcsv( $fh, $header_row , ";");

		foreach ( $data as $data_row ) {
			$row = array();
			foreach ( $header_row as $key => $value ) {
				$row[] = isset( $data_row[ $value ] ) ? $data_row[ $value ] : '';
			}
			fputcsv( $fh, $row , ";");
		}

		fclose( $fh );
		die();
	}

	public function actionExportDomainsOffline() {
		$DomainsTable	 = new DomainsOfflineTable();
		$data			 = $DomainsTable->getData();

		$filename	 = 'DomainsOffline.csv';
		$fh			 = @fopen( 'php://output', 'w' );
		header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header( 'Content-Description: File Transfer' );
		header( 'Content-type: text/csv' );
		header( "Content-Disposition: attachment; filename={$filename}" );
		header( 'Expires: 0' );
		header( 'Pragma: public' );

		$header_row = array( 'domain', 'tld','date_found','http_code','whois_available', 'whois_expire_date', 'whois_available_since_date', 'ms_trust', 'ms_citation', 'moz_da', 'date_found','ms_linking_domains','ms_total_links' );

		fputcsv( $fh, $header_row, ";" );

		foreach ( $data as $data_row ) {
			$row = array();
			foreach ( $header_row as $key => $value ) {
				$row[] = isset( $data_row[ $value ] ) ? $data_row[ $value ] : '';
			}
			fputcsv( $fh, $row , ";");
		}

		fclose( $fh );
		die();
	}

}
