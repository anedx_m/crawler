<?php

class DomainrAPI extends MultiCurl {

	public function __construct( $curl_options = array(), $connection_timeout = 30, $max_page_size = 1024100 ) {
		$db					 = DB::getInstance();
		$connection_timeout	 = Helper::getSetting( 'connection_timeout' );
		$connection_timeout	 = 120;
		$max_page_size		 = Helper::getSetting( 'max_page_size' );
		parent::__construct( $curl_options, $connection_timeout, $max_page_size );

		$this->query_update_off_good = $db->prepare( "UPDATE domains_offline SET whois_available=1, whois_available_since_date = COALESCE(whois_available_since_date,current_date) WHERE domain=:domain" );
		$this->query_update_off_bad	 = $db->prepare( "UPDATE domains_offline SET whois_available=0, whois_available_since_date = NULL  WHERE domain=:domain" );

		$this->query_update_all_good = $db->prepare( "UPDATE domains_all SET whois_available=1 WHERE domain=:domain" );
		$this->query_update_all_bad	 = $db->prepare( "UPDATE domains_all SET whois_available=0 WHERE domain=:domain" );

		$this->query_update_not_avail		 = $db->prepare( "UPDATE domains_offline SET whois_available=0 WHERE domain=:domain" );
		$this->query_update_not_avail_mask	 = $db->prepare( "UPDATE domains_offline SET whois_available=0 WHERE domain LIKE :domain" );
		
		$this->query_update_off_for_retest = $db->prepare( "UPDATE domains_offline SET whois_check_avail_date=NULL WHERE domain=:domain" );
		
	}

	public function getNextDomains( $limit ) {
		$db				 = Db::getInstance();
		$refresh_whois	 = Helper::getSetting( 'refresh_whois' );
		$min_moz_da_domainr	 = Helper::getSetting( 'min_moz_da_domainr' );

		//try NEW domains_offline 
		$pr					 = $db->prepare( "UPDATE domains_offline SET whois_check_avail_date = current_date
			FROM  (    SELECT id   FROM   domains_offline WHERE  whois_check_avail_date IS NULL AND moz_da>=:min_moz_da_domainr AND nameserver_exists=0 LIMIT :limit FOR UPDATE) sub
			WHERE  domains_offline.id = sub.id
		RETURNING domains_offline.domain as url, domains_offline.id as domain_id" ); //debug 
		$r					 = $pr->execute( array( ':limit' => $limit, ':min_moz_da_domainr' => $min_moz_da_domainr ) );
		$results			 = $pr->fetchAll( PDO::FETCH_NAMED );
		if ( $results )
			return $results;

		//try AGE domains_offline 
		$pr		 = $db->prepare( "UPDATE domains_offline SET whois_check_avail_date = current_date
			FROM  (    SELECT id   FROM   domains_offline WHERE  age(current_date, whois_check_avail_date)>interval '$refresh_whois days' AND moz_da>=:min_moz_da_domainr AND nameserver_exists=0 LIMIT :limit FOR UPDATE) sub
			WHERE  domains_offline.id = sub.id
		RETURNING domains_offline.domain as url, domains_offline.id as domain_id" ); //debug 
		$r		 = $pr->execute( array( ':limit' => $limit, ':min_moz_da_domainr' => $min_moz_da_domainr ) );
		$results = $pr->fetchAll( PDO::FETCH_NAMED );
		return $results;
	}

	public function processNextDomains( $limit ) {
		$mashape_key = Helper::getSetting( 'mashape_key' );
		$domains	 = $this->getNextDomains( $limit );
		//$domains[]= array('url'=>"buesd.k12.ca.us"); //debug 
		
		if(count($domains)<$limit) {
			foreach($domains as $domain)
				$this->query_update_off_for_retest->execute( array(':domain'=>$domain['url']));
			echo count($domains)." is less than $limit. stop";	
			return false;
		}
		
		if ( $domains ) {
			$urls		 = array();
			$urls[] = array(
				'url' => $this->DomainrUrl( $domains, $mashape_key ),
				'domains' => $domains,
			);
			$this->start( $urls, array( $this, 'handler' ), FALSE );
		}

		return count( $domains );
	}

	function do_extra_api_check( $domain ) {
		$addr		 = "https://api.domrobot.com/xmlrpc/";
		//$addr = "https://api.ote.domrobot.com/xmlrpc/";
		$usr		 = "tripleaom";
		$pwd		 = "kgza5vaa";
		$domrobot	 = new Domrobot( $addr );
		$domrobot->setDebug( false );
		$domrobot->setLanguage( 'en' );
		$res		 = $domrobot->login( $usr, $pwd );

		if ( $res[ 'code' ] == 1000 ) {
			$obj				 = "domain";
			$meth				 = "check";
			$params				 = array();
			$params[ 'domain' ]	 = $domain;
			$res				 = $domrobot->call( $obj, $meth, $params );
			print_r( $res );
			if ( $res[ 'code' ] == 1000 AND isset( $res[ 'resData' ][ 'domain' ][ 0 ][ 'avail' ] ) ) {
				return $res[ 'resData' ][ 'domain' ][ 0 ][ 'avail' ];
			}
		}
		print_r( $res );
		return 1; // Yes it's avaliable still if API failed
	}

	protected function handler( $content, $curl_info, $curl_multi_info, $urls_params ) {
		$data	 = json_decode( $content, true );
		if(!is_array($data) OR empty($data[ 'status' ]) AND empty($data[ 'errors' ])) 
			die("Fatal error : $content");
			
		if(!empty($data[ 'status' ])) {
			foreach ( $data[ 'status' ] as $row ) {
				echo "OK $row[domain] -> $row[summary]\n";
				$domain = $row[ 'domain' ];
				if ( $row[ 'summary' ] !== 'inactive' ) {
					$this->query_update_all_bad->execute( array( ':domain' => $domain ) );
					$this->query_update_off_bad->execute( array( ':domain' => $domain ) );
				} else {
					//inactive , run 2nd check 
					$whois_available = $this->do_extra_api_check( $domain );
					echo "$domain is ok, 2nd api return $whois_available\n";
					if ( $whois_available ) {
						$this->query_update_all_good->execute( array( ':domain' => $domain ) );
						$this->query_update_off_good->execute( array( ':domain' => $domain ) );
					} else {
						$this->query_update_all_bad->execute( array( ':domain' => $domain ) );
						$this->query_update_off_bad->execute( array( ':domain' => $domain ) );
					}
				}
			}
		}	
		
		if(!empty($data[ 'errors' ])) {
			foreach ( $data[ 'errors' ] as $row ) {
				echo "ERROR $row[detail] -> $row[message]\n";
				$this->query_update_all_bad->execute( array( ':domain' => $row['detail'] ) );
				$this->query_update_off_bad->execute( array( ':domain' => $row['detail'] ) );
			}
		}
	}

	public function DomainrUrl( $domains, $mashape_key ) {
		$arr_domains = array();
		foreach ( $domains as $domain ) {
			$arr_domains[] = $domain[ 'url' ];
		}
		$str_domains = implode( ',', $arr_domains );
		return "https://domainr.p.mashape.com/v2/status?domain=$str_domains&mashape-key=$mashape_key";
	}

}
