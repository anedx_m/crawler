<?php

class DB {
	/**
	 *
	 * @var PDO 
	 */
	private static $_PDO;

	private function __construct() {
		
	}

	public static function getInstance() {
		if (null === self::$_PDO) {
			self::$_PDO = new PDO('pgsql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USERNAME, DB_PASSWORD);
			//self::$_PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			//self::$_PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
			self::$_PDO->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);			
		}
		return self::$_PDO;
	}
	
	
	
}
