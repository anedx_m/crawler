<?php

class DynadotAPI extends MultiCurl {

	public function __construct( $curl_options = array(), $connection_timeout = 30, $max_page_size = 1024100 ) {
		$db								 = DB::getInstance();
		$connection_timeout				 = Helper::getSetting( 'connection_timeout' );
		$connection_timeout				 =  120;
		$max_page_size					 = Helper::getSetting( 'max_page_size' );
		parent::__construct( $curl_options, $connection_timeout, $max_page_size );
		
		$this->query_select_random_proxy = $db->prepare( 'SELECT * FROM proxies ORDER BY random() LIMIT 1;' );
		
		$this->query_update_off_good = $db->prepare( "UPDATE domains_offline SET whois_available=1, whois_available_since_date = current_date WHERE domain=:domain" );
		$this->query_update_off_bad  = $db->prepare( "UPDATE domains_offline SET whois_available=0, whois_available_since_date = NULL  WHERE domain=:domain" );
		
		$this->query_update_all_good = $db->prepare( "UPDATE domains_all SET whois_available=1 WHERE domain=:domain" );
		$this->query_update_all_bad  = $db->prepare( "UPDATE domains_all SET whois_available=0 WHERE domain=:domain" );
	}

	public function getNextDomains( $limit ) {
		$db				 = Db::getInstance();
		$refresh_whois	 = Helper::getSetting( 'refresh_whois' );

		//try NEW domains_offline 
		$pr			 = $db->prepare( "UPDATE domains_offline SET whois_check_avail_date = current_date
			FROM  (    SELECT id   FROM   domains_offline WHERE  whois_check_avail_date IS NULL LIMIT :limit FOR UPDATE) sub
			WHERE  domains_offline.id = sub.id
		RETURNING domains_offline.domain as url, domains_offline.id as domain_id" ); //debug 
		$r			 = $pr->execute( array( ':limit' => $limit ) );
		$results = $pr->fetchAll( PDO::FETCH_NAMED );
		if($results)
			return $results;
		
		//try AGE domains_offline 
		$pr			 = $db->prepare( "UPDATE domains_offline SET whois_check_avail_date = current_date
			FROM  (    SELECT id   FROM   domains_offline WHERE  age(current_date, whois_check_avail_date)>interval '$refresh_whois days' LIMIT :limit FOR UPDATE) sub
			WHERE  domains_offline.id = sub.id
		RETURNING domains_offline.domain as url, domains_offline.id as domain_id" ); //debug 
		$r			 = $pr->execute( array( ':limit' => $limit ) );
		$results = $pr->fetchAll( PDO::FETCH_NAMED );

		return $results;
	}

	public function processNextDomains( $limit_of_key ) {
		$urls			 = array();
		$arr_used_keys	 = array();
		$domains		 = array();

		while ( $dynadotkey	 = $this->getDynadotKey() AND $domains	 = $this->getNextDomains( $limit_of_key ) ) {
			$arr_used_keys[] = $dynadotkey;
			$urls[]			 = array(
				'url' => $this->getWhoisUrl( $domains, $dynadotkey ),
			);
		}
//		var_dump( $urls );
		if ( count( $urls ) ) {
//			$curl_params = array( CURLOPT_ENCODING, "gzip,deflate" );
			$this->start( $urls, array( $this, 'handler' ), FALSE );
		}
		foreach ( $arr_used_keys as $key ) {
			var_dump( 'release key ' . $key );
			$this->releaseKey( $key );
		}
		return count( $domains );
	}
	
	function do_extra_api_check($domain) {
		
		$addr = "https://api.domrobot.com/xmlrpc/";
		//$addr = "https://api.ote.domrobot.com/xmlrpc/";
		$usr = "tripleaom";
		$pwd = "kgza5vaa";
		$domrobot = new Domrobot($addr);
		$domrobot->setDebug(false);
		$domrobot->setLanguage('en');
		$res = $domrobot->login($usr,$pwd);

		if ($res['code']==1000) {
			$obj = "domain";
			$meth = "check";
			$params = array();
			$params['domain'] = $domain;
			$res = $domrobot->call($obj,$meth,$params);
			print_r($res);
			if ($res['code']==1000 AND isset($res['resData']['domain'][0]['avail'])) {
				return $res['resData']['domain'][0]['avail'];
			}
		} 
			print_r($res);
		return 1; // Yes it's avaliable still if API failed
	}
	

	protected function handler( $content, $curl_info, $curl_multi_info, $urls_params ) {
		echo '--------';

		if ( $curl_info[ 'http_code' ] == 200 ) {
			$db		 = DB::getInstance();
			$content = explode( "\n", $content );
			$data	 = array();
			foreach ( $content as $row ) {
				$data[] = str_getcsv( $row );
			}

			$first_row = $data[ 0 ];
			unset( $data[ 0 ] );
			unset( $data[ 1 ] );
			
			if($first_row[1] =="account banned for 10 minutes due to abuse")
				die("account banned for 10 minutes due to abuse");
			if($first_row[1] =="currently processing another request from this account")
				die("currently processing another request from this account");
				

			
			var_dump( $first_row, $data );
			if ( $first_row[ 0 ] == 'ok' ) {
				foreach ( $data as $row ) {
					if(empty($row[ 1 ]))
						continue;
					$domain	 = $row[ 1 ];
					
					if ( $row[ 0 ] == 'error' ) {
						$whois_available = $this->do_extra_api_check($domain);
						echo "$domain is error1 ?, 2nd api return $whois_available\n";
					}	
					elseif ( is_null( $row[ 0 ] ) OR $row[ 3 ] == 'error' ){
						$whois_available = $this->do_extra_api_check($domain);
						echo "$domain is error2 ?, 2nd api return $whois_available\n";
						
// 						if($row[ 4 ] == 'unsupported domain type') {
// 							file_put_contents(dirname(__FILE__)."/../bad_tlds.txt",$domain."\n",FILE_APPEND );
// 						}
					}else {
						$whois_available = $row[ 3 ] == 'no' ? 0 : 1;
						if ( $whois_available ) {
							$whois_available = $this->do_extra_api_check($domain);
							echo "$domain is avaliable?, 2nd api return $whois_available\n";
						}					
					}	
					
					if($whois_available ) {
						$this->query_update_all_good->execute( array( ':domain' => $domain ) );
						$this->query_update_off_good->execute( array( ':domain' => $domain ) );
					} else {
						$this->query_update_all_bad->execute( array( ':domain' => $domain ) );
						$this->query_update_off_bad->execute( array( ':domain' => $domain ) );
					}	
				}
			}
		} else {
//			var_dump( $curl_info );
		}
		echo '--------';
	}

	public function getWhoisUrl( $domains, $key ) {
		$arr_domain	 = array();
		$i			 = 0;
		foreach ( $domains as $domain ) {
			$arr_domain[] = "domain{$i}=" . $domain[ 'url' ];
			$i++;
		}
		$str_domains = implode( '&', $arr_domain );
		return "https://api.dynadot.com/api2.html?key=$key&command=search&$str_domains";
	}

	public function getDynadotKey() {
		self::checkKeys();
		$db	 = DB::getInstance();
		$q	 = $db->query( "UPDATE dynadot_keys SET locked_at_time = current_timestamp
			FROM  (SELECT id FROM dynadot_keys WHERE locked_at_time IS NULL LIMIT 1 FOR UPDATE) sub
			WHERE  dynadot_keys.id = sub.id
		RETURNING  dynadot_keys.key as key" );
//		var_dump( $db->errorInfo() );
		if ( $q ) {
			$r = $q->fetchColumn();
		} else {
			$r = null;
		}
		return $r;
	}

	public function releaseKey( $key ) {
		$db	 = DB::getInstance();
		$q	 = $db->prepare( "UPDATE dynadot_keys SET locked_at_time = NULL WHERE key=:key" );
		$q->execute( array( ':key' => $key ) );
	}

	public static function checkKeys() {
		$db	 = DB::getInstance();
		$q	 = $db->query( "UPDATE dynadot_keys SET locked_at_time = NULL
			FROM  (SELECT id FROM dynadot_keys WHERE (current_timestamp - locked_at_time) > interval '60 second' LIMIT 1 FOR UPDATE) sub
			WHERE  dynadot_keys.id = sub.id
		RETURNING  dynadot_keys.key as key" );
//		var_dump( $db->errorInfo() );
		$q->execute();
	}

}
