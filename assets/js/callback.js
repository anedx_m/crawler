function Callback() {
    this.add = function( callback, extparams ) {
        if ( typeof ( this.callbacks ) == 'undefined' ) {
            this.callbacks = [ ];
        }
        this.callbacks[this.callbacks.length] = { callback: callback, extparams: extparams };
    }
    this.run = function( params ) {
        if ( typeof ( params ) == 'undefined' ) {
            params = { };
        }
        if ( typeof ( this.callbacks ) != 'undefined' ) {

            jQuery.each( this.callbacks, function( index, e ) {

                e.callback( params, e.extparams )
            } )
        }
    }
}